CREATE OR REPLACE FUNCTION reset_match(mid INTEGER) RETURNS void AS $$
  BEGIN
    -- Reset the start date of the match
    UPDATE matches SET started = NULL WHERE id = mid;
    -- Reset all the scores
    UPDATE players SET shots = 0,
                      sweeps = 0,
                       kills = 0,
                        self = 0,
                 total_score = 0,
                 match_score = 0
     WHERE match_id = mid;
    -- Reset all player states to the default
    UPDATE player_states SET arrows = DEFAULT, shield = DEFAULT, wings = DEFAULT,
                             hat = DEFAULT, invisible = DEFAULT, speed = DEFAULT,
                             alive = DEFAULT, lava = DEFAULT, killer = DEFAULT
     WHERE player_id IN (SELECT id FROM players WHERE match_id = mid);
     -- Delete the messages in the match
     DELETE FROM messages WHERE match_id = mid;
  END;$$
LANGUAGE plpgsql;

-- Replay a specific match.
CREATE OR REPLACE FUNCTION replay_match(mid INTEGER) RETURNS void AS $$
  DECLARE newmid INTEGER;
  BEGIN
    SELECT latest_match() INTO newmid;
    INSERT INTO messages (match_id, TYPE, json, timestamp)
      SELECT newmid, TYPE, json, NOW() FROM messages
       WHERE match_id = mid
         AND type IN ('kill', 'round_start', 'round_end', 'match_start', 'match_end')
       ORDER BY ID ASC;
    RAISE NOTICE 'Replayed previous match % for %', mid, newmid;
  END; $$
LANGUAGE plpgsql;

-- Same as above, but pick a random match.
CREATE OR REPLACE FUNCTION replay_match() RETURNS void AS $$
  BEGIN
    PERFORM replay_match(replayable_match());
  END; $$
LANGUAGE plpgsql;

-- Replay a set amount of matches
CREATE OR REPLACE FUNCTION replay_matches(amount INTEGER) RETURNS void AS $$
  BEGIN
    FOR i IN 1..amount LOOP
      PERFORM replay_match();
    END LOOP;
  END; $$
LANGUAGE plpgsql;

-- Grab a random replayable match from a real tournament.
CREATE OR REPLACE FUNCTION replayable_match() RETURNS integer AS $$
  DECLARE mkind match_kind;
  BEGIN
    -- Select the kind of the current match as to match the thing we're trying
    -- to replay.
    SELECT m.kind FROM matches m WHERE ID = latest_match() INTO mkind;
    RETURN (
      SELECT ID FROM matches
       WHERE tournament_id = real_tournament()
         AND kind = mkind
       ORDER BY random() LIMIT 1);
  END; $$
LANGUAGE plpgsql;

-- Create the next matches, and move players to them.
CREATE OR REPLACE FUNCTION post_match_handling(mid INTEGER) RETURNS void AS $$
  DECLARE kind match_kind;
  DECLARE tid INTEGER;
  DECLARE newpid INTEGER;
  DECLARE newmid INTEGER;
  BEGIN
    SELECT tournament_id FROM matches WHERE ID = mid INTO tid;
    SELECT m.kind FROM matches m WHERE ID = mid INTO kind;
    -- Start with updating the tournament level scores for the players in the match
    PERFORM set_match_scores(mid);
    PERFORM update_summary(tid, p.person_id) FROM players P WHERE p.match_id = mid;
    -- Then do handling based on the match kinds
    IF (kind = 'qualifying') THEN
      IF NOT qualifying_ended(tid) THEN
        -- If we are still doing qualifiers, then we need to add another match
        -- and schedule the next runnerups to be in that match.
        INSERT INTO matches (tournament_id, kind, LENGTH)
          VALUES (tid, 'qualifying', 10)
          RETURNING ID INTO newmid;
        -- We also need to populate that match with runnerup players
        INSERT INTO players (match_id, person_id)
          SELECT newmid, person_id FROM runnerups() LIMIT 4;
      END IF;
      -- Set the qualifying positions of everyone
      PERFORM set_qualifying_positions(tid);
      -- If we are past the time of qualifying matches, then we need to
      -- either schedule the endgame or play the last few matches;
      IF all_matches_finished(tid, 'qualifying') THEN
        -- Aight, so we are done! First, insert the playoff matches...
        PERFORM schedule_short_playoff(tid, '{1,12,2,11}');
        PERFORM schedule_short_playoff(tid, '{3,10,4,9}');
        PERFORM schedule_short_playoff(tid, '{5,6,7,8}'); -- My romeo, romeo! :dancer:
        -- Then insert the last-chance match, but without players for now...
        INSERT INTO matches (tournament_id, kind, LENGTH, title)
          VALUES (tid, 'playoff', 10, 'Last Chance Match');
        -- ...and finally, the final. The final doesn't need any players - they
        -- will be populated as the playthroughs are finished
        INSERT INTO matches (tournament_id, kind, LENGTH)
          VALUES (tid, 'final', 20);
      END IF;
    ELSIF (kind = 'playoff') THEN
      -- For the playoffs, move the winner into the final
      INSERT INTO players (match_id, person_id)
        SELECT final_id(tid), p.person_id FROM players P
         WHERE match_id = mid
         ORDER BY p.kills DESC
         LIMIT 1
         RETURNING person_id INTO newpid;
      RAISE NOTICE 'Sending % to the final for winning', person_nick(newpid);
      -- And also move the second-place into the last-chance match, but only if
      -- we didn't just play that match.
      IF (mid != last_chance_id(tid)) THEN
        INSERT INTO players (match_id, person_id)
          SELECT last_chance_id(tid), p.person_id FROM players P
           WHERE match_id = mid
           ORDER BY p.kills DESC, p.sweeps DESC, p.SELF ASC, p.total_score DESC
           LIMIT 1 OFFSET 1
           RETURNING person_id INTO newpid;
        RAISE NOTICE 'Sending % to the last-chance for getting second', person_nick(newpid);
        -- Once we are done with the three playoffs and we are yet to add players
        -- to the last match, we want to add it and the last player into it
        IF all_matches_finished(tid, 'playoff') THEN
          -- Use the runnerups algorithm to grab the last worthy player from the
          -- playoffs that did not make it to the final yet. Basically, a
          -- last-chance match.
          INSERT INTO players (match_id, person_id)
              SELECT last_chance_id(tid), person_id FROM playoff_runnerups(tid)
               LIMIT 1
               RETURNING person_id INTO newpid;
          RAISE NOTICE 'Sending % as backfill to the last-chance', person_nick(newpid);
        END IF;
      END IF;
    ELSIF (kind = 'final') THEN
      -- If we've just ended the final, then we should close the tournament
      UPDATE tournaments SET ended = NOW() WHERE ID = tid;
      RAISE NOTICE 'Tournament % ended', tid;
    END IF;
  END; $$
LANGUAGE plpgsql;

-- Trigger of the above function
CREATE OR REPLACE FUNCTION match_end() RETURNS TRIGGER AS $$
  BEGIN
    RAISE NOTICE 'Running match_end for %', NEW.ID;
    PERFORM post_match_handling(NEW.ID);
    NOTIFY drunkenfall, 'match_end';
    RETURN NEW;
  END; $$
LANGUAGE plpgsql;

-- Trigger the above once a match is set as ended.
DROP TRIGGER on_match_end ON matches;
CREATE TRIGGER on_match_end
  AFTER UPDATE ON matches
  FOR EACH ROW
  WHEN (OLD.ended IS NULL AND NEW.ended IS NOT NULL)
  EXECUTE PROCEDURE match_end();

-- Get the latest match in the latest tournament.
CREATE OR REPLACE FUNCTION latest_match() RETURNS INTEGER AS $$
  BEGIN
    RETURN (
      SELECT ID FROM matches M
       WHERE m.ended IS NULL
         AND m.tournament_id = latest_tournament()
       -- ASC looks weird, but it's what we want since otherwise
       -- we'll start the scheduled +1 matches.
       ORDER BY ID ASC LIMIT 1);
  END; $$
LANGUAGE plpgsql;


-- Schedule one of the playoff matches.
--
-- Since we want to bucket the players so that the leader goes to match 1, the
-- runnerup to match 2, the third to match 3 et.c., it helps if we have a helper
-- function for this.
-- The `mod` argument should correspond to which playoff it is, 1-indexed. So,
-- for the leader match, the argument should be 1. This also means that the
-- last match needs `mod` to be 0.
CREATE OR REPLACE FUNCTION schedule_playoff(tid INTEGER, mod INTEGER, runnerups INTEGER) RETURNS void AS $$
  DECLARE mid INTEGER;
  BEGIN
    -- First, insert the match
    INSERT INTO matches (tournament_id, kind, LENGTH)
      VALUES (tid, 'playoff', 10)
      RETURNING ID INTO mid;
    -- Then, insert the players. Grab them by checking the runnerup overview and
    -- selecting every fourth player based on the offset provided.
    INSERT INTO players (match_id, person_id)
         SELECT mid, T.person_id FROM (
                      SELECT person_id, ROW_NUMBER() OVER(ORDER BY skill_score DESC) AS INDEX
                        FROM overview(tid) LIMIT runnerups) AS T
          WHERE T.INDEX % 4 = mod;
  END; $$
LANGUAGE plpgsql;

-- Schedule a 12-player playoff.
--
-- Since we don't have perfectly distributable players (12 instead of 16) we do
-- a different algorithm to determine positions. Just placing the top players
-- against each other would make for a great first match, but probably a pretty
-- dull third one.
--
-- Instead, we select the first, then the last, then the second, then the second
-- last etc, rotating inversely. There is probably a neat way to do so
-- algorithmically, but since we only have the 12 it's easier to just hard code them.
-- See the callees.
CREATE OR REPLACE FUNCTION schedule_short_playoff(tid INTEGER, positions INTEGER[]) RETURNS void AS $$
  DECLARE mid INTEGER;
  BEGIN
    -- Insert the match
    INSERT INTO matches (tournament_id, kind, LENGTH)
      VALUES (tid, 'playoff', 10)
      RETURNING ID INTO mid;
    -- Then, insert the players.
    INSERT INTO players (match_id, person_id)
         SELECT mid, T.person_id FROM (
                      SELECT person_id, ROW_NUMBER() OVER(ORDER BY skill_score DESC) AS INDEX
                        FROM overview(tid)) AS T
            WHERE INDEX = ANY(positions);
  END; $$
LANGUAGE plpgsql;

-- Return if a match has a full set of players or not
CREATE OR REPLACE FUNCTION has_players(mid INTEGER) RETURNS BOOLEAN AS $$
  BEGIN
    RETURN (SELECT COUNT(*) = 4
              FROM players
             WHERE match_id = mid);
  END; $$
LANGUAGE plpgsql;

-- Shorthand for easy access
CREATE OR REPLACE FUNCTION final_id(tid INTEGER) RETURNS INTEGER AS $$
  BEGIN
    RETURN (SELECT ID FROM matches WHERE kind = 'final' AND tournament_id = tid);
  END; $$
LANGUAGE plpgsql;

-- Shorthand for easy access
CREATE OR REPLACE FUNCTION last_chance_id(tid INTEGER) RETURNS INTEGER AS $$
  BEGIN
    RETURN (SELECT ID FROM matches
              WHERE kind = 'playoff' AND tournament_id = tid
              ORDER BY ID DESC
              LIMIT 1);
  END; $$
LANGUAGE plpgsql;

-- For pretty-printing the scores
CREATE OR REPLACE FUNCTION match_name(mid INTEGER) RETURNS text AS $$
  DECLARE mkind match_kind;
  DECLARE tid INTEGER;
  BEGIN
    SELECT INTO mkind, tid
                m.kind, m.tournament_id
       FROM matches m WHERE ID = mid;
    IF (mkind = 'final') THEN
      RETURN 'Final';
    END IF;
    RETURN (
      SELECT initcap(mkind::TEXT)
             || ' ' ||
             (SELECT pos FROM (SELECT ROW_NUMBER() OVER(ORDER BY ID ASC) AS pos, ID
                FROM matches
                WHERE tournament_id = tid
                  AND kind = mkind
                ORDER BY ID ASC) AS T
              WHERE ID = mid)
    );
  END $$
LANGUAGE plpgsql;

ALTER TABLE matches
  ADD COLUMN title TEXT;

-- The name setting needs to be an AFTER trigger since it needs to lookup the
-- table to determine what the actual name would be.
CREATE TRIGGER set_name_on_insert
  AFTER INSERT ON matches
  FOR EACH ROW
  EXECUTE PROCEDURE set_match_name();

-- Automatically set the name of the match as it is inserted
CREATE OR REPLACE FUNCTION set_match_name() RETURNS TRIGGER AS $$
  BEGIN
    UPDATE matches
      SET title = match_name(NEW.ID)
      WHERE ID = NEW.ID
        AND title IS NULL;
    RETURN NEW;
  END; $$
LANGUAGE plpgsql;

-- Also add rounds to the matches table again
ALTER TABLE matches
  ADD COLUMN round BIGINT NOT NULL DEFAULT 1;

-- Update the `round' field of the match whenever a new round is inserted
CREATE TRIGGER update_match_round
  AFTER INSERT ON rounds
  FOR EACH ROW
  EXECUTE PROCEDURE update_match_round();

-- Automatically set the name of the match as it is inserted
CREATE OR REPLACE FUNCTION update_match_round() RETURNS TRIGGER AS $$
  BEGIN
    UPDATE matches SET round = t.rounds
      FROM (
        SELECT match_id, COUNT(ID) AS rounds
          FROM rounds
          WHERE match_id = NEW.match_id
          GROUP BY match_id) AS T
      WHERE ID = NEW.match_id;
    RETURN NEW;
  END; $$
LANGUAGE plpgsql;
