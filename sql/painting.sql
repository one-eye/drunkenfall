SELECT tournament_id, SUM(SELF) AS count FROM player_summaries GROUP BY tournament_id;

-- Summarizing a single field of the player summaries table
--
-- Also calculating what it probably should have been in non-app tournaments
-- based on the average from the app ones.
WITH ps AS (
  SELECT tournament_id,
         SUM(total_score) AS COUNT
    FROM player_summaries
    GROUP BY tournament_id
  )
  SELECT
    (SUM(COUNT) + (AVG(COUNT) * 10))::INTEGER AS total,
    SUM(COUNT) AS app,
    (AVG(COUNT) * 10)::INTEGER AS OLD
  FROM ps;

-- Similar to above, but for amount of arrows fired

-- First find out the average amount of matches per tournament (it's 12.2). Use
-- this result times 10 (122) as multiplier for arrows
WITH T AS (SELECT tournament_id, COUNT(*) AS C FROM matches GROUP BY tournament_id)
  SELECT AVG(C) FROM T;

-- Then count from the messages table
WITH msg AS (
  SELECT match_id,
         COUNT(id) AS COUNT
    FROM messages
    WHERE TYPE = 'arrow_shot'
    GROUP BY match_id
  )
  SELECT
    (SUM(COUNT) + (AVG(COUNT) * 122))::INTEGER AS total,
    SUM(COUNT) AS app,
    (AVG(COUNT) * 122)::INTEGER AS OLD
  FROM msg;

-- Similar to above, but with the amount of rounds
WITH r AS (
  SELECT match_id,
         COUNT(*) AS COUNT
    FROM rounds
    GROUP BY match_id
  )
  SELECT
    (SUM(COUNT) + (AVG(COUNT) * 122))::INTEGER AS total,
    SUM(COUNT) AS app,
    (AVG(COUNT) * 122)::INTEGER AS OLD
  FROM r;

-- Shots per match per tournament
SELECT t.NAME AS tournament, m.title AS match, SUM AS shots, players
  FROM (
    SELECT m.ID AS match_id,
           m.tournament_id,
           SUM(shots) AS SUM,
           string_agg(ps.nick, ', ') AS players
      FROM players P
      INNER JOIN matches M ON m.ID = p.match_id
      INNER JOIN people ps ON p.person_id = ps.id
      GROUP BY m.id, m.tournament_id
      ORDER BY SUM DESC
    ) AS tmp
    FULL OUTER JOIN tournaments T ON t.ID = tournament_id
    INNER JOIN matches M ON m.ID = match_id
    LIMIT 20;

-- Same as above, but grouped as a ratio into different
SELECT t.ID,
       t.NAME,
       m.kind,
       ROUND(AVG(SUM), 2) AS shots_avg
  FROM (
    SELECT match_id, m.tournament_id, SUM(shots) AS SUM
      FROM players P
      INNER JOIN matches M ON m.ID = p.match_id
      INNER JOIN people ps ON p.person_id = ps.id
      GROUP BY match_id, m.tournament_id
    ) AS tmp
  FULL OUTER JOIN tournaments T ON t.ID = tournament_id
  INNER JOIN matches M ON m.ID = match_id
  GROUP BY T.ID, T.NAME, kind
  ORDER BY t.ID;
