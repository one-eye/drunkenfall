DROP TABLE people_auth;
CREATE TABLE people_auth (
    ID SERIAL PRIMARY KEY,
    person_id INTEGER REFERENCES people(ID) ON DELETE CASCADE,
    provider TEXT NOT NULL,
    remote_id TEXT NOT NULL,
    avatar_url TEXT NOT NULL,
    nick TEXT NOT NULL,
    name TEXT,
    access_token TEXT
);

-- Connect a person_auth (pa) to a user. This is useful when someone that has
-- been registered in the app before wants to connect their Discord account.
--
-- Note that this will try to remove the person that the registration process
-- created, but it will fail in doing so if the user has already participated in
-- a tournament. This should never happen, but you never know.
CREATE OR REPLACE FUNCTION connect_discord(pid INTEGER, paid INTEGER) RETURNS void AS $$
  DECLARE played_tournaments INTEGER;
  DECLARE other_person_id INTEGER;
  BEGIN
    SELECT person_id FROM people_auth WHERE ID = paid INTO other_person_id;
    SELECT COUNT(ID) FROM player_summaries WHERE person_id = other_person_id INTO played_tournaments;
    UPDATE people_auth SET person_id = pid WHERE ID = paid;
    -- Also update the existing person object to have the Discord avatar
    UPDATE people p SET avatar_url = pa.avatar_url
      FROM people_auth pa
      WHERE pa.ID = paid AND p.ID = pid;
    IF (played_tournaments != 0) THEN
      UPDATE people SET disabled = TRUE WHERE ID = other_person_id;
      RAISE NOTICE 'Cannot delete person % because they have been in tournaments; disabling them', other_person_id;
    ELSE
      DELETE FROM people WHERE ID = other_person_id;
      RAISE NOTICE 'Deleted %', other_person_id;
    END IF;
  END;$$
LANGUAGE plpgsql;
