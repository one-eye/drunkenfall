-- Actions after a player is removed from a tournament
--
-- If the player is scheduled in any matches, remove them from those matches and
-- replace them with the most worthy non-scheduled player.
CREATE OR REPLACE FUNCTION eject_from_tournament(tid INTEGER, pid INTEGER) RETURNS void AS $$
  DECLARE mid INTEGER;
  DECLARE matches INTEGER;
  DECLARE mkind match_kind;
  DECLARE replacement INTEGER;
  BEGIN
    -- Figure out if they were in any matches that are scheduled but have
    -- not been played yet.
    SELECT COUNT(*) FROM matches m
     INNER JOIN players P ON p.match_id = m.id
     WHERE m.tournament_id = tid
       AND p.person_id = pid
       AND m.ended IS NULL
      INTO matches;
    -- Check if they actually were...
    IF matches != 0 THEN
      RAISE NOTICE 'Deleted player % was in % matches', person_nick(pid), matches;
      -- ...and if so, first delete the existing player object...
      DELETE FROM players P
        WHERE p.person_id = pid
          AND p.match_id IN (
             SELECT ID FROM matches
              WHERE tournament_id = tid
                AND ended IS NULL)
        RETURNING p.match_id INTO mid;
      -- ...then decide on who the replacement should be
      SELECT kind FROM matches WHERE id = mid INTO mkind;
      RAISE NOTICE 'Backfilling into % (% %)', mid, mkind, (SELECT INDEX FROM matches WHERE ID = mid);
      IF (mkind = 'qualifying') THEN
        -- If it's qualifying, just do the next runnerup
        SELECT r.pid FROM runnerups(tid) AS r LIMIT 1 INTO replacement;
      ELSE
        SELECT get_endgame_backfill(tid, mkind) INTO replacement;
      END IF;
      -- ...and then insert the next non-scheduled player, based on the
      -- runnerup algorithms
      INSERT INTO players (match_id, person_id, color, preferred_color, archer_type, nick, index)
        SELECT mid, p.id, p.preferred_color,
                          p.preferred_color,
                          p.archer_type,
                          p.nick,
                          first_available_index(mid)
          FROM people P
         WHERE p.ID = replacement;
       RAISE NOTICE 'Replaced % with %', person_nick(pid), person_nick(replacement);
    END IF;
  END $$
LANGUAGE plpgsql;

-- Trigger of the above function
CREATE OR REPLACE FUNCTION remove_player() RETURNS TRIGGER AS $$
  BEGIN
    -- If there is a queue, then we should backfill from it!
    IF (SELECT COUNT(*) > 0 FROM player_queue WHERE tournament_id = OLD.tournament_id) THEN
      PERFORM backfill_from_queue(OLD.tournament_id);
    END IF;
    -- Then do the ejection procedure
    PERFORM eject_from_tournament(OLD.tournament_id, OLD.person_id);
    RETURN NEW;
  END; $$
LANGUAGE plpgsql;

CREATE TRIGGER on_remove_player
  AFTER DELETE ON player_summaries
  FOR EACH ROW
  WHEN (tournament_exists(OLD.tournament_id))
  EXECUTE PROCEDURE remove_player();

CREATE OR REPLACE FUNCTION first_available_index(mid integer) RETURNS INTEGER AS $$
  BEGIN
    RETURN CASE
      WHEN (SELECT COUNT(*) = 0 FROM players WHERE match_id = mid AND INDEX = 0) THEN 0
      WHEN (SELECT COUNT(*) = 0 FROM players WHERE match_id = mid AND INDEX = 1) THEN 1
      WHEN (SELECT COUNT(*) = 0 FROM players WHERE match_id = mid AND INDEX = 2) THEN 2
      ELSE 3
    END;
  END $$
LANGUAGE plpgsql;

-- After the playoffs have started, we need to use a modified version of the
-- runnerup algorithm to find the next worthy person that was never inserted
-- into the playoffs (I.e., pretty much the player in position 17)
CREATE OR REPLACE FUNCTION get_endgame_backfill(tid INTEGER, mkind match_kind) RETURNS INTEGER AS $$
  BEGIN
    RETURN (SELECT ps.person_id FROM player_summaries ps
             WHERE ps.tournament_id = tid
               AND ps.person_id NOT IN (
                   SELECT p.person_id
                     FROM players P INNER JOIN matches M ON m.ID = p.match_id
                     WHERE m.kind = mkind AND m.tournament_id = tid)
             ORDER BY ps.skill_score DESC, ps.ID ASC
             LIMIT 1);
  END $$
LANGUAGE plpgsql;

-- If someone is removed, we are still doing qualifyers, and there are people
-- in the queue - it's time to move them from the queue! Pick the top one, of course.
CREATE OR REPLACE FUNCTION backfill_from_queue(tid INTEGER) RETURNS void AS $$
  DECLARE pid INTEGER;
  BEGIN
    DELETE FROM player_queue
      WHERE tournament_id = tid
      AND ID IN (SELECT id FROM player_queue ORDER BY ID ASC LIMIT 1)
      RETURNING person_id INTO pid;
    INSERT INTO player_summaries (tournament_id, person_id) SELECT tid, pid;
    RAISE NOTICE '% has been moved from the queue to the tournament', (SELECT nick FROM people WHERE ID = pid);
  END $$
LANGUAGE plpgsql;
