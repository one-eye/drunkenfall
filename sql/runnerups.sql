-- Prints the list of players eligible for runnerup in the given
-- tournament. Excludes those already booked into matches that haven't
-- ended yet (i.e. scheduled matches)
DROP FUNCTION runnerups(INTEGER);
CREATE OR REPLACE FUNCTION runnerups(tid INTEGER) RETURNS
  SETOF summary AS $$
  BEGIN
   RETURN QUERY (
     SELECT
       ROW_NUMBER() OVER(ORDER BY ps.matches ASC, ps.skill_score DESC, ps.ID ASC),
       ps.ID,
       ps.person_id,
       ps.tournament_id,
       person_nick(ps.person_id),
       ps.kills,
       ps.sweeps,
       ps.SELF,
       ps.shots,
       ps.matches,
       ps.total_score,
       ps.skill_score
     FROM player_summaries ps
     WHERE ps.tournament_id = tid
       AND ps.person_id NOT IN (
         SELECT p.person_id
           FROM players P INNER JOIN matches M ON m.ID = p.match_id
           WHERE m.ended IS NULL AND m.tournament_id = tid
       )
     ORDER BY ps.matches ASC, ps.skill_score DESC, ps.ID ASC);
  END $$
LANGUAGE plpgsql;

-- Same as above, but for latest tournament only
DROP FUNCTION runnerups();
CREATE OR REPLACE FUNCTION runnerups() RETURNS
  SETOF summary AS $$
  BEGIN
   RETURN QUERY SELECT * FROM runnerups(latest_tournament());
  END $$
LANGUAGE plpgsql;

-- Playoff runnerups, to show who might be in the last-chance match
DROP FUNCTION playoff_runnerups(INTEGER);
CREATE OR REPLACE FUNCTION playoff_runnerups(tid INTEGER) RETURNS
  SETOF summary AS $$
  BEGIN
   RETURN QUERY (
     SELECT ROW_NUMBER() OVER(ORDER BY p.kills DESC, p.sweeps DESC, p.SELF ASC, p.total_score DESC),
            p.ID,
            p.person_id,
            tid,
            player_nick(p.ID),
            p.kills,
            p.sweeps,
            p.SELF,
            p.shots,
            -1::SMALLINT AS matches,
            p.total_score,
            -1 AS skill_score
     FROM players P
     INNER JOIN matches M ON m.ID = p.match_id
     WHERE m.ended IS NOT NULL
       AND m.kind = 'playoff'
       AND m.tournament_id = tid
       AND p.person_id NOT IN (
         -- Remove those that are already scheduled
         SELECT p.person_id
           FROM players P INNER JOIN matches M ON m.ID = p.match_id
           WHERE m.ID IN (last_chance_id(tid), final_id(tid))
         )
     -- And then order them not by score, but by kills sweeps and selfs and
     -- then finally total_score, just in case someone for some reason has exactly
     -- the same thing otherwise.
     ORDER BY p.kills DESC, p.sweeps DESC, p.SELF ASC, p.total_score DESC
   );
  END $$
LANGUAGE plpgsql;

-- Same as above, but for latest tournament only
DROP FUNCTION playoff_runnerups();
CREATE OR REPLACE FUNCTION playoff_runnerups() RETURNS
  SETOF summary AS $$
  BEGIN
   RETURN QUERY SELECT * FROM playoff_runnerups(latest_tournament());
  END $$
LANGUAGE plpgsql;
