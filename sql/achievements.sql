-- Returns all the ids of matches that two players fought in
CREATE OR REPLACE FUNCTION matches_fought(p1 INTEGER, p2 integer) RETURNS
  TABLE(match_id INTEGER) AS $$
  BEGIN
    RETURN query (
        SELECT p.match_id AS mid FROM players P WHERE p.person_id IN (p1)
        INTERSECT
        SELECT p.match_id AS mid FROM players P WHERE p.person_id IN (p2)
    );
  END; $$
LANGUAGE plpgsql;

-- Returns all the ids of tournaments that two players fought in
CREATE OR REPLACE FUNCTION tournaments_fought(p1 INTEGER, p2 INTEGER) RETURNS
  TABLE(tournament_id INTEGER) AS $$
  BEGIN
    RETURN query (
        SELECT m.tournament_id FROM matches m
         WHERE ID IN (SELECT match_id FROM matches_fought(p1, p2))
    );
  END; $$
LANGUAGE plpgsql;

-- Returns all the ids of tournaments that two players fought in
CREATE OR REPLACE FUNCTION tournaments_between(p1 INTEGER, p2 integer) RETURNS INTEGER AS $$
  BEGIN
    RETURN (
        SELECT COUNT(DISTINCT m.tournament_id) FROM matches m
         WHERE ID IN (SELECT match_id FROM matches_fought(p1, p2))
    );
  END; $$
LANGUAGE plpgsql;

-- Returns how many times one player has killed another
CREATE OR REPLACE FUNCTION times_killed(killer INTEGER, killed integer) RETURNS
  INTEGER AS $$
  BEGIN
    RETURN (SELECT SUM(times_killed_in_match(t.match_id,
                       killer, killed))
              FROM (SELECT * FROM matches_fought(killer, killed)) AS T);
  END; $$
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION times_killed_in_match(mid INTEGER, killer INTEGER, killed integer) RETURNS
  INTEGER AS $$
  BEGIN
    RETURN (SELECT COUNT(*)
              FROM messages msg
             WHERE
               msg.match_id = mid
               AND msg.TYPE = 'kill'
               AND CAST(msg.json->>'killer' AS INTEGER) = (
                   SELECT INDEX FROM players P
                    WHERE p.match_id = mid
                      AND p.person_id = killer)
               AND CAST(msg.json->>'player' AS INTEGER) = (
                   SELECT INDEX FROM players P
                    WHERE p.match_id = mid
                      AND p.person_id = killed)
    );
  END; $$
LANGUAGE plpgsql;


CREATE OR REPLACE FUNCTION self_by_reason_in_match(mid INTEGER, pid INTEGER) RETURNS
  TABLE(reason reason, times integer) AS $$
  BEGIN
    RETURN (SELECT COUNT(*)
              FROM messages msg
             WHERE
               AND msg.TYPE = 'kill'
               AND CAST(msg.json->>'killer' AS INTEGER) = (
                   SELECT INDEX FROM players P
                    WHERE p.match_id = mid
                      AND p.person_id = killer)
               AND CAST(msg.json->>'player' AS INTEGER) = (
                   SELECT INDEX FROM players P
                    WHERE p.match_id =s mid
                      AND p.person_id = killed)
    );
  END; $$
LANGUAGE plpgsql;


-- Returns an unordered unique set of person combinations
--
-- So, players x and y will only be returned as `x vs y`, but not as `y vs x`.
-- This is useful whenever the order of the players don't matter, i.e. when
-- checking how many tournaments two people have participated in together.
CREATE OR REPLACE FUNCTION people_combinations() RETURNS
  TABLE(p1 INTEGER, p2 INTEGER) AS $$
  BEGIN
    RETURN query (
        WITH combo AS (SELECT ID FROM people WHERE NOT disabled)
           SELECT p1.ID, p2.ID
             FROM combo p1
            CROSS JOIN combo p2
            WHERE p1.ID < p2.ID
    );
  END; $$
LANGUAGE plpgsql;


-- Returns all person combinations
--
-- This is the greedier version of the function above, and it returns all
-- combinations of players. i.e. both `x vs y` and `y vs x`.
-- This is useful when order matters, as in to checking how many times x killed
-- y but also how many times y killed x.
-- This DOES include `x vs x`.
CREATE OR REPLACE FUNCTION people_bilateral_combinations() RETURNS
  TABLE(p1 INTEGER, p2 INTEGER) AS $$
  BEGIN
    RETURN query (
        WITH combo AS (SELECT ID FROM people WHERE NOT disabled)
           SELECT p1.ID, p2.ID
             FROM combo p1
            CROSS JOIN combo p2
    );
  END; $$
LANGUAGE plpgsql;

-- Returns the person id based on a a player index in a match
CREATE OR REPLACE FUNCTION person_index_in_match(mid INTEGER, idx INTEGER) RETURNS
  INTEGER AS $$
  BEGIN
    RETURN (
       SELECT person_id FROM players
        WHERE match_id = mid
          AND INDEX = idx
        LIMIT 1 -- What the actual
    );
  END; $$
LANGUAGE plpgsql;


-- Returns the nick of a person based on their summary
CREATE OR REPLACE FUNCTION person_nick(pid INTEGER) RETURNS
  TEXT AS $$
  BEGIN
    RETURN (
       SELECT nick FROM people
        WHERE id = pid
        LIMIT 1 -- What the actual
    );
  END; $$
LANGUAGE plpgsql;

-- Returns the nick of a person based on a player object
CREATE OR REPLACE FUNCTION player_nick(pid INTEGER) RETURNS TEXT AS $$
  BEGIN
    RETURN (
       SELECT p.nick FROM players pl
         INNER JOIN people p ON p.ID = pl.person_id
         WHERE pl.id = pid
         LIMIT 1
    );
  END; $$
LANGUAGE plpgsql;



-- Return the toplist of players killing other players.
SELECT *, round(kills::numeric / tournaments::NUMERIC, 2) AS ratio
         FROM (SELECT person_nick(p1) AS killer,
                      person_nick(p2) AS victim,
                      times_killed(p1, p2) AS kills,
                      tournaments_between(p1, p2) AS tournaments
                FROM people_bilateral_combinations()
                -- WHERE p1 != p2
                ) AS t
  WHERE kills IS NOT NULL
  ORDER BY kills DESC
  LIMIT 30;

-- Return the toplist of players competing in the same tournament.
SELECT * FROM (
       SELECT person_nick(p1),
              person_nick(p2),
              tournaments_between(p1, p2) AS tournaments
         FROM people_combinations()) AS T
 WHERE tournaments >= 3
 ORDER BY tournaments DESC;

-- Return the toplist of suicides causes
SELECT person_nick(person_index_in_match(match_id, idx)) AS nick,
       reason,
       COUNT(reason) AS times FROM (
    SELECT match_id,
           CAST(json->>'player' AS INTEGER) AS idx,
           kill_reason(CAST(json->>'cause' AS INTEGER)) AS reason
      FROM messages
     WHERE TYPE = 'kill'
       AND CAST(json->>'killer' AS INTEGER) = CAST(json->>'player' AS INTEGER)
       AND CAST(json->>'player' AS INTEGER) != -1) AS T
  WHERE person_nick(person_index_in_match(match_id, idx)) IS NOT NULL
  --AND person_nick(person_index_in_match(match_id, idx)) IN (SELECT nick FROM people WHERE userlevel = 100)
  GROUP BY nick, reason
  ORDER BY times DESC LIMIT 20;
