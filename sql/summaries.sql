CREATE TYPE summary AS (
  position BIGINT,
  summary_id INTEGER,
  person_id INTEGER,
  tournament_id INTEGER,
  nick TEXT,
  kills SMALLINT,
  sweeps SMALLINT,
  self SMALLINT,
  shots SMALLINT,
  matches SMALLINT,
  total_score INTEGER,
  skill_score INTEGER
);

CREATE TYPE score AS (
  tournament_id INTEGER,
  match_id INTEGER,
  title TEXT,
  player_id INTEGER,
  nick TEXT,
  color color,
  kills SMALLINT,
  sweeps SMALLINT,
  self SMALLINT,
  shots SMALLINT,
  total_score INTEGER,
  match_score INTEGER
);

-- TODO(thiderman): Make these use new types
 -- public | leaderboard       | TABLE(pos bigint, nick text, id integer, league_id integer, pid integer, shots bigint, sweeps bigint, kills bigint, self bigint, matches bigint, tournaments bigint, total_score bigint, skill_score bigint)

 -- public | states            | TABLE(id integer, mid integer, pid integer, nick text, color color, arrows integer[], shield boolean, wings boolean, hat boolean, invisible boolean, speed boolean, alive boolean, lava boolean, killer integer)

CREATE TABLE tournament_player_summaries OF summary (
  UNIQUE (summary_id, person_id, tournament_id)
);

-- Add a cascading delete for the TPSs so that when tournaments are deleted, so
-- are their summary records.
ALTER TABLE tournament_player_summaries
  ADD CONSTRAINT tournament_fk
  FOREIGN KEY (tournament_id)
  REFERENCES tournaments (ID)
  ON DELETE CASCADE;

CREATE OR REPLACE FUNCTION set_summaries_after_tournament() RETURNS TRIGGER AS $$
 BEGIN
   RAISE NOTICE 'Inserting tournament summaries for %', NEW.ID;
   INSERT INTO tournament_player_summaries SELECT * FROM overview(NEW.ID);
   RETURN NEW;
 END;$$
LANGUAGE plpgsql;

DROP TRIGGER on_tournament_end_set_summaries ON tournaments;
CREATE TRIGGER on_tournament_end_set_summaries
  AFTER UPDATE ON tournaments
  FOR EACH ROW
  WHEN (OLD.ended IS NULL AND NEW.ended IS NOT NULL)
  EXECUTE PROCEDURE set_summaries_after_tournament();

-- A view where we return the highest position a player has gotten in a
-- tournament, as well as how many times they have gotten it. This is presented
-- to viewers as an indication of general skill level of the player.
CREATE OR REPLACE VIEW highest_positions AS
  WITH grouped_positions AS (
    -- First, create a CTE where we have calculated all the positions a player has
    -- gotten, and also how many times they have gotten it.
    -- This is so that we later on can grab just the highest one per player.
    SELECT MAX(POSITION) AS position, COUNT(*) AS times, person_id
      FROM tournament_player_summaries
      GROUP BY POSITION, person_id
      ORDER BY POSITION ASC, times DESC
  )
    SELECT gp.*
      FROM grouped_positions gp
      -- Then, utilize a LEFT OUTER JOIN with a conditional in the ON clause. This
      -- effectively only sets gp2.person_id (the joining statement) on the row
      -- that matches the conditional, meaning that only the row with the best
      -- position will have their person_id set on both tables.
      -- See https://stackoverflow.com/a/7745635
      LEFT OUTER JOIN grouped_positions gp2
        ON gp.person_id = gp2.person_id
          AND gp.POSITION > gp2.POSITION
      WHERE gp2.person_id IS NULL;
