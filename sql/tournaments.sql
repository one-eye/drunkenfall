-- Clean up the join tables for levels and nicknames once a tournament
-- is done
CREATE OR REPLACE FUNCTION cleanup_after_tournament() RETURNS TRIGGER AS $$
 BEGIN
   DELETE FROM tournament_levels WHERE tournament_id = NEW.id;
   DELETE FROM tournament_names WHERE tournament_id = NEW.id;
   RETURN NEW;
 END;$$
LANGUAGE plpgsql;

-- Set the trigger to only happen when the ended date is set
CREATE TRIGGER on_tournament_end_cleanup
  AFTER UPDATE ON tournaments
  FOR EACH ROW
  WHEN (NEW.ended IS NOT NULL)
  EXECUTE PROCEDURE cleanup_after_tournament();

-- Helper to create the first two matches when a tournament starts
CREATE OR REPLACE FUNCTION create_first_matches() RETURNS TRIGGER AS $$
  DECLARE mid INTEGER;
  BEGIN
    FOR I IN 1..2 LOOP
      RAISE NOTICE 'Creating start match %', I;
      INSERT INTO matches (tournament_id, kind) VALUES (NEW.ID, 'qualifying')
        RETURNING ID INTO mid;
      INSERT INTO players (match_id, person_id)
             SELECT mid, person_id FROM runnerups(NEW.ID) LIMIT 4;
    END LOOP;
    -- Also set the qualifying positions for the players.
    PERFORM set_qualifying_positions(NEW.ID);
    RETURN NEW;
  END;$$
LANGUAGE plpgsql;

-- Set the trigger to only happen when the ended date is set
CREATE TRIGGER on_tournament_start_create_matches
  AFTER UPDATE ON tournaments
  FOR EACH ROW
  WHEN (OLD.started IS NULL AND NEW.started IS NOT NULL)
  EXECUTE PROCEDURE create_first_matches();


-- Get the running tournament
CREATE OR REPLACE FUNCTION latest_tournament() RETURNS integer AS $$
 BEGIN
   RETURN (SELECT ID FROM tournaments ORDER BY ID DESC LIMIT 1);
 END;$$
LANGUAGE plpgsql;

-- Return whether the tournament is still doing qualifying matches or not
CREATE OR REPLACE FUNCTION qualifying_ended(tid INTEGER) RETURNS BOOLEAN AS $$
 BEGIN
   -- We need the first check for NOT NULL for the time where the time simply
   -- has not been set yet.
   RETURN (SELECT qualifying_end IS NOT NULL
                  AND now() > qualifying_end AS passed
             FROM tournaments WHERE ID = tid);
 END;$$
LANGUAGE plpgsql;

-- Return whether we have finished playing a category of matches or not.
CREATE OR REPLACE FUNCTION all_matches_finished(tid INTEGER, mkind match_kind) RETURNS BOOLEAN AS $$
 BEGIN
   RETURN (SELECT COUNT(*) = 0
             FROM matches m
            WHERE tournament_id = tid
              AND kind = mkind
              AND has_players(m.id)
              AND ended IS NULL);
 END;$$
LANGUAGE plpgsql;

-- Returns true or false if we have filled the last-call match with players or not.
CREATE OR REPLACE FUNCTION last_chance_not_scheduled(tid INTEGER) RETURNS BOOLEAN AS $$
 BEGIN
   RETURN (SELECT COUNT(*) = 1
             FROM matches m
            WHERE tournament_id = tid
              AND kind = 'playoff'
              AND NOT has_players(m.id));
 END;$$
LANGUAGE plpgsql;

-- Create a test tournament that's ready to go, with a defined amount of players
CREATE OR REPLACE FUNCTION test_tournament(players INTEGER) RETURNS integer AS $$
  DECLARE tid INTEGER;
  DECLARE x INTEGER;
  BEGIN
    -- Start with creating the tournament itself
    SELECT trunc(random() * 100 + 10) INTO x;
    INSERT INTO tournaments (NAME, slug, opened, scheduled)
          VALUES ('TestingFall ' || x, 'test-' || x, now(), now())
      RETURNING ID INTO tid;
    PERFORM usurp_tournament(tid, players);
    UPDATE tournaments SET started = now() WHERE ID = tid;
    RETURN tid;
  END;$$
LANGUAGE plpgsql;

-- Create a test tournament that's ready to go
CREATE OR REPLACE FUNCTION test_tournament() RETURNS integer AS $$
  DECLARE x INTEGER;
  BEGIN
    -- Add a random amount of people (at least 16, but up to 32 more)
    SELECT trunc(random() * 32 + 16)::INTEGER INTO x;
    RETURN (SELECT test_tournament(x));
  END;$$
LANGUAGE plpgsql;

-- Clear all test tournaments
CREATE OR REPLACE FUNCTION clear_test_tournaments() RETURNS void AS $$
  DECLARE del INTEGER;
  BEGIN
    WITH deleted AS (
      DELETE FROM tournaments
       WHERE NAME NOT LIKE 'DrunkenFall%'
       RETURNING *
    ) SELECT COUNT(*) FROM deleted INTO del;
    RAISE notice 'Deleted % test tournaments', del;
  END;$$
LANGUAGE plpgsql;

CREATE VIEW real_tournaments AS
  SELECT * FROM tournaments
    WHERE ID >= 69
      AND NAME LIKE 'DrunkenFall%'
      AND league_id IS NOT NULL;

-- Get a real tournament that has had messages
-- (used for replaying functions)
CREATE OR REPLACE FUNCTION real_tournament() RETURNS integer AS $$
  DECLARE tid INTEGER;
  BEGIN
    SELECT * FROM real_tournaments
      ORDER BY random() LIMIT 1
      INTO tid;
    RETURN tid;
  END;$$
LANGUAGE plpgsql;

-- Get all real tournaments
CREATE OR REPLACE FUNCTION real_tournaments() RETURNS integer[] AS $$
  DECLARE tids INTEGER[];
  BEGIN
    SELECT ARRAY_AGG(ID) FROM real_tournaments INTO tids;
    RETURN tids;
  END;$$
LANGUAGE plpgsql;

ALTER TABLE tournaments
  ADD COLUMN IF NOT EXISTS max_players SMALLINT NOT NULL DEFAULT 32;

-- Check if we are allowed to add more players to the tournament
CREATE OR REPLACE FUNCTION can_add_players_to_tournament(tid INTEGER) RETURNS BOOL AS $$
  DECLARE maxp INTEGER;
  BEGIN
    SELECT max_players FROM tournaments WHERE ID = tid INTO maxp;
    RETURN (SELECT COUNT(ps.ID) < maxp
              FROM player_summaries ps
             WHERE tournament_id = tid);
  END;$$
LANGUAGE plpgsql;

-- Check if the tournament still exists. This is to prevent cascading queries to
-- error out when clearing test tournaments. See `on_remove_player`.
CREATE OR REPLACE FUNCTION tournament_exists(tid INTEGER) RETURNS BOOL AS $$
  BEGIN
    RETURN (SELECT EXISTS(SELECT 1 FROM tournaments WHERE ID = tid));
  END;$$
LANGUAGE plpgsql;

-- Set the qualifiers to end right away
CREATE OR REPLACE FUNCTION end_qualifiers() RETURNS void AS $$
  BEGIN
    PERFORM end_qualifiers(NOW());
  END;$$
LANGUAGE plpgsql;

-- Set the qualifiers to end right away
CREATE OR REPLACE FUNCTION end_qualifiers(ts TIMESTAMP WITH TIME ZONE) RETURNS void AS $$
  BEGIN
    UPDATE tournaments SET qualifying_end = ts
      WHERE ID = latest_tournament();
  END;$$
LANGUAGE plpgsql;
