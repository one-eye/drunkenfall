-- Prints a human friendly list (i.e. nicks instead of person_ids) of
-- all matches in a tournament
CREATE OR REPLACE FUNCTION scores(arg_tid INTEGER) RETURNS SETOF score AS $$
  BEGIN
   RETURN QUERY (
     SELECT
       m.tournament_id,
       m.ID,
       m.title,
       p.ID,
       person_nick(p.person_id),
       p.color,
       p.kills,
       p.sweeps,
       p.SELF,
       p.shots,
       p.total_score,
       p.match_score
     FROM players P
     INNER JOIN matches m ON p.match_id = m.ID
     WHERE m.tournament_id = arg_tid
     ORDER BY m.id, p.index);
  END $$
LANGUAGE plpgsql;

-- Same as above, but always from the most recent tournament
CREATE OR REPLACE FUNCTION scores() RETURNS SETOF score AS $$
  BEGIN
   RETURN QUERY SELECT * FROM scores(latest_tournament());
  END $$
LANGUAGE plpgsql;

CREATE TABLE points (
       ID SERIAL PRIMARY KEY,
       kind text,
       score INTEGER
);

INSERT INTO points (kind, score) VALUES
  ('kill', 147),
  ('self', -245),
  ('sweep', 679),
  ('winner', 2450),
  ('second', 1050),
  ('third', 490),
  ('fourth', 210);

-- Update the player on the match scope based on the rounds in the match
CREATE OR REPLACE FUNCTION update_player(pid INTEGER) RETURNS void AS $$
  DECLARE mid INTEGER;
  DECLARE idx INTEGER;
  DECLARE pkills INTEGER;
  BEGIN
   -- First, grab the match id and the index
   SELECT match_id FROM players WHERE ID = pid INTO mid;
   SELECT INDEX FROM player_states WHERE player_id = pid INTO idx;
   -- Then grab the straightforward stats
   SELECT calculate_kills(mid, idx) INTO pkills;
   -- RAISE notice 'update_player %(%): %', mid, idx, pkills;
   UPDATE players p
   SET (kills, sweeps, self, shots, total_score)
   =
   (SELECT pkills,
           COUNT(*) FILTER (WHERE up = 3) AS sweeps,
           SUM(ps.down) AS SELF,
           COUNT(*) FILTER (WHERE up = 3) + SUM(ps.down) AS shots,
           calculate_score(
             pkills,
             COUNT(*) FILTER (WHERE up = 3)::INTEGER,
             SUM(ps.down)::INTEGER)
      FROM player_states ps
      INNER JOIN players P ON p.ID = ps.player_id
      WHERE ps.player_id = pid)
    WHERE id = pid;
   RETURN;
  END $$
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION calculate_kills(mid INTEGER, idx INTEGER) RETURNS integer AS $$
  -- Calculate the same score as in the game; make sure the result is always non-zero
  DECLARE res INTEGER;
  DECLARE x record;
  BEGIN
    SELECT 0 INTO res;
    FOR x IN SELECT CAST(json->>'player' AS INTEGER) AS victim,
                    CAST(json->>'killer' AS INTEGER) AS killer
               FROM messages
              WHERE match_id = mid
                AND type = 'kill'
                AND (
                     -- Whenever the player we are looking at was the killer
                     CAST(json->>'killer' AS INTEGER) = idx OR
                     -- Whenever the player is the victim, either by self-kill
                     -- or by getting killed by the level, lol
                     (CAST(json->>'player' AS INTEGER) = idx
                       AND (CAST(json->>'killer' AS INTEGER) = idx OR
                            CAST(json->>'killer' AS INTEGER) = -1)))
                     -- I apologize for this query.
                     -- If I had more time, I would have written a better query.
              ORDER BY ID ASC
    LOOP
      -- RAISE NOTICE '% in %: % %', idx, mid, x.victim;
      IF x.victim = idx OR x.killer = -1 THEN
        IF res != 0 THEN
          SELECT res - 1 INTO res;
        END IF;
      ELSE
        SELECT res + 1 INTO res;
      END IF;
    END LOOP;
    -- RAISE NOTICE 'returning %', res;
    RETURN res;
  END; $$
LANGUAGE plpgsql;


CREATE OR REPLACE FUNCTION update_summary(tid INTEGER, pid INTEGER) RETURNS void AS $$
  BEGIN
   UPDATE player_summaries ps
     SET (kills, sweeps, self, shots, matches, total_score)
     =
     (SELECT SUM(p.kills),
             SUM(p.sweeps),
             SUM(p.SELF),
             SUM(p.shots),
             COUNT(*),
             SUM(p.total_score + p.match_score)
        FROM players P
        INNER JOIN matches m ON p.match_id = m.ID
        WHERE m.tournament_id = tid
          AND m.started IS NOT NULL
          AND person_id = pid)
      WHERE person_id = pid
        AND tournament_id = tid;
     -- After the initial calculation, also set the skill_score
   UPDATE player_summaries ps
     SET skill_score = ps.total_score / ps.matches
     WHERE person_id = pid
       AND tournament_id = tid;
  END $$
LANGUAGE plpgsql;

-- Without the tournament id, calculate just based on the values
CREATE OR REPLACE FUNCTION calculate_score(kills INTEGER, sweeps INTEGER, self INTEGER) RETURNS INTEGER AS $$
  BEGIN
   RETURN (kills *  (SELECT score FROM points WHERE kind = 'kill')) +
          (sweeps * (SELECT score FROM points WHERE kind = 'sweep')) +
          (SELF *   (SELECT score FROM points WHERE kind = 'self'));
  END $$
LANGUAGE plpgsql;

-- Because of the nature of the 1-indexed ROW_NUMBER, the positions
-- make sense until the last; 4. Whoever came last will have the pos
-- 0, so the calling functions will need to take that into
-- consideration as well.
--
-- This makes sure to only consider matches that have ended, so this
-- will not affect maches that are ongoing.
CREATE OR REPLACE FUNCTION count_positions(tid INTEGER, pid INTEGER, pos INTEGER) RETURNS INTEGER AS $$
  BEGIN
    RETURN (SELECT COUNT(person_id)
      FROM (SELECT ROW_NUMBER() OVER(ORDER BY p.match_id, p.kills DESC, p.SELF) AS n, p.person_id
              FROM players p WHERE p.match_id IN (
                  SELECT ID FROM matches
                   WHERE tournament_id = tid
                     AND kind != 'final'
                     AND ended IS NOT NULL)
      ) AS t WHERE t.n % 4 = pos AND person_id = pid);
  END $$
LANGUAGE plpgsql;

-- Set the match_score field on the player objects based on their positions
CREATE OR REPLACE FUNCTION set_match_scores(mid INTEGER) RETURNS void AS $$
  DECLARE mkind match_kind;
  BEGIN
    -- If the match hasn't ended yet, we should not set the scores
    IF (SELECT ended IS NULL FROM matches WHERE ID = mid) THEN
      RAISE EXCEPTION 'Cannot set match scores for % - not ended yet', match_name(mid);
    END IF;
    SELECT kind FROM matches WHERE ID = mid INTO mkind;
    -- Then, update the player objects to have their alloted scores set
    UPDATE players SET match_score = targets.match_score
      FROM
        (SELECT pos, ID, match_score(pos, mkind) FROM
          (SELECT
            ROW_NUMBER() OVER(ORDER BY p.kills DESC, p.sweeps DESC, p.SELF) AS pos,
            p.ID, p.person_id, person_nick(p.person_id),
            p.match_id, p.kills, p.sweeps, p.SELF
          FROM players P
          WHERE p.match_id = mid
          ORDER BY p.kills DESC, p.sweeps DESC, p.SELF ASC) AS T) AS targets
      WHERE players.ID = targets.ID;
  END $$
LANGUAGE plpgsql;

-- Set the match_score field on the player objects based on their positions
CREATE OR REPLACE FUNCTION match_score(pos BIGINT, mkind match_kind) RETURNS INTEGER AS $$
  DECLARE multiplier NUMERIC(2, 1);
  BEGIN
    -- First, set the multiplier. The final get more points.
    SELECT
      CASE WHEN mkind = 'final' THEN 2.5
           ELSE 1.0
      END
      INTO multiplier;
    -- Then, make a lookup into the points table and check what that position is worth.
    RETURN (
      SELECT points.score * multiplier
      FROM
        (SELECT ROW_NUMBER() OVER(ORDER BY score DESC) AS POSITION, score
          FROM points
          WHERE kind IN ('winner', 'second', 'third', 'fourth')
          ORDER BY score DESC) AS points
      WHERE points.POSITION = pos
    );
  END $$
LANGUAGE plpgsql;

-- Same as above, but only for the final
CREATE OR REPLACE FUNCTION count_final_positions(tid INTEGER, pid INTEGER, pos INTEGER) RETURNS INTEGER AS $$
  BEGIN
    RETURN (SELECT COUNT(person_id)
      FROM (SELECT ROW_NUMBER() OVER(ORDER BY p.match_id, p.kills DESC, p.SELF) AS n, p.person_id
              FROM players p WHERE p.match_id IN (
                  SELECT ID FROM matches
                   WHERE tournament_id = tid
                     AND kind = 'final'
                     AND ended IS NOT NULL)
      ) AS t WHERE t.n % 4 = pos AND person_id = pid);
  END $$
LANGUAGE plpgsql;

-- Redo all the scores for a single tournament
CREATE OR REPLACE FUNCTION recalculate_tournament(tid INTEGER) RETURNS void AS $$
  BEGIN
    PERFORM update_player(p.ID)
       FROM players P
      INNER JOIN matches M ON m.ID = p.match_id
      WHERE m.tournament_id = tid;
    PERFORM update_summary(tid, ps.ID)
       FROM player_summaries ps
      WHERE ps.tournament_id = tid;
  END $$
LANGUAGE plpgsql;
