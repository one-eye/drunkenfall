CREATE OR REPLACE FUNCTION correct_color_conflicts_on_player_insert() RETURNS
  TRIGGER AS $$
  BEGIN
    PERFORM correct_color_conflicts(NEW.match_id);
    RETURN NEW;
  END; $$
LANGUAGE plpgsql;

DROP TRIGGER correct_fucking_color_conflicts ON players;
CREATE TRIGGER correct_fucking_color_conflicts
  AFTER INSERT ON players
  FOR EACH ROW
  -- We only correct color conflicts when we are playing headhunters, because in
  -- team deathmatch we are always at least two colors of the same kind
  WHEN (is_headhunters(NEW.match_id))
  EXECUTE PROCEDURE correct_color_conflicts_on_player_insert();

CREATE OR REPLACE FUNCTION correct_color_conflicts(mid INTEGER) RETURNS
  TABLE(match_id INTEGER, person_id INTEGER, prev_color color, new_color color) AS $$
  DECLARE target color;
  DECLARE changed color;
  DECLARE pid INTEGER;
  BEGIN
    -- If there aren't four players in the match, we cannot start the correction
    -- just yet.
    IF (SELECT COUNT(*) = 4 FROM players P WHERE p.match_id = mid) THEN
      -- If there are four players, we need to determine if there are any
      -- conflicts.
      -- If there aren't four distinct colors, then at least two players share
      -- colors.
      RAISE DEBUG 'Correcting color conflicts for mid % (% %)', mid,
        (SELECT kind FROM matches WHERE ID = mid), (SELECT INDEX FROM matches WHERE ID = mid);
      WHILE (SELECT COUNT(*) != 4 FROM (SELECT DISTINCT color FROM players P WHERE p.match_id = mid) AS T) LOOP
        -- Grab the color that more than one player has
        SELECT color FROM (
          SELECT color, COUNT(color)
            FROM players P WHERE p.match_id = mid
            GROUP BY color ORDER BY 2 DESC
          ) AS T WHERE COUNT > 1
        INTO target;
        -- Store the player that has the lower score. Only care for the second
        -- player - if more than two players have the same color, successive
        -- iterations of the loop will take care of them.
       SELECT P.ID FROM players P
        INNER JOIN player_summaries ps ON ps.person_id = P.person_id
        INNER JOIN matches M ON P.match_id = M.ID
        INNER JOIN people pp ON pp.id = p.person_id
        WHERE p.match_id = mid
          AND ps.tournament_id = M.tournament_id
          AND color = target ORDER BY pp.userlevel DESC, skill_score DESC
          LIMIT 1 OFFSET 1
        INTO pid;
       -- Now that we know the player, set their color to a random available
       -- of the set.
       IF (pid IS NULL) THEN
         RAISE NOTICE 'pid is null when correcting color conflicts for %', target;
         RETURN;
       END IF;
       SELECT available_color(mid) INTO changed;
       RAISE NOTICE 'Correcting color of % from % to % (% already present)',
         player_nick(pid),
         target,
         changed,
         (SELECT array_to_string(ARRAY_AGG(color), ', ') FROM (
            SELECT DISTINCT color FROM players P WHERE p.match_id = mid) AS T);
       UPDATE players SET color = changed WHERE ID = pid;
      END LOOP;
    END IF;
  END; $$
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION available_color(mid INTEGER) RETURNS color AS $$
  BEGIN
    RETURN (SELECT color::color FROM (
      SELECT UNNEST(enum_range(NULL::color)) color) AS colors
       WHERE color NOT IN (SELECT DISTINCT color FROM players WHERE match_id = mid)
       ORDER BY random() LIMIT 1);
  END; $$
LANGUAGE plpgsql;
