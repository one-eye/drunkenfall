-- Add random players to the tournament. Takes care to only add them once.
CREATE OR REPLACE FUNCTION usurp_tournament(tid INTEGER, amount INTEGER) RETURNS void AS $$
  BEGIN
    INSERT INTO player_summaries (tournament_id, person_id)
      SELECT tid, id FROM people
       WHERE NOT disabled
         AND id NOT IN (
             SELECT person_id FROM player_summaries
              WHERE tournament_id = tid)
       ORDER BY random() LIMIT amount;
  END;$$
LANGUAGE plpgsql;

-- Same as above, but defaults to 8 players
CREATE OR REPLACE FUNCTION usurp_tournament(tid INTEGER) RETURNS void AS $$
  BEGIN
    PERFORM usurp_tournament(tid, 8);
  END;$$
LANGUAGE plpgsql;

-- Same as above, but defaults to current tournament
CREATE OR REPLACE FUNCTION usurp_tournament() RETURNS void AS $$
  BEGIN
    PERFORM usurp_tournament(latest_tournament(), 8);
  END;$$
LANGUAGE plpgsql;
