-- This is pretty much the state machine for the post-effect of messages.
--
-- Any pre-effects such as starting the match or creating the player_state
-- will happen in the trigger_round() set of functions.
CREATE OR REPLACE FUNCTION handle_message(msgid INTEGER) RETURNS void AS $$
  DECLARE msgtype message_type;
  BEGIN
    SELECT TYPE FROM messages WHERE ID = msgid INTO msgtype;
    IF (msgtype = 'kill') THEN
      -- The calculation of the up and down fields happens in the
      -- trigger_round() set of functions and does not need to be handled here.
      UPDATE player_states ps
         SET alive = FALSE, killer = CAST(msg.json->>'killer' AS INTEGER)
        FROM messages msg
        WHERE msg.ID = msgid
          AND ps.round_id = msg.round_id
          AND ps.INDEX = CAST(msg.json->>'player' AS INTEGER);
      -- Also, recalculate all of the kill data on the round
      PERFORM update_round(ID) FROM rounds ORDER BY ID DESC LIMIT 1;
    ELSIF (msgtype = 'round_start') THEN
      -- The round start comes with the arrow updates of all the players. Could
      -- perhaps be a better query, but the layers of JSON makes that ish
      -- tricky.
      UPDATE player_states ps
         SET arrows = json_array_castint(msg.json->'arrows'->0) FROM messages msg
        WHERE msg.ID = msgid AND ps.round_id = msg.round_id AND ps.INDEX = 0;
      UPDATE player_states ps
         SET arrows = json_array_castint(msg.json->'arrows'->1) FROM messages msg
        WHERE msg.ID = msgid AND ps.round_id = msg.round_id AND ps.INDEX = 1;
      UPDATE player_states ps
         SET arrows = json_array_castint(msg.json->'arrows'->2) FROM messages msg
        WHERE msg.ID = msgid AND ps.round_id = msg.round_id AND ps.INDEX = 2;
      UPDATE player_states ps
         SET arrows = json_array_castint(msg.json->'arrows'->3) FROM messages msg
        WHERE msg.ID = msgid AND ps.round_id = msg.round_id AND ps.INDEX = 3;
    -- ELSIF (msgtype = 'round_end') THEN
      -- Nothing happens on round end, really. Perhaps setting an end date in
      -- the future? Could be reconstructed from the message timestamps anyways.
    -- ELSIF (msgtype = 'match_start') THEN
    -- ELSIF (msgtype = 'match_end') THEN
    ELSIF (msgtype = 'arrows_collected') OR (msgtype = 'arrow_shot') THEN
      -- The arrow state messages are identical.
      UPDATE player_states ps
         SET arrows = json_array_castint(msg.json->'arrows')
        FROM messages msg
        WHERE msg.ID = msgid
          AND ps.round_id = msg.round_id
          AND ps.INDEX = CAST(msg.json->>'player' AS INTEGER);
    ELSIF (msgtype = 'shield_state') THEN
      -- These three are pretty much the same...
      UPDATE player_states ps
         SET shield = CAST(msg.json->>'state' AS BOOLEAN)
        FROM messages msg
        WHERE msg.ID = msgid
          AND ps.round_id = msg.round_id
          AND ps.INDEX = CAST(msg.json->>'player' AS INTEGER);
    ELSIF (msgtype = 'wings_state') THEN
      UPDATE player_states ps
         SET wings = CAST(msg.json->>'state' AS BOOLEAN)
        FROM messages msg
        WHERE msg.ID = msgid
          AND ps.round_id = msg.round_id
          AND ps.INDEX = CAST(msg.json->>'player' AS INTEGER);
    ELSIF (msgtype = 'lava_orb_state') THEN
      UPDATE player_states ps
         SET lava = CAST(msg.json->>'state' AS BOOLEAN)
        FROM messages msg
        WHERE msg.ID = msgid
          AND ps.round_id = msg.round_id
          AND ps.INDEX = CAST(msg.json->>'player' AS INTEGER);
    -- The last three ones have no application state right now
    ELSIF (msgtype = 'slow_orb_state') THEN
    ELSIF (msgtype = 'dark_orb_state') THEN
    ELSIF (msgtype = 'scroll_orb_state') THEN
    END IF;
  END; $$
LANGUAGE plpgsql;

-- wtf
-- https://stackoverflow.com/a/48013356
CREATE OR REPLACE FUNCTION json_array_castint(json) RETURNS int[] AS $f$
    SELECT array_agg(x)::int[] || ARRAY[]::int[] FROM json_array_elements_text($1) t(x);
$f$ LANGUAGE sql IMMUTABLE;


CREATE OR REPLACE FUNCTION random_messages_for_replay() RETURNS SETOF messages AS $$
  DECLARE rid INTEGER;
  BEGIN
    SELECT replayable_round() INTO rid;
    RETURN QUERY (SELECT * FROM messages WHERE round_id = rid ORDER BY id);
  END;$$
LANGUAGE plpgsql;
