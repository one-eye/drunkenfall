-- To get the log output from the various functions
SET client_min_messages = 'NOTICE';
SET log_min_messages = 'NOTICE';

-- More detailed logs in the server log
SET log_statement = 'all';
SET log_line_prefix = '[%p] [%c] [%m] [%x]: ';
