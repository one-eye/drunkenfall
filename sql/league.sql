-- A league is the absolute top level of data in this application
CREATE TABLE leagues (
  ID SERIAL PRIMARY KEY,
  title TEXT NOT NULL,
  description TEXT,
  started DATE,
  ended DATE
);

-- Insert some of the basic data that we need
INSERT INTO leagues (title, description, started, ended)
  VALUES
  ('DrunkenFall at East Mountain',
   'The oldest tournaments we have in the app. Roman numerals abound.',
   '2016-09-24', '2017-04-02'),
  ('DrunkenFall 2018',
   'Our first league. All the tournament themes where spins on movies.',
   '2018-01-01', '2018-12-31'),
  ('DrunkenFall 2019',
   'DrunkenFall at Kappa Bar!',
   '2019-01-01', '2019-12-31');

-- Make sure tournaments can belong to a league, but don't require it.
ALTER TABLE tournaments
  ADD COLUMN league_id INTEGER REFERENCES leagues(ID);

-- Make a leaderboard table, per league
CREATE TABLE leaderboards (
  ID SERIAL PRIMARY KEY,
  league_id INTEGER NOT NULL REFERENCES leagues(ID) ON DELETE CASCADE,
  person_id INTEGER NOT NULL REFERENCES people(ID) ON DELETE CASCADE,
  tournaments BIGINT NOT NULL DEFAULT 0,
  matches BIGINT NOT NULL DEFAULT 0,
  kills BIGINT NOT NULL DEFAULT 0,
  sweeps BIGINT NOT NULL DEFAULT 0,
  self BIGINT NOT NULL DEFAULT 0,
  shots BIGINT NOT NULL DEFAULT 0,
  total_score BIGINT NOT NULL DEFAULT 0,
  skill_score BIGINT NOT NULL DEFAULT 0
);
ALTER TABLE leaderboards ADD UNIQUE (league_id, person_id);

CREATE OR REPLACE FUNCTION update_leaderboard(lid INTEGER) RETURNS void AS $$
  BEGIN
    -- This is an unfortunately verbose way to do an UPSERT of the data as
    -- presented from the calculate_leaderboard() function.
    INSERT INTO leaderboards (league_id, person_id, tournaments, matches, kills,
                              sweeps, SELF, shots, total_score, skill_score)
      SELECT
        league_id, person_id, tournaments, matches, kills,
        sweeps, SELF, shots, total_score, skill_score
      FROM calculate_leaderboard(lid) cl
      ON CONFLICT (league_id, person_id)
      DO UPDATE SET
        tournaments = EXCLUDED.tournaments,
        matches = EXCLUDED.matches,
        kills = EXCLUDED.kills,
        sweeps = EXCLUDED.sweeps,
        SELF = EXCLUDED.self,
        shots = EXCLUDED.shots,
        total_score = EXCLUDED.total_score,
        skill_score = EXCLUDED.skill_score;
  END $$
LANGUAGE plpgsql;

-- Make the summary query for an entire league leaderboard
CREATE OR REPLACE FUNCTION calculate_leaderboard(lid INTEGER) RETURNS SETOF leaderboards AS $$
  BEGIN
   RETURN QUERY (
    SELECT
      -1,
      lid,
      ps.person_id,
      COUNT(ps.person_id),
      SUM(ps.matches),
      SUM(ps.kills),
      SUM(ps.sweeps),
      SUM(ps.SELF),
      SUM(ps.shots),
      SUM(ps.total_score) AS total_score,
      SUM(ps.total_score) / SUM(ps.matches)
    FROM player_summaries ps
    INNER JOIN people p ON p.id = ps.person_id
    WHERE NOT p.disabled
      AND ps.matches != 0
      AND ps.tournament_id IN (SELECT ID FROM tournaments t WHERE t.league_id = lid)
    GROUP BY ps.person_id
    ORDER BY total_score DESC);
  END $$
LANGUAGE plpgsql;

DROP FUNCTION leaderboard();
CREATE OR REPLACE FUNCTION leaderboard() RETURNS
  TABLE(pos BIGINT, nick TEXT, ID INTEGER, league_id INTEGER, person_id INTEGER,
        tournaments BIGINT, matches BIGINT, kills BIGINT, sweeps BIGINT, self BIGINT,
        shots BIGINT, total_score BIGINT, skill_score BIGINT) AS $$
  BEGIN
   RETURN QUERY (SELECT
                   ROW_NUMBER() OVER(ORDER BY l.total_score DESC),
                   person_nick(l.person_id) AS nick,
                   *
                 FROM leaderboards l
                 ORDER BY total_score DESC);
  END $$
LANGUAGE plpgsql;

-- Select a sum of all the leaderboard objects a person has
CREATE OR REPLACE FUNCTION person_leaderboard(pid INTEGER) RETURNS SETOF leaderboards AS $$
  BEGIN
    RETURN QUERY (
      SELECT
        -1 AS ID,
        -1 AS league_id,
        pid,
        SUM(l.tournaments)::BIGINT,
        SUM(l.matches)::BIGINT,
        SUM(l.kills)::BIGINT,
        SUM(l.sweeps)::BIGINT,
        SUM(l.SELF)::BIGINT,
        SUM(l.shots)::BIGINT,
        SUM(l.total_score)::BIGINT AS total_score,
        SUM(l.total_score)::BIGINT / SUM(l.matches)::BIGINT
      FROM leaderboards l
      WHERE person_id = pid
      GROUP BY person_id
    );
  END $$
LANGUAGE plpgsql;

-- Update the leaderboard of a tournament if it is part of a league
CREATE OR REPLACE FUNCTION update_leaderboard_after_tournament() RETURNS TRIGGER AS $$
 BEGIN
   -- The OLD record is so that the DELETE trigger works as well. This might
   -- lead to confusing results if we set `ended` and change `league_id` in one
   -- update so... don't do that.
   PERFORM update_leaderboard(OLD.league_id);
   RAISE NOTICE 'Leaderboard for league % updated', OLD.league_id;
   -- Yet somehow... it does not complain about this?
   RETURN NEW;
 END;$$
LANGUAGE plpgsql;

CREATE TRIGGER on_tournament_update_leaderboard
  AFTER UPDATE ON tournaments
  FOR EACH ROW
  WHEN (NEW.ended IS NOT NULL AND NEW.league_id IS NOT NULL)
  EXECUTE PROCEDURE update_leaderboard_after_tournament();

CREATE TRIGGER on_tournament_delete_update_leaderboard
  AFTER DELETE ON tournaments
  FOR EACH ROW
  WHEN (OLD.league_id IS NOT NULL)
  EXECUTE PROCEDURE update_leaderboard_after_tournament();

-- Set the league for a tournament if there is one spanning over the current dates
CREATE OR REPLACE FUNCTION set_tournament_league() RETURNS TRIGGER AS $$
  DECLARE lid INTEGER;
  BEGIN
    UPDATE tournaments SET league_id = (
      SELECT ID FROM leagues l
        WHERE NOW() BETWEEN l.started AND l.ended
        ORDER BY ID ASC
        LIMIT 1)
      WHERE ID = NEW.id
      RETURNING league_id INTO lid;
    RAISE NOTICE 'Tournament league set to %', lid;
    RETURN NEW;
  END;$$
LANGUAGE plpgsql;

CREATE TRIGGER on_tournament_create_set_league
  AFTER INSERT ON tournaments
  FOR EACH ROW
  EXECUTE PROCEDURE set_tournament_league();
