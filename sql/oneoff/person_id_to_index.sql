-- These are the commands needed to change the person_id column to be a integer index rather than
-- the facebook id

-- Helper function that makes conversion possible in one statement
CREATE OR REPLACE FUNCTION pid_from_fbid(fbid text) RETURNS INTEGER AS $$
  BEGIN
    RETURN (SELECT ID FROM people WHERE person_id = fbid);
  END; $$
LANGUAGE plpgsql;


-- Players table
ALTER TABLE players ALTER COLUMN person_id
  SET DATA TYPE INTEGER
  USING pid_from_fbid(person_id);

ALTER TABLE players
  ADD CONSTRAINT people_fk
     FOREIGN KEY (person_id)
     REFERENCES people (ID)
     ON DELETE CASCADE;


-- Player summaries table
ALTER TABLE player_summaries ALTER COLUMN person_id
  SET DATA TYPE INTEGER
  USING pid_from_fbid(person_id);

ALTER TABLE player_summaries
  ADD CONSTRAINT people_fk
     FOREIGN KEY (person_id)
     REFERENCES people (ID)
     ON DELETE CASCADE;
