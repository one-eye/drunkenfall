-- Loop through all the messages for a match and assign them to their rounds accordingly
CREATE OR REPLACE FUNCTION bind_messages_to_rounds(mid INTEGER) RETURNS void AS $$
  DECLARE rid INTEGER;
  DECLARE M messages%rowtype;
  BEGIN
    -- Store the first ID of the rounds
    SELECT ID FROM rounds WHERE match_id = mid ORDER BY ID LIMIT 1 INTO rid;
    -- Loop all the messages of the match
    FOR M IN SELECT * FROM messages WHERE match_id = mid ORDER BY TIMESTAMP LOOP
      -- Update the current message to the round that we are on
      UPDATE messages SET round_id = rid WHERE id = m.ID;
      -- If we've just updated a `round_end`, then we're about to go to the next round
      -- so we increase the current round_id
      IF M.TYPE = 'round_end' THEN
         SELECT rid+1 INTO rid;
      END IF;
    END LOOP;
    -- Lastly, set the match_end to be on the same round as the round_end
    UPDATE messages SET round_id = rid-1 WHERE id = m.ID AND TYPE = 'match_end';
  END;$$
LANGUAGE plpgsql;


-- Updates the data in a `round` based on the messages that are in it
CREATE OR REPLACE FUNCTION update_round(rid INTEGER) RETURNS void AS $$
  BEGIN
    -- Calculate the score for all four players for every message we get.
    -- Perhaps not the most efficient way, but it makes it easier to backfill
    -- and calculate from earlier tournaments.
    UPDATE player_states ps
       SET up = sum_kills(rid, ps.INDEX),
           down = sum_self(rid, ps.INDEX)
      FROM players P
     WHERE ps.round_id = rid
       AND p.ID = ps.player_id;
     -- And now that we have updated the player state objects (round local), we
     -- should also update the player objects (match local).
     PERFORM update_player(ps.player_id) FROM player_states ps WHERE round_id = rid;
  END;$$
LANGUAGE plpgsql;

-- Get the amount of kills a player has done, and disregard selfs
CREATE OR REPLACE FUNCTION sum_kills(rid INTEGER, P INTEGER) RETURNS INTEGER AS $$
  BEGIN
    RETURN (SELECT COUNT(*) FROM messages
            WHERE round_id = rid
              AND TYPE = 'kill'
              AND CAST(json->>'player' AS INTEGER) != P
              AND CAST(json->>'killer' AS INTEGER) = P);
  END;$$
LANGUAGE plpgsql;

-- Get the amount of selfs a player has done in a round.
-- Can only be one or zero, but for the sake of the data model it is kept as an
-- integer. Parts of the interface expects it to be numbers.
CREATE OR REPLACE FUNCTION sum_self(rid INTEGER, P INTEGER) RETURNS INTEGER AS $$
  DECLARE C INTEGER;
  BEGIN
    SELECT COUNT(*) FROM messages
            WHERE round_id = rid
              AND TYPE = 'kill'
              AND  CAST(json->>'player' AS INTEGER) = p
              AND (CAST(json->>'killer' AS INTEGER) = P OR
                   CAST(json->>'killer' AS INTEGER) = -1)
      INTO C;
    RETURN C;
  END;$$
LANGUAGE plpgsql;


-- Update the stats of the current round whenever a new message arrives
CREATE OR REPLACE FUNCTION update_round_on_new_message() RETURNS trigger AS $$
  BEGIN
    PERFORM trigger_round(NEW.ID);
    PERFORM handle_message(NEW.ID);
    RETURN NEW;
  END;$$
LANGUAGE plpgsql;


-- Updates the data in a `round` based on the messages that are in it
CREATE OR REPLACE FUNCTION trigger_round(msgid INTEGER) RETURNS void AS $$
  BEGIN
    -- First check if the message type is either match_start or match_end. Both
    -- of these are special since they 1) Don't belong to a round 2) Manipulate
    -- the state of the match rather than the round.
    IF (SELECT TYPE = 'match_start' FROM messages WHERE ID = msgid) THEN
      -- Start the first non-started match in the latest tournament
      UPDATE matches SET started = NOW()
       WHERE ID = latest_match();
      RETURN;
    ELSIF (SELECT TYPE = 'match_end' FROM messages WHERE ID = msgid) THEN
      -- End the latest started but non-ended match
      UPDATE matches SET ended = NOW()
       WHERE ID = (SELECT ID FROM matches m
                    WHERE m.started IS NOT NULL AND m.ended IS NULL
                    ORDER BY ID DESC LIMIT 1);
      -- Now that the match is ended, we should do a recalculation of the scores
      -- of the players in the match, so that they get their end-of-match scores
      -- as well.
      PERFORM update_summary(latest_tournament(), p.person_id) FROM players p
        INNER JOIN matches m ON p.match_id = m.ID
        WHERE m.ID = (SELECT ID FROM matches M
                       WHERE m.ended IS NOT NULL
                       ORDER BY ID DESC LIMIT 1);
      -- End here.
      RETURN;
    END IF;
    -- If we're starting a new round, make sure to enter rows into the
    -- rounds table.
    IF (SELECT TYPE = 'round_start' FROM messages WHERE ID = msgid) THEN
      -- This insert triggers generation of the player_state objects, so we
      -- don't need to handle those here.
      INSERT INTO rounds (match_id)
        SELECT id FROM matches M
         WHERE m.started IS NOT NULL AND m.ended IS NULL;
    END IF;
    -- Lastly, set the round_id of the message. This has to be last since we
    -- might just have made a new round above.
    UPDATE messages
       SET round_id = (SELECT ID FROM rounds ORDER BY ID DESC LIMIT 1)
       WHERE ID = msgid;
  END;$$
LANGUAGE plpgsql;


CREATE TRIGGER on_message_update_round
  AFTER INSERT ON messages
  FOR EACH ROW EXECUTE PROCEDURE update_round_on_new_message();

ALTER TABLE rounds
  DROP p1up,
  DROP p1down,
  DROP p2up,
  DROP p2down,
  DROP p3up,
  DROP p3down,
  DROP p4up,
  DROP p4down;

-- Query to grab the round numbers, should they ever be needed
SELECT r.*, ((ROW_NUMBER() OVER(ORDER BY m.ID) -1) /4) +1 AS round
  FROM player_rounds r
  INNER JOIN players P ON r.player_id = p.ID
  INNER JOIN matches M ON p.match_id = m.ID;

DROP TABLE player_rounds;

CREATE TABLE player_rounds (
  ID SERIAL PRIMARY KEY,
  round_id INTEGER REFERENCES rounds(ID) ON DELETE CASCADE,
  player_id INTEGER REFERENCES players(ID) ON DELETE CASCADE,
  up   SMALLINT NOT NULL DEFAULT 0,
  down SMALLINT NOT NULL DEFAULT 0
);

CREATE TRIGGER on_round_create_player_states
  AFTER INSERT ON rounds
  FOR EACH ROW EXECUTE PROCEDURE create_player_states_on_new_round();

CREATE OR REPLACE FUNCTION create_player_states_on_new_round() RETURNS trigger AS $$
  BEGIN
    INSERT INTO player_states (round_id, player_id, index)
      SELECT NEW.ID, p.ID, p.index FROM players P
       WHERE p.match_id = NEW.match_id
       ORDER BY INDEX ASC;
    RETURN NEW;
  END;$$
LANGUAGE plpgsql;

CREATE INDEX messages_round_id_idx ON messages (round_id);

SELECT *,
  sum_kills(id, 0) as p1up, sum_self(id, 0) as p1down,
  sum_kills(id, 1) as p2up, sum_self(id, 1) as p2down,
  sum_kills(id, 2) as p3up, sum_self(id, 2) as p3down,
  sum_kills(id, 3) as p4up, sum_self(id, 3) as p4down
  FROM rounds where match_id = 583 ORDER BY ID;

CREATE OR REPLACE FUNCTION replayable_round() RETURNS INTEGER AS $$
  DECLARE mid INTEGER;
  BEGIN
    -- If this is moved to the RETURN statement, it causes an infinite loop. What.
    SELECT replayable_match() INTO mid;
    RETURN (SELECT ID FROM rounds
              WHERE match_id = mid
              ORDER BY random() LIMIT 1);
  END;$$
LANGUAGE plpgsql;
