CREATE OR REPLACE FUNCTION avatars(tid INTEGER) RETURNS SETOF text AS $$
  BEGIN
    SET client_min_messages TO WARNING;
    DROP TABLE IF EXISTS avatars;
    CREATE TEMP TABLE avatars(filename TEXT);
    -- The mounted directory only contains the filename, so we use sed to make
    -- it the full URL. It's possible to do that in the SELECT further down, but
    -- that would require a LATERAL join and this was simply easier.
    COPY avatars FROM PROGRAM 'ls /avatars/ | sed ''s;\(.*\);/img/avatars/fake/\1;''';
    RETURN QUERY
      SELECT filename AS avatar FROM avatars
       WHERE filename NOT IN (
         SELECT avatar_url FROM people P
          INNER JOIN player_summaries ps ON p.ID = ps.person_id
          WHERE ps.tournament_id = tid
       )
      ORDER BY avatar ASC;
  END;$$
LANGUAGE plpgsql SECURITY DEFINER;

COMMENT ON FUNCTION avatars(tid INTEGER) IS 'Grab unused avatar URLs based on a tournament ID.';

CREATE OR REPLACE FUNCTION avatar(tid INTEGER) RETURNS text AS $$
  BEGIN
    RETURN (SELECT * FROM avatars(tid) ORDER BY random() LIMIT 1);
  END;$$
LANGUAGE plpgsql;

COMMENT ON FUNCTION avatar(tid INTEGER) IS 'Grab a single avatar URL based on a tournament ID.';
