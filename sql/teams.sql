-- Firstly, let's add a kind!

CREATE TYPE tournament_kind AS ENUM (
    'headhunters',
    'team-deathmatch'
);

ALTER TABLE tournaments
  ADD COLUMN IF NOT EXISTS kind tournament_kind NOT NULL DEFAULT 'headhunters';


-- Make simple detection functions that will help us determine what kind of
-- tournament we are running.
-- Notably, we are operating on the match level since that's the level that's
-- _usually_ accessible from everywhere, and checking it directly on the
-- tournaments table is just... real easy.
CREATE OR REPLACE FUNCTION is_headhunters(mid INTEGER) RETURNS BOOLEAN AS $$
BEGIN
  RETURN (SELECT t.kind = 'headhunters'
            FROM matches M
            INNER JOIN tournaments T
              ON t.ID = m.tournament_id
            WHERE m.ID = mid);
END $$
LANGUAGE plpgsql;

-- Same as above, of course.
CREATE OR REPLACE FUNCTION is_team_deathmatch(mid INTEGER) RETURNS BOOLEAN AS $$
BEGIN
  RETURN (SELECT t.kind = 'team-deathmatch'
            FROM matches M
            INNER JOIN tournaments T
              ON t.ID = m.tournament_id
            WHERE m.ID = mid);
END $$
LANGUAGE plpgsql;


CREATE TABLE teams (
  ID SERIAL PRIMARY KEY,
  tournament_id INTEGER NOT NULL REFERENCES tournaments(ID) ON DELETE CASCADE,
  name TEXT NOT NULL
);

CREATE TABLE team_players (
  ID SERIAL PRIMARY KEY,
  team_id INTEGER NOT NULL REFERENCES teams(ID) ON DELETE CASCADE,
  player_summary_id INTEGER NOT NULL REFERENCES player_summaries(ID) ON DELETE CASCADE,
  UNIQUE (team_id, player_summary_id)
);

-- TODO(thiderman): Constraint so that a player is only in one team per tournament

CREATE OR REPLACE FUNCTION generate_team_name(nick1 TEXT, nick2 text) RETURNS TEXT AS $$
  DECLARE FIRST TEXT[];
  DECLARE SECOND TEXT[];
  BEGIN
    SELECT split_in_half(nick1), split_in_half(nick2) INTO FIRST, SECOND;
    IF random() * 100 >= 50 THEN
      RETURN FIRST[1] || SECOND[2] ;
    ELSE
      RETURN SECOND[1] || FIRST[2] ;
    END IF;
  END $$
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION split_in_half(str text) RETURNS TEXT[] AS $$
  DECLARE l INTEGER;
  DECLARE FIRST TEXT;
  DECLARE SECOND TEXT;
  DECLARE clean_str TEXT;
  BEGIN
    -- Clean the nick by un-camelcasing and replacing all double spaces
    SELECT TRIM(regexp_replace(regexp_replace(str, '([^ A-Z][^A-Z]*)','\1 '), '[^0-9A-Za-z]+', ' ', 'g')) INTO clean_str;
    -- If it contains a space, use that as a splitting point
    IF clean_str LIKE '% %' THEN
      RETURN string_to_array(clean_str, ' ');
    ELSE
      SELECT LENGTH(str) INTO l;
      SELECT substr(str, 0, l/2+1) INTO FIRST;
      SELECT substr(str, l/2+1) INTO SECOND;
      RETURN ARRAY[FIRST, SECOND];
    END IF;
  END $$
LANGUAGE plpgsql;

-- Generate some of the admin names
SELECT * FROM (
       SELECT person_nick(p1),
              person_nick(p2),
              generate_team_name(person_nick(p1), person_nick(p2)) AS team_name
         FROM people_combinations()) AS T
 LIMIT 30;

WITH combo AS
  (SELECT ID FROM people WHERE NOT disabled ORDER BY random() LIMIT 8)
  --(SELECT ID FROM admins WHERE NOT disabled)
  SELECT person_nick(p1.ID) AS FIRST, person_nick(p2.ID) AS SECOND,
         generate_team_name(person_nick(p1.ID), person_nick(p2.ID)) AS team_name
    FROM combo p1
    CROSS JOIN combo p2
    WHERE p1.ID < p2.ID;
