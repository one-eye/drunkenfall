-- Show the total scores of a tournament
DROP FUNCTION overview(INTEGER);
CREATE OR REPLACE FUNCTION overview(tid INTEGER) RETURNS
  SETOF summary AS $$
  BEGIN
   RETURN QUERY (
     SELECT
       ROW_NUMBER() OVER(ORDER BY ps.skill_score DESC, ps.ID ASC),
       ps.ID,
       p.ID,
       ps.tournament_id,
       p.nick,
       ps.kills,
       ps.sweeps,
       ps.SELF,
       ps.shots,
       ps.matches,
       ps.total_score,
       ps.skill_score
     FROM player_summaries ps
     INNER JOIN people p ON p.id = ps.person_id
     INNER JOIN tournaments t ON t.id = ps.tournament_id
     WHERE ps.tournament_id = tid
     -- Sort first by the score and then by the order they joined the tournament
     ORDER BY ps.skill_score DESC, ps.id ASC);
  END $$
LANGUAGE plpgsql;

-- Same as above, but for latest tournament only
DROP FUNCTION overview();
CREATE OR REPLACE FUNCTION overview() RETURNS
  SETOF summary AS $$
  BEGIN
   RETURN QUERY SELECT * FROM overview(latest_tournament());
  END $$
LANGUAGE plpgsql;
