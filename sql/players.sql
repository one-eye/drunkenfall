-- Trigger function to be used on insertion of players. Generates
-- display names as by the function defined above, or grabs from the
-- person object if already set. Also sets all the other data that's grabbed
-- from the person; color, preferred color, archer type and nick.
CREATE OR REPLACE FUNCTION player_insert() RETURNS trigger AS $$
  DECLARE tid INTEGER;
  BEGIN
    -- First set the index, since that's captured from the `players' table.
    SELECT INTO NEW.INDEX first_available_index(NEW.match_id);
    -- Then grab data from the person
    SELECT INTO NEW.nick, NEW.color, NEW.preferred_color, NEW.archer_type, NEW.display_names
                p.nick, p.preferred_color, p.preferred_color, p.archer_type, p.display_names
           FROM people p
          WHERE p.ID = NEW.person_id;
    IF NEW.display_names IS NULL THEN
      -- If we don't have any display names set for this person, grab random ones
      SELECT m.tournament_id FROM matches M WHERE id = NEW.match_id INTO tid;
      NEW.display_names = display_names(tid, NEW.nick);
    END IF;
    RETURN NEW;
  END;$$
LANGUAGE plpgsql;

CREATE TRIGGER on_player_insert
  BEFORE INSERT ON players FOR EACH ROW EXECUTE PROCEDURE player_insert();

-- Simple table keeping tabs on players that want to join the tournament but
-- cannot yet because it is full.
CREATE TABLE player_queue (
  ID SERIAL PRIMARY KEY,
  tournament_id INTEGER REFERENCES tournaments(ID) ON DELETE CASCADE,
  person_id INTEGER REFERENCES people(ID) ON DELETE CASCADE
);

-- Send the player to the queue instead of the player_summaries
CREATE OR REPLACE FUNCTION queue_player() RETURNS trigger AS $$
  BEGIN
    -- Since the trigger to move to the queue happens before the UNIQUE
    -- constraint, we kinda also want to repeat the check here so that no one
    -- can be in the tournament _and_ in the queue.
    IF (SELECT COUNT(ID) > 0 FROM player_summaries
          WHERE tournament_id = NEW.tournament_id
            AND person_id = NEW.person_id) THEN
      RAISE EXCEPTION '% (person_id %) is already in the tournament and cannot be in the queue as well',
        person_nick(NEW.person_id), NEW.person_id;
    END IF;
    -- If that check passed, then we insert the player into the queue for realz.
    INSERT INTO player_queue (tournament_id, person_id)
      SELECT NEW.tournament_id, NEW.person_id;
    RAISE NOTICE '% moved to the queue', person_nick(NEW.person_id);
    -- Return NULL so that the original insert is aborted
    RETURN NULL;
  END;$$
LANGUAGE plpgsql;

-- If we spill over the max amount of players, we should immediately delete this
-- recent insert and then put them into the queue instead.
CREATE TRIGGER on_player_summary_insert
  BEFORE INSERT ON player_summaries
  FOR EACH ROW
  WHEN (NOT can_add_players_to_tournament(NEW.tournament_id))
  EXECUTE PROCEDURE queue_player();

CREATE OR REPLACE FUNCTION block_duplicate_name() RETURNS trigger AS $$
  BEGIN
    RAISE EXCEPTION '% blocked from entering due to duplicate name', person_nick(NEW.person_id);
  END;$$
LANGUAGE plpgsql;

-- Prohibit two players from having the same name in one tournament.
--
-- This prevents mistakes (one time a player accidentally made two accounts and
--joined twice, causing several kinds of mild headaches) and also just lowers
--general confusion.
CREATE TRIGGER block_player_with_same_name
  BEFORE INSERT ON player_summaries
  FOR EACH ROW
  WHEN (has_name_in_tournament(NEW.tournament_id, person_nick(NEW.person_id)))
  EXECUTE PROCEDURE block_duplicate_name();

-- Print the queue
CREATE OR REPLACE FUNCTION queue(tid INTEGER) RETURNS
  TABLE(id INTEGER, td INTEGER, pid INTEGER, nick text) AS $$
  BEGIN
   RETURN QUERY (
     SELECT q.ID,
            q.tournament_id AS tid,
            q.person_id AS pid,
            person_nick(q.person_id) AS nick
       FROM player_queue q
       WHERE q.tournament_id = tid
       ORDER BY q.ID ASC);
  END $$
LANGUAGE plpgsql;

-- Same as above, but always from the most recent tournament
CREATE OR REPLACE FUNCTION queue() RETURNS
  TABLE(id INTEGER, td INTEGER, pid INTEGER, nick text) AS $$
  BEGIN
   RETURN QUERY SELECT * FROM queue(latest_tournament());
  END $$
LANGUAGE plpgsql;


-- Returns the current list of playoff_runnerups, but as entries in the
-- player_summaries table.
CREATE OR REPLACE FUNCTION playoff_runnerups_as_summaries(tid INTEGER) RETURNS SETOF player_summaries AS $$
  BEGIN
    RETURN QUERY (
      SELECT P.ID,
             tid AS tournament_id,
             p.person_id,
             p.shots,
             p.sweeps,
             p.kills,
             p.SELF,
             1::SMALLINT AS matches,
             p.total_score,
             p.total_score AS skill_score
        FROM players P
        INNER JOIN (SELECT * FROM playoff_runnerups(tid)) AS r
          ON r.id = p.ID
        ORDER BY r.pos);
  END $$
LANGUAGE plpgsql;


-- Add constraints so that a player can never be twice in anything
ALTER TABLE player_summaries ADD UNIQUE (tournament_id, person_id);
ALTER TABLE player_queue ADD UNIQUE (tournament_id, person_id);
ALTER TABLE players ADD UNIQUE (match_id, person_id);

-- Add missing NOT NULL constraints to player_summaries
ALTER TABLE player_summaries
  ALTER tournament_id SET NOT NULL,
  ALTER person_id SET NOT NULL;

-- Only four players per match
CREATE TRIGGER reject_player_if_match_is_full
  BEFORE INSERT ON players
  FOR EACH ROW
  WHEN (NOT can_add_player_to_match(NEW.match_id))
  EXECUTE PROCEDURE reject_player();

-- Trigger of the above function
CREATE OR REPLACE FUNCTION reject_player() RETURNS TRIGGER AS $$
  BEGIN
    RAISE EXCEPTION 'Match % (%) already has four players',
      match_name(NEW.match_id), NEW.match_id;
  END; $$
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION can_add_player_to_match(mid INTEGER) RETURNS BOOLEAN AS $$
  BEGIN
    RETURN (SELECT COUNT(*) < 4 FROM players WHERE match_id = mid);
  END; $$
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION has_name_in_tournament(tid INTEGER, NAME TEXT) RETURNS BOOLEAN AS $$
  BEGIN
    RETURN (
      SELECT COUNT(*) >= 1
        FROM player_summaries ps
        WHERE person_nick(ps.person_id) = NAME
          AND tournament_id = tid
    );
  END; $$
LANGUAGE plpgsql;

-- We need to keep track on what position someone had when the playoffs started
-- - basically what position they qualified at.
ALTER TABLE player_summaries
  ADD COLUMN qualifying_position SMALLINT;

-- Function that sets positions for all the players
CREATE OR REPLACE FUNCTION set_qualifying_positions(tid INTEGER) RETURNS void AS $$
  BEGIN
    WITH o AS (SELECT position, person_id FROM overview(tid))
      UPDATE player_summaries ps
        SET qualifying_position = o.position
        FROM o
        WHERE ps.person_id = o.person_id
          AND ps.tournament_id = tid;
  END $$
LANGUAGE plpgsql;
