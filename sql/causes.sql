-- Causes of death
CREATE TYPE reason AS ENUM (
  'arrow',
	'explosion',
	'brambles',
	'jumped_on',
	'lava',
	'shock',
	'spikeball',
	'falling_object',
	'squish',
	'curse',
	'miasma',
	'enemy',
	'chalice'
);

CREATE OR REPLACE FUNCTION kill_reason(code INTEGER) RETURNS reason AS $$
  BEGIN
    RETURN (
      SELECT UNNEST(enum_range(NULL::reason))
             OFFSET code LIMIT 1);
  END; $$
LANGUAGE plpgsql;
