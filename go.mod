module gitlab.com/one-eye/drunkenfall

go 1.12

require (
	github.com/PuerkitoBio/purell v1.1.1 // indirect
	github.com/StefanSchroeder/Golang-Roman v0.0.0-20140210191918-81f07040d2fe
	github.com/alecthomas/template v0.0.0-20160405071501-a0175ee3bccc
	github.com/gin-contrib/sessions v0.0.0-20180724132311-854e10e72056
	github.com/gin-contrib/zap v0.0.0-20180322124736-26d5a72cacce
	github.com/gin-gonic/gin v1.3.0
	github.com/go-openapi/jsonpointer v0.18.0 // indirect
	github.com/go-openapi/swag v0.18.0 // indirect
	github.com/go-pg/pg v8.0.4+incompatible
	github.com/golang/protobuf v1.3.4 // indirect
	github.com/gorilla/mux v1.7.3 // indirect
	github.com/gorilla/websocket v1.4.1 // indirect
	github.com/jinzhu/inflection v0.0.0-20180308033659-04140366298a // indirect
	github.com/json-iterator/go v1.1.7 // indirect
	github.com/kr/pretty v0.1.0 // indirect
	github.com/magefile/mage v1.8.0
	github.com/mailru/easyjson v0.0.0-20190312143242-1de009706dbe // indirect
	github.com/markbates/goth v1.56.0
	github.com/mitchellh/go-homedir v1.1.0
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/modern-go/reflect2 v1.0.1 // indirect
	github.com/olahol/melody v0.0.0-20170518105555-d52139073376
	github.com/onsi/ginkgo v1.12.0 // indirect
	github.com/onsi/gomega v1.8.1 // indirect
	github.com/pkg/errors v0.8.1
	github.com/spf13/cobra v0.0.5
	github.com/spf13/viper v1.3.2
	github.com/streadway/amqp v0.0.0-20180315184602-8e4aba63da9f
	github.com/stretchr/testify v1.4.0 // indirect
	github.com/swaggo/gin-swagger v1.1.0
	github.com/swaggo/swag v1.4.1
	github.com/ugorji/go/codec v0.0.0-20190204201341-e444a5086c43 // indirect
	go.uber.org/atomic v1.3.2 // indirect
	go.uber.org/multierr v1.1.0 // indirect
	go.uber.org/zap v1.7.1
	golang.org/x/crypto v0.0.0-20200302210943-78000ba7a073 // indirect
	golang.org/x/net v0.0.0-20200301022130-244492dfa37a // indirect
	golang.org/x/oauth2 v0.0.0-20190604053449-0f29369cfe45 // indirect
	golang.org/x/sys v0.0.0-20200302150141-5c8b2ff67527 // indirect
	golang.org/x/tools v0.0.0-20191216173652-a0e659d51361 // indirect
	google.golang.org/appengine v1.6.1 // indirect
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127 // indirect
	gopkg.in/go-playground/assert.v1 v1.2.1 // indirect
	gopkg.in/yaml.v2 v2.2.8
	mellium.im/sasl v0.2.1 // indirect
)

replace github.com/mholt/certmagic => github.com/mholt/certmagic v0.8.3
