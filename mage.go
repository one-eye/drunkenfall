//+build mage

package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"time"

	"github.com/magefile/mage/mg"
	"github.com/magefile/mage/sh"
)

const binary = "./drunkenfall"
const dbFile = "./sql/skeleton.sql"

// psql runs a postgres command
var psql = sh.RunCmd("psql", "--host", "localhost", "--user", "postgres")

func version() string {
	version, err := sh.OutCmd("git", "describe", "--always", "--tags")()
	if err != nil {
		log.Fatal(err)
	}
	return version
}

func buildTime() string {
	return time.Now().Format(time.RFC3339)
}

// compile runs either build or install
func compile(install bool) error {
	if os.Getenv("DRUNKENFALL_NO_DOCS") == "" {
		mg.Deps(Code.GenerateDocs)
	} else {
		log.Println("DRUNKENFALL_NO_DOCS is set, skipping docs generation")
	}

	env := make(map[string]string)

	// These are needed for static linking to work inside Docker
	env["CGO_ENABLED"] = "0"
	env["GOOS"] = "linux"

	ldflags := fmt.Sprintf(
		"-w -X gitlab.com/one-eye/drunkenfall/cmd.Version=%s -X gitlab.com/one-eye/drunkenfall/cmd.BuildTime=%s",
		version(),
		buildTime(),
	)

	if install {
		return sh.RunWith(
			env,
			"go", "install", "-v",
			"-ldflags", ldflags,
		)
	} else {
		return sh.RunWith(
			env,
			"go", "build", "-v",
			"-ldflags", ldflags,
			"-o", binary,
		)
	}
}

type Build mg.Namespace

// Builds the backend service
func (Build) Drunkenfall() error {
	return compile(false)
}

// Installs the backend service onto the $PATH
func (Build) Install() error {
	return compile(true)
}

// Rebuilds node-sass.
//
// For whatever reason, `node-sass` has a compiled component that is intensely
// fragile to kernel updates, so it's alarmingly common that it breaks
// haphazardly and needs a rebuild. If you see `node-sass` runner errors,
// this is the thing you want to run.
func (Build) Sass() error {
	if err := os.Chdir("js"); err != nil {
		return err
	}

	defer os.Chdir("../")
	return sh.Run("npm", "rebuild", "node-sass")
}

type Run mg.Namespace

// Runs the backend service
func (Run) Drunkenfall() error {
	mg.Deps(Build.Drunkenfall)

	return sh.Run(binary, "serve")
}

// Runs the npm frontend
func (Run) Npm() error {
	if err := os.Chdir("fronkendfall"); err != nil {
		return err
	}

	defer os.Chdir("../")
	return sh.Run("npm", "run", "start")
}

// Runs the postgres service via docker-compose
func (Run) Databaz() error {
	return sh.Run("docker-compose", "up", "databaz")
}

// Runs the proxy, with localhost settings
func (Run) Proxy() error {
	mg.Deps(Proxy.Install)

	return sh.Run("sudo", "caddy", "-conf", "Caddyfile.local")
}

// Runs the proxy, with dev.drunkenfall.com settings
func (Run) ProxyDev() error {
	mg.Deps(Proxy.Install)

	_ = sh.Run("sudo", "killall", "caddy")
	return sh.Run("sudo", "caddy", "-conf", "Caddyfile")
}

type Docker mg.Namespace

// Builds the backend service docker image
func (Docker) Backend() error {
	mg.Deps(Docker.Frontend)

	return sh.Run("docker-compose", "build", "backend")
}

// Builds the frontend docker image
func (Docker) Frontend() error {
	mg.Deps(Docker.SetFrontendVersion)

	// Clean out previous build data
	err := sh.Run("sudo", "rm", "-rf", "./dist/static", "./dist/index.html")
	if err != nil {
		return err
	}

	err = sh.Run("docker-compose", "up", "--no-start", "frontend")
	if err != nil {
		return err
	}

	return sh.Run("docker-compose", "run", "--rm", "frontend", "npm", "run", "build")
}

// Generate the frontend version file from git
func (Docker) SetFrontendVersion() error {
	data := fmt.Sprintf("export const version = '%s (%s)'\n", version(), buildTime())
	err := ioutil.WriteFile("js/src/version.js", []byte(data), 0644)
	if err != nil {
		return err
	}

	log.Printf("Frontend version set to \"%s\"", data)
	return nil
}

type Code mg.Namespace

// Run all backend tests
func (Code) Test() error {
	env := make(map[string]string)
	env["GIN_MODE"] = "test"

	return sh.RunWith(env, "go", "test", "-coverprofile=cover.out", "-v", "./towerfall")
}

// Run all backend tests, fail fast
func (Code) TestFast() error {
	env := make(map[string]string)
	env["GIN_MODE"] = "test"

	return sh.RunWith(env, "go", "test", "-failfast", "-v", "./towerfall")
}

// Install go dependencies
func (Code) Vendor() error {
	return sh.Run("go", "ensure", "-v")
}

// Lints the backend code
func (Code) Lint() error {
	mg.Deps(Code.InstallLinter)

	return sh.Run("golangci-lint", "run", "--config", ".golangci-pedantic.yaml")
}

// Install the linter
func (Code) InstallLinter() error {
	path, err := exec.LookPath("golangci-lint")
	if err != nil {
		return err
	}

	if path != "" {
		log.Print("golangci-lint already installed - skipping installation")
		return nil
	}

	err = sh.Run("go", "get", "-coverprofile=cover.out", "-v", "./towerfall")
	if err != nil {
		return err
	}

	return nil
}

// Install the documentation generator
func (Code) InstallSwagger() error {
	_, err := exec.LookPath("swag")
	if err != nil {
		return sh.Run("go", "get", "github.com/swaggo/swag/cmd/swag")
	}

	log.Print("swag already installed - skipping installation")
	return nil
}

// Auto-generate the Swagger documentation
func (Code) GenerateDocs() error {
	mg.Deps(Code.InstallSwagger)
	return sh.Run("swag", "init")
}

type Proxy mg.Namespace

// Downloads and installs the proxy
func (Proxy) Install() error {
	path, err := exec.LookPath("caddy")
	if err != nil {
		return err
	}

	if path != "" {
		log.Print("caddy already installed - skipping installation")
		return nil
	}

	err = sh.Run("go", "get", "github.com/mholt/caddy/caddy")
	if err != nil {
		return err
	}

	err = sh.Run("go", "get", "github.com/caddyserver/builds")
	if err != nil {
		return err
	}

	path, err = sh.OutCmd("go", "env", "GOPATH")()
	if err != nil {
		return err
	}

	if err := os.Chdir(fmt.Sprintf("%s/src/github.com/mholt/caddy/caddy", path)); err != nil {
		return err
	}

	return sh.Run("go", "run", "build.go")
}

type Database mg.Namespace

// Open a postgres shell
func (Database) Shell() error {
	return psql()
}

// Dumps the current database
func (Database) Dump() error {
	return sh.Run(
		"pg_dump",
		"--host", "localhost", "--user", "postgres",
		"--file", dbFile,
		"drunkenfall",
	)
}

// Dumps a skeleton of the databaz, can be used for tests
func (Database) Skeleton() error {
	return sh.Run(
		"pg_dump",
		"--host", "localhost", "--user", "postgres",
		"--clean", "--create",

		// The levels, names and points tables have data that should be in the dumps
		"--exclude-table-data", "facebook_auth",
		"--exclude-table-data", "matches",
		"--exclude-table-data", "messages",
		"--exclude-table-data", "people",
		"--exclude-table-data", "player_states",
		"--exclude-table-data", "player_summaries",
		"--exclude-table-data", "players",
		"--exclude-table-data", "rounds",
		"--exclude-table-data", "tournament_levels",
		"--exclude-table-data", "tournament_names",
		"--exclude-table-data", "tournaments",

		"--file", "./data/skeleton.sql",
		"drunkenfall",
	)
}

// Resets from a database dump
func (Database) Reset() error {
	// TODO(thiderman): Add a backup dump before doing this
	err := psql("-c", "DROP DATABASE IF EXISTS drunkenfall")
	if err != nil {
		return err
	}

	err = psql("-c", "CREATE DATABASE drunkenfall")
	if err != nil {
		return err
	}

	return psql(
		"--file", dbFile,
		"drunkenfall",
	)
}

// Resets the test database from the database dump
func (Database) TestReset() error {
	err := psql("-c", "DROP DATABASE IF EXISTS test_drunkenfall")
	if err != nil {
		return err
	}

	err = psql("-c", "CREATE DATABASE test_drunkenfall")
	if err != nil {
		return err
	}

	return psql(
		"--file", dbFile,
		"test_drunkenfall",
	)
}
