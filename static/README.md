# Static files

The files here are synced to our [DO
Space](https://one-eye-assets.fra1.digitaloceanspaces.com/). It's an S3
compatible object storage, so any S3 client should be able to use it.

Any path under this one is also found under that domain, so
`img/covers/hero.jpg` is hosted on
[https://one-eye-assets.fra1.digitaloceanspaces.com/img/covers/hero.jpg](https://one-eye-assets.fra1.digitaloceanspaces.com/img/covers/hero.jpg).
Simple stuff!

## TODO
* As of this writing, the `streams/` directory has not been uploaded.
* The uploading process is manual. Should write a tool to sync if we're updating
  statics often.
