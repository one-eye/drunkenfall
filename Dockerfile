FROM golang:alpine as builder

ARG BUILD_ID
ENV DF_ROOT=/go/src/gitlab.com/one-eye/drunkenfall/
WORKDIR $DF_ROOT

RUN apk --no-cache add curl alpine-sdk ca-certificates
RUN curl -sL https://taskfile.dev/install.sh | sh -s -- -d -b /go/bin
RUN go get github.com/swaggo/swag/cmd/swag

COPY go.mod go.sum ./
# RUN go mod vendor

COPY faking/ ./faking/
COPY towerfall/ ./towerfall/
COPY cmd/ ./cmd/
COPY *.go ./
COPY ./Taskfile.yml ./

# The backend:build target technically depends on docs:generate, but it seems
# that Docker has a hard time finding those files since task helpfully tries to
# run the dependency commands asynchronously. Running them separately makes it
# work.
RUN task docs:generate
RUN task BUILD_ID=$BUILD_ID backend:build

FROM scratch
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=builder /go/src/gitlab.com/one-eye/drunkenfall/drunkenfall /
ENTRYPOINT ["/drunkenfall", "serve"]
