import store from '../core/store'

export default class LeaderboardPlayer {
  static fromObject (obj) {
    let l = new LeaderboardPlayer()
    Object.assign(l, obj)
    return l
  }

  get avatar () {
    return this.person.avatar
  }

  get person () {
    let ret = store.getters.getPerson(this.person_id)
    if (ret === undefined) {
      console.log("undefined person", this.person_id)
    }
    return ret
  }

  set person (x) {
    return
  }
}
