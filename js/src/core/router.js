import Vue from 'vue'
import Router from 'vue-router'

import About from '../components/About.vue'
import Admin from '../components/Admin.vue'
import Archers from '../components/Archers.vue'
import Casters from '../components/Caster.vue'
import Credits from '../components/Credits.vue'
import Disable from '../components/Disable.vue'
import EndQualifying from '../components/EndQualifying'
import Control from '../components/Control.vue'
import HUD from '../components/Hud.vue'
import Join from '../components/Join.vue'
import New from '../components/New.vue'
import NextScreen from '../components/NextScreen.vue'
import Participants from '../components/Participants.vue'
import Profile from '../components/Profile.vue'
import Register from '../components/Register.vue'
import Rules from '../components/Rules.vue'
import Settings from '../components/Settings.vue'
import Statusbar from '../components/Statusbar.vue'
import Start from '../components/Start.vue'
import Stream from '../components/Stream'
import TournamentList from '../components/TournamentList.vue'
import TournamentView from '../components/Tournament.vue'
import Qualifications from '../components/Qualifications.vue'

import DrunkenFallNew from '../components/new/DrunkenFall.vue'
import GroupNew from '../components/new/Group.vue'

Vue.use(Router)

var router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'start',
      component: Start
    },
    {
      path: '/about',
      name: 'about',
      component: About
    },
    {
      path: '/register',
      name: 'register',
      component: Register
    },
    {
      path: '/facebook/finalize',
      name: 'facebook',
      component: Settings
    },
    {
      path: '/tournaments/',
      name: 'tournaments',
      component: TournamentList
    },
    {
      path: '/tournaments/new/',
      name: 'new',
      component: New,
    },
    {
      path: '/tournaments/new/drunkenfall/',
      name: 'newDrunkenfall',
      component: DrunkenFallNew,
    },
    {
      path: '/tournaments/new/group/',
      name: 'newGroup',
      component: GroupNew,
    },
    {
      path: '/settings/',
      name: 'settings',
      component: Settings,
    },
    {
      path: '/archers/',
      name: 'archers',
      component: Archers,
    },
    {
      path: '/archers/:id',
      name: 'profile',
      component: Profile,
    },
    {
      path: '/admin',
      name: 'admin',
      component: Admin,
    },
    {
      path: '/rules',
      name: 'rules',
      component: Rules,
    },
    {
      path: '/admin/disable',
      name: 'disable',
      component: Disable,
    },
    {
      path: '/tournaments/:tournament/',
      name: 'tournament',
      component: TournamentView
    },
    {
      path: '/tournaments/:tournament/join/',
      name: 'join',
      component: Join
    },
    {
      path: '/tournaments/:tournament/participants/',
      name: 'participants',
      component: Participants
    },
    {
      path: '/tournaments/:tournament/casters/',
      name: 'casters',
      component: Casters
    },
    {
      path: '/tournaments/:tournament/next/',
      name: 'next',
      component: NextScreen
    },
    {
      path: '/tournaments/:tournament/control/',
      name: 'control',
      component: Control
    },
    {
      path: '/tournaments/:tournament/endqualifying/',
      name: 'endqualifying',
      component: EndQualifying
    },
    {
      path: '/tournaments/:tournament/qualifications',
      name: 'qualifications',
      component: Qualifications
    },
    {
      path: '/live/game',
      name: 'live',
      component: Stream
    },
    {
      path: '/live/qualifications',
      name: 'live-qualifications',
      component: Stream
    },
    {
      path: '/live/next',
      name: 'live-next',
      component: NextScreen
    },
    {
      path: '/live/status',
      name: 'live-status',
      component: Statusbar
    },
    {
      path: '/live/hud/',
      name: 'hud',
      component: HUD
    },
    {
      path: '/live/overview/',
      name: 'live-overview',
      component: TournamentView
    },
    {
      path: '/tournaments/:tournament/credits/',
      name: 'credits',
      component: Credits
    },
    {
      path: '/tournaments/:tournament/*',
      redirect: '/tournaments/:tournament/',
    },
  ],
})

export default router
