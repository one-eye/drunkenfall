package towerfall

import (
	"encoding/json"
	"time"

	"github.com/pkg/errors"
	"github.com/streadway/amqp"
	"gitlab.com/one-eye/drunkenfall/towerfall/messages"
	"gitlab.com/one-eye/drunkenfall/towerfall/models"
	"go.uber.org/zap"
)

type Listener struct {
	DB       *Database
	conn     *amqp.Connection
	incoming amqp.Queue
	ch       *amqp.Channel
	msgs     <-chan amqp.Delivery
	log      *zap.Logger
}

// NewListener sets up a new listener
func NewListener(conf *Config, db *Database) (*Listener, error) {
	var err error
	l := Listener{
		DB:  db,
		log: conf.Log,
	}

	l.conn, err = amqp.Dial(conf.Rabbit.URL)
	if err != nil {
		l.log.Fatal("Dial failed", zap.Error(err))
	}

	l.ch, err = l.conn.Channel()
	if err != nil {
		return nil, errors.New("Failed to open a channel")
	}

	l.incoming, err = l.ch.QueueDeclare(
		conf.Rabbit.Incoming, // name
		false,                // durable
		false,                // delete when unused
		false,                // exclusive
		false,                // no-wait
		nil,                  // arguments
	)

	if err != nil {
		return nil, errors.New("Failed to declare the incoming queue")
	}

	l.msgs, err = l.ch.Consume(
		conf.Rabbit.Incoming, // queue
		"",                   // consumer
		true,                 // auto-ack
		false,                // exclusive
		false,                // no-local
		false,                // no-wait
		nil,                  // args
	)

	if err != nil {
		return nil, errors.New("Failed to register the consumer")
	}

	return &l, err
}

func (l *Listener) Serve() {
	l.log.Info("Listening for AMQP messages...")
	defer l.ch.Close()

	for d := range l.msgs {
		t, err := l.DB.GetCurrentTournament()
		if err != nil {
			l.log.Info(
				"Could not get current tournament, skipping message",
				zap.Error(err),
			)
			continue
		}

		msg := d.Body
		go func() {
			err := l.handle(t, msg)
			if err != nil {
				l.log.Error(
					"Handling failed",
					zap.String("msg", string(msg)),
					zap.Error(err),
				)
			}
		}()
	}
}

func (l *Listener) handle(t *models.Tournament, body []byte) error {
	l.log.Debug("Incoming message", zap.Any("msg", body))
	msg := messages.IncomingGameMessage{
		Timestamp: time.Now(),
	}

	err := json.Unmarshal(body, &msg)
	if err != nil {
		l.log.Info("fail", zap.Error(err))
		return err
	}

	// Check if this is a meta communication from the game.
	switch msg.Type {
	case messages.GameConnect:
		// If this is a connect event, it means that the game wants to
		// know what's up with the current match.
		if globalPublisher == nil {
			return nil
		}

		// TODO(thiderman): This is duplicated from elsewhere and should be put into
		// a single function
		next, err := l.DB.NextMatch(t)
		if err != nil {
			t.Log.Error("Getting next match failed", zap.Error(err))
			return err
		}

		// If it is a special match, allow less than four players
		if len(next.Players) != 4 && next.Kind != models.SPECIAL {
			return models.ErrPublishIncompleteMatch
		}

		msg := messages.GameMatchMessage{
			Tournament: t.Slug,
			Level:      next.RealLevel(),
			Length:     next.Length,
			Ruleset:    next.Ruleset,
			Kind:       next.Kind,
		}

		for _, p := range next.Players {
			gp := messages.GamePlayer{
				TopName:    p.DisplayNames[0],
				BottomName: p.DisplayNames[1],
				Color:      p.NumericColor(),
				ArcherType: p.ArcherType,
			}
			msg.Players = append(msg.Players, gp)
		}

		t.Log.Info("Sending publish", zap.Any("match", msg))
		return globalPublisher.Publish(messages.GameMatch, msg)

	case messages.GameDisconnect:
		return nil
	}

	l.DB.messages <- &msg

	return nil
}
