package backend

import (
	"github.com/markbates/goth"
	"gitlab.com/one-eye/drunkenfall/towerfall/models"
	"go.uber.org/zap"
)

func (s *Server) getPersonFromAuth(user *goth.User) (*models.Person, error, bool) {
	pa := &models.PersonAuth{
		Provider:    "discord",
		RemoteID:    user.UserID,
		AvatarURL:   user.AvatarURL,
		Nick:        user.Name,
		Name:        user.Name,
		AccessToken: user.AccessToken,
	}

	person, err, created := s.DB.GetOrCreatePerson(pa)
	if err != nil {
		s.log.Error("Could not save person_auth", zap.Any("user", pa), zap.Error(err))
		return nil, err, false
	}

	return person, nil, created
}
