package backend

import (
	"errors"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"strconv"
	"strings"
	"syscall"
	"time"

	"github.com/gin-contrib/sessions"
	"github.com/gin-contrib/sessions/cookie"
	ginzap "github.com/gin-contrib/zap"
	"github.com/gin-gonic/gin"
	"github.com/markbates/goth"
	"github.com/markbates/goth/gothic"
	"github.com/markbates/goth/providers/discord"
	ginSwagger "github.com/swaggo/gin-swagger"
	"github.com/swaggo/gin-swagger/swaggerFiles"
	"gitlab.com/one-eye/drunkenfall/faking"
	"gitlab.com/one-eye/drunkenfall/towerfall"
	"gitlab.com/one-eye/drunkenfall/towerfall/messages"
	"gitlab.com/one-eye/drunkenfall/towerfall/models"
	"go.uber.org/zap"

	// The swagger docs generation needs to be triggered this way
	_ "gitlab.com/one-eye/drunkenfall/docs"
)

// Setup variables for the cookies.
var (
	CookieStoreName = "user-session"
	CookieStoreKey  = []byte("dtf")
)

var Global *Server

// Server is an abstraction that runs a web interface
type Server struct {
	DB        *towerfall.Database
	config    *towerfall.Config
	router    *gin.Engine
	log       *zap.Logger
	wb        *messages.WebsocketBroker
	publisher *towerfall.Publisher
	quit      chan os.Signal
}

// NewServer instantiates a server with an active database
func NewServer(config *towerfall.Config, db *towerfall.Database, wb *messages.WebsocketBroker) *Server {
	var err error
	s := Server{
		DB:     db,
		config: config,
		log:    config.Log,
		wb:     wb,
	}
	s.router = s.BuildRouter(wb)

	// Add zap logging
	s.router.Use(ginzap.Ginzap(s.log, time.RFC3339, true))

	if s.config.HasRabbit() {
		// Add the Rabbit publisher
		s.publisher, err = towerfall.NewPublisher(config)
		if err != nil {
			s.log.Fatal("Could not make publisher", zap.Error(err))
		}

		s.log.Info("RabbitMQ configured - publisher enabled")
	} else {
		s.log.Info("RabbitMQ not configured - publisher disabled")
	}

	Global = &s
	return &s
}

// BuildRouter sets up the routes
func (s *Server) BuildRouter(wb *messages.WebsocketBroker) *gin.Engine {
	gin.SetMode(s.config.Server.Mode)

	router := gin.New()
	router.Use(gin.Recovery())
	router.Use(RequestLogger(s.log))
	cookieStore := cookie.NewStore(CookieStoreKey)

	router.Use(sessions.Sessions(CookieStoreName, cookieStore))

	index := "dist/index.html"
	if _, err := os.Stat(index); !os.IsNotExist(err) {
		router.LoadHTMLFiles(index)
		router.NoRoute(func(c *gin.Context) {
			if strings.HasPrefix(c.Request.URL.Path, "/api") {
				c.JSON(404, nil)
				return
			}
			c.HTML(200, "index.html", gin.H{})
		})
		router.Static("/img", "./js/static/img")
		router.Static("/static", "./dist/static")
	}

	api := router.Group("/api")

	// Set up automatic documentation
	api.GET("/docs/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))

	// Public routes that don't require any authentication
	api.GET("/people/", s.PeopleHandler)
	api.GET("/people/:person", s.PersonHandler)
	api.GET("/user/", s.UserHandler)
	api.GET("/user/logout/", s.LogoutHandler)
	api.POST("/user/settings/", s.SettingsHandler)
	api.POST("/user/register/", s.RegisterHandler)
	api.GET("/fake/name/", s.FakeNameHandler)
	api.GET("/leagues/", s.LeagueListHandler)
	api.GET("/leagues/:id/", s.LeagueHandler)
	api.GET("/tournaments/", s.TournamentListHandler)
	api.GET("/tournaments/:id/", s.TournamentHandler)
	api.GET("/facebook/dblogin/:fb", s.DBLoginHandler)

	// World class implementation of continuous deployment
	api.POST("/perish/", s.PerishHandler)

	// Websockets are auth free
	api.GET("/auto-updater", func(c *gin.Context) {
		s.log.Info("Websocket setup")

		err := wb.HandleRequest(c.Writer, c.Request)
		if err != nil {
			s.log.Error("Handling websocket setup failed", zap.Error(err))
		}
	})

	// Authentication routes are also pre-auth free, perhaps unsurprisingly
	goth.UseProviders(discord.New(
		s.config.Discord.Key,
		s.config.Discord.Secret,
		s.config.Discord.Callback,
		discord.ScopeIdentify,
	))
	auth := api.Group("/auth")

	auth.GET("/discord", func(c *gin.Context) {
		q := c.Request.URL.Query()
		q.Add("provider", "discord")
		c.Request.URL.RawQuery = q.Encode()

		gothic.BeginAuthHandler(c.Writer, c.Request)
	})

	auth.GET("/discord/callback", func(c *gin.Context) {
		q := c.Request.URL.Query()
		q.Add("provider", "discord")
		c.Request.URL.RawQuery = q.Encode()
		user, err := gothic.CompleteUserAuth(c.Writer, c.Request)

		if err != nil {
			s.log.Error("Completing auth failed", zap.Error(err))
			_ = c.AbortWithError(http.StatusInternalServerError, err)
			return
		}

		s.log.Info("User authenticated successfully", zap.Any("user", user))
		person, err, created := s.getPersonFromAuth(&user)
		if err != nil {
			s.log.Error("Could not get a person from auth", zap.Error(err))
			_ = c.AbortWithError(http.StatusInternalServerError, err)
			return
		}
		err = person.StoreCookies(c)
		if err != nil {
			s.log.Error("Could not store cookies", zap.Error(err))
			_ = c.AbortWithError(http.StatusInternalServerError, err)
			return
		}

		s.log.Info("Person signed in", zap.Any("person", person))
		redir := "/"
		if created {
			redir = "/settings"
		}
		c.Redirect(http.StatusTemporaryRedirect, redir)
	})

	// Unprotected tournament routes - used by the interfaces
	t := api.Group("/tournaments/:id")
	t.GET("/players/", s.PlayerSummariesHandler)
	t.GET("/overview/", s.OverviewHandler)
	t.GET("/scores/", s.ScoresHandler)
	t.GET("/matches/", s.MatchesHandler)
	t.GET("/leaderboard/", s.TournamentLeaderboardHandler)
	t.GET("/queue/", s.QueueHandler)
	t.GET("/runnerups/", s.RunnerupsHandler)
	t.GET("/runnerups/playoff/", s.PlayoffRunnerupsHandler)
	t.GET("/credits/", s.CreditsHandler)

	// Protected routes - everything past this point requires that you
	// are at least a judge. If we are not in production, this is skipped
	if s.config.Production {
		api.Use(s.RequireJudge())
		s.log.Info("Requiring permissions for protected routes", zap.Bool("production", s.config.Production))
	} else {
		s.log.Info("Route protection permissions disabled", zap.Bool("production", s.config.Production))
	}

	api.GET("/user/disable/:person", s.DisableHandler)
	api.DELETE("/tournaments/", s.ClearTournamentHandler)

	api.POST("/tournaments/", s.NewHandler)

	// Reset the tournament helper now that authorization has been added.
	t = api.Group("/tournaments/:id")
	t.GET("/autoplay/", s.AutoplayMatchHandler)
	// t.GET("/join/", s.JoinHandler)
	t.GET("/time/:time", s.SetTimeHandler)
	t.POST("/endqualifying/", s.EndQualifyingHandler)
	t.GET("/toggle/:person", s.ToggleHandler)
	t.GET("/usurp/", s.UsurpTournamentHandler)
	t.POST("/message/", s.AddMessageToQueue)
	t.GET("/replay/", s.ReplayRound)
	t.GET("/start/", s.StartTournamentHandler)

	t.POST("/play/", s.StartPlayHandler)
	// t.POST("/casters/", s.CastersHandler)

	return router
}

// FakeNameHandler returns a fake name for a tournament
func (s *Server) FakeNameHandler(c *gin.Context) {
	name, numeral := faking.FakeTournamentTitle()
	c.JSON(http.StatusOK, gin.H{
		"name":    name,
		"numeral": numeral,
	})
}

// Serve starts serving
func (s *Server) Serve() error {
	srv := &http.Server{
		Addr:    fmt.Sprintf(":%d", s.config.Server.Port),
		Handler: s.router,
	}

	go func() {
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			s.log.Fatal("Listening failed", zap.Error(err))
		}
	}()

	// Set up listening for exit signals. They can be syscalls, or from the PerishHandler
	s.quit = make(chan os.Signal, 1)
	signal.Notify(s.quit, syscall.SIGINT, syscall.SIGTERM)

	msg := <-s.quit

	s.log.Info("Shutting down...", zap.Any("signal", msg))
	s.DB.Close()

	return nil
}

func (s *Server) getTournament(c *gin.Context) (*models.Tournament, error) {
	var tm *models.Tournament
	var err error

	id, found := c.Params.Get("id")
	if !found {
		log.Printf("going for id in URL but none is there")
		return nil, errors.New("no tournament ID in url")
	}

	if id == ":latest" {
		tm, err = s.DB.GetCurrentTournament()
		if err != nil {
			return nil, err
		}
	} else {

		x, err := strconv.Atoi(id)
		if err != nil {
			return nil, err
		}

		tm, err = s.DB.GetTournament(uint(x))
		if err != nil {
			return nil, err
		}
	}

	return tm, nil
}

func (s *Server) RequireJudge() gin.HandlerFunc {
	return func(c *gin.Context) {
		p, err := s.PersonFromSession(c)
		if err != nil {
			// If the request is from someone that's not signed in, we need to
			// signal that.
			s.log.Info(
				"Unauthorized",
				zap.String("path", c.Request.URL.Path),
			)
			c.JSON(http.StatusUnauthorized, gin.H{"message": "Please log in"})
			c.Abort()

		} else if p.Userlevel < models.PermissionJudge {
			// If not, we need to check if the permissions are enough
			s.log.Info(
				"Permission denied",
				zap.String("path", c.Request.URL.Path),
				zap.Uint("person", p.ID),
				zap.Int("userlevel", p.Userlevel),
			)
			c.JSON(http.StatusUnauthorized, gin.H{"message": "Permission denied"})
			c.Abort()
		}
		c.Next()
	}
}

// HasPermission checks that the user is allowed to do an action
func HasPermission(c *gin.Context, lvl int) bool {
	session := sessions.Default(c)
	l := session.Get("userlevel")

	// Nothing set in the session - no permission
	if l == nil {
		return false
	}

	return l.(int) >= lvl
}

func RequestLogger(log *zap.Logger) gin.HandlerFunc {
	return func(c *gin.Context) {
		start := time.Now().UTC()
		c.Next()
		end := time.Now().UTC()
		log.Info(
			"Request served",
			zap.String("path", c.Request.URL.Path),
			zap.Int("status", c.Writer.Status()),
			zap.String("method", c.Request.Method),
			zap.Duration("duration", end.Sub(start)),
			zap.String("ip", c.ClientIP()),
			zap.String("user_agent", c.Request.UserAgent()),
		)
	}
}
