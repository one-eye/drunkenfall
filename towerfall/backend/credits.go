package backend

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"go.uber.org/zap"
)

// CreditsHandler godoc
// @Summary Returns the data needed for the credits roll
// @Tags tournament
// @Accept json
// @Produce json
// @Success 200 {object} models.Credits
// @Failure 400 {object} backend.HTTPError
// @Failure 404 {object} backend.HTTPError
// @Failure 500 {object} backend.HTTPError
// @Router /tournaments/{id}/credits/ [get]
func (s *Server) CreditsHandler(c *gin.Context) {
	tm, err := s.getTournament(c)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	tlog := s.log.With(
		zap.String("path", c.Request.URL.Path),
		zap.String("tournament", tm.Slug),
	)

	credits, err := tm.GetCredits()

	if err != nil {
		tlog.Info("Could not get credits", zap.Error(err))
		c.JSON(http.StatusForbidden, gin.H{"message": err.Error()})
		return
	}

	tlog.Info("Credits grabbed")
	c.JSON(http.StatusOK, credits)
}
