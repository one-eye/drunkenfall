package backend

import (
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"go.uber.org/zap"
)

// LeagueHandler godoc
// @Summary Get a single league
// @Tags league
// @Accept json
// @Produce json
// @Param id path int true "League ID"
// @Success 200 {object} backend.LeagueResponse
// @Failure 400 {object} backend.HTTPError
// @Failure 404 {object} backend.HTTPError
// @Failure 500 {object} backend.HTTPError
// @Router /leagues/{id}/ [get]
func (s *Server) LeagueHandler(c *gin.Context) {
	log := s.log.With(
		zap.String("path", c.Request.URL.Path),
	)

	id, found := c.Params.Get("id")
	if !found {
		log.Error("No ID found")
		c.JSON(http.StatusInternalServerError, gin.H{"message": "No ID"})
		return
	}

	x, err := strconv.Atoi(id)
	if err != nil {
		log.Error("Couldn't strconv", zap.Error(err))
		c.JSON(http.StatusInternalServerError, gin.H{"message": "Couldn't strconv"})
		return
	}

	l, err := s.DB.GetLeague(uint(x))
	if err != nil {
		log.Error("Couldn't get league",
			zap.Int("id", x),
			zap.Error(err),
		)
		c.JSON(http.StatusInternalServerError, gin.H{"message": "Couldn't get league"})
		return
	}

	c.JSON(http.StatusOK, LeagueResponse{League: l})
}

// LeagueListHandler godoc
// @Summary Get all leagues
// @Description Returns a list of all leagues, with tournaments. Note that the
// @Description tournaments are not sorted, because the ORM is very good for such cases.
// @Tags league
// @Accept json
// @Produce json
// @Success 200 {object} backend.LeagueListResponse
// @Failure 400 {object} backend.HTTPError
// @Failure 404 {object} backend.HTTPError
// @Failure 500 {object} backend.HTTPError
// @Router /leagues/ [get]
func (s *Server) LeagueListHandler(c *gin.Context) {
	log := s.log.With(
		zap.String("path", c.Request.URL.Path),
	)

	ls, err := s.DB.GetLeagues()
	if err != nil {
		log.Error("Couldn't get leagues", zap.Error(err))
		c.JSON(http.StatusInternalServerError, gin.H{"message": "Couldn't get leagues"})
		return
	}
	c.JSON(http.StatusOK, LeagueListResponse{
		Leagues: ls,
	})
}
