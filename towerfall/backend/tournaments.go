package backend

import (
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"gitlab.com/one-eye/drunkenfall/towerfall/messages"
	"gitlab.com/one-eye/drunkenfall/towerfall/models"
	"go.uber.org/zap"
)

// NewHandler shows the page to create a new tournament
func (s *Server) NewHandler(c *gin.Context) {
	var req NewRequest
	var t *models.Tournament

	plog := s.log.With(zap.String("path", c.Request.URL.Path))

	err := c.BindJSON(&req)
	if err != nil {
		plog.Error("Bind failed", zap.Error(err))
		c.JSON(http.StatusInternalServerError, gin.H{"message": "Cannot JSON"})
		return
	}

	idlog := plog.With(zap.String("id", req.ID))
	t, err = models.NewTournament(req.Name, req.ID, req.Cover, req.Kind, req.Scheduled)
	if err != nil {
		idlog.Info("Creation failed")
		c.JSON(http.StatusInternalServerError, gin.H{"message": "Cannot create tournament"})
		return
	}

	err = s.DB.NewTournament(t)
	if err != nil {
		idlog.Info("Saving to database failed")
		c.JSON(http.StatusInternalServerError, gin.H{"message": "Could not save tournament"})
		return
	}

	err = s.wb.WebsocketUpdate("tournament", t)
	if err != nil {
		idlog.Info("Sending websocket update failed", zap.Error(err))
		c.JSON(http.StatusInternalServerError, gin.H{"message": "Couldn't publish websocket"})
		return
	}

	idlog.Info("Tournament created", zap.String("name", t.Name))
	c.JSON(http.StatusOK, gin.H{"id": t.ID})
}

// TournamentHandler godoc
// @Summary Current state of a tournament
// @Description Gets the entire current state of the tournament
// @Tags tournament
// @Accept json
// @Produce json
// @Param id path int true "Tournament ID"
// @Success 200 {object} backend.TournamentResponse
// @Failure 400 {object} backend.HTTPError
// @Failure 404 {object} backend.HTTPError
// @Failure 500 {object} backend.HTTPError
// @Router /tournaments/{id}/ [get]
func (s *Server) TournamentHandler(c *gin.Context) {
	tm, err := s.getTournament(c)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	tlog := s.log.With(
		zap.String("path", c.Request.URL.Path),
		zap.String("tournament", tm.Slug),
	)

	ms, err := s.DB.GetMatches(tm, "all")

	if err != nil {
		tlog.Error("Couldn't get matches", zap.Error(err))
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": "Couldn't get matches",
			"error":   err.Error(),
		})
		return
	}

	ps, err := s.DB.GetPlayerSummaries(tm)
	if err != nil {
		tlog.Info("Could not get summaries", zap.Error(err))
		c.JSON(http.StatusInternalServerError, gin.H{"message": "could not get summaries"})
		return
	}

	var rups []*models.PlayerSummary
	var sts []*models.PlayerState

	// These are only really needed when the tournament is running
	if tm.IsRunning() {
		rups, err = s.DB.GetAllRunnerups(tm)
		if err != nil {
			tlog.Info("Could not get summaries", zap.Error(err))
			c.JSON(http.StatusInternalServerError, gin.H{"message": "could not get summaries"})
			return
		}

		m, err := s.DB.CurrentMatch(tm)
		if err != nil {
			tlog.Info("Could not get current match", zap.Error(err))
			c.JSON(http.StatusInternalServerError, gin.H{"message": "could not get summaries"})
			return
		}

		sts, err = s.DB.GetPlayerStates(m)
		if err != nil {
			tlog.Info("Could not get player states", zap.Error(err))
			c.JSON(http.StatusInternalServerError, gin.H{"message": "could not get summaries"})
			return
		}
	}

	c.JSON(http.StatusOK, TournamentResponse{
		Tournament:      tm,
		Matches:         ms,
		Runnerups:       rups,
		PlayerSummaries: ps,
		PlayerStates:    sts,
	})
}

// TournamentListHandler godoc
// @Summary State of all tournaments
// @Description Returns a list of all tournaments
// @Tags tournament
// @Accept json
// @Produce json
// @Success 200 {object} backend.TournamentListResponse
// @Failure 400 {object} backend.HTTPError
// @Failure 404 {object} backend.HTTPError
// @Failure 500 {object} backend.HTTPError
// @Router /tournaments/ [get]
func (s *Server) TournamentListHandler(c *gin.Context) {
	ts, err := s.DB.GetTournaments()
	if err != nil {
		s.log.Error("Could not get tournaments", zap.Error(err))
		c.JSON(http.StatusInternalServerError, gin.H{"message": err.Error()})
		return
	}
	c.JSON(http.StatusOK, TournamentListResponse{
		Tournaments: ts,
	})
}

// StartTournamentHandler godoc
// @Summary ...it starts tournaments. What. ¯\_(ツ)_/¯
// @Tags tournament
// @Accept json
// @Produce json
// @Success 200 {object} backend.HTTPResponse
// @Failure 400 {object} backend.HTTPError
// @Failure 404 {object} backend.HTTPError
// @Failure 500 {object} backend.HTTPError
// @Router /tournaments/{id}/start/ [get]
func (s *Server) StartTournamentHandler(c *gin.Context) {
	tm, err := s.getTournament(c)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	tlog := s.log.With(
		zap.String("path", c.Request.URL.Path),
		zap.String("tournament", tm.Slug),
	)

	err = tm.StartTournament()
	if err != nil {
		tlog.Info("Could not start tournament", zap.Error(err))
		c.JSON(http.StatusInternalServerError, gin.H{"message": err.Error()})
		return
	}

	err = s.DB.SaveTournament(tm)
	if err != nil {
		tlog.Info("Could not save tournament", zap.Error(err))
		c.JSON(http.StatusInternalServerError, gin.H{"message": err.Error()})
		return
	}

	err = s.PublishNext(tm)
	if err != nil && err != models.ErrPublishDisconnected {
		tlog.Info("Could not publish next match", zap.Error(err))
		c.JSON(http.StatusInternalServerError, gin.H{"message": err.Error()})
		return
	}

	ms, err := s.DB.GetMatches(tm, "all")
	if err != nil {
		tlog.Info("Couldn't get matches", zap.Error(err))
		c.JSON(http.StatusInternalServerError, gin.H{"message": err.Error()})
		return
	}

	runnerups, err := s.DB.GetAllRunnerups(tm)
	if err != nil {
		tlog.Info("Couldn't get runnerups", zap.Error(err))
		c.JSON(http.StatusInternalServerError, gin.H{"message": err.Error()})
		return
	}

	summaries, err := s.DB.GetPlayerSummaries(tm)
	if err != nil {
		tlog.Info("Couldn't get summaries", zap.Error(err))
		c.JSON(http.StatusInternalServerError, gin.H{"message": err.Error()})
		return
	}

	err = s.wb.MatchEndUpdate(tm, ms, runnerups, summaries)
	if err != nil {
		tlog.Info("Could not send tournament update", zap.Error(err))
		c.JSON(http.StatusInternalServerError, gin.H{"message": err.Error()})
	}

	tlog.Info("Tournament started")
	c.JSON(http.StatusOK, HTTPResponse{})
}

// PlayerSummariesHandler godoc
// @Summary Returns the player summaries for all the players in a tournament
// @Tags tournament
// @Accept json
// @Produce json
// @Success 200 {object} backend.PlayerSummariesResponse
// @Failure 400 {object} backend.HTTPError
// @Failure 404 {object} backend.HTTPError
// @Failure 500 {object} backend.HTTPError
// @Router /tournaments/{id}/players/ [get]
func (s *Server) PlayerSummariesHandler(c *gin.Context) {
	tm, err := s.getTournament(c)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	tlog := s.log.With(
		zap.String("path", c.Request.URL.Path),
		zap.String("tournament", tm.Slug),
	)

	ps, err := s.DB.GetPlayerSummaries(tm)
	if err != nil {
		tlog.Info("Could not get summaries", zap.Error(err))
		c.JSON(http.StatusInternalServerError, gin.H{"message": "could not get summaries"})
		return
	}

	c.JSON(http.StatusOK, PlayerSummariesResponse{ps})
}

// OverviewHandler godoc
// @Summary Returns the player summaries for all the players in a tournament
// @Tags tournament
// @Accept json
// @Produce json
// @Success 200 {object} backend.SummariesResponse
// @Failure 400 {object} backend.HTTPError
// @Failure 404 {object} backend.HTTPError
// @Failure 500 {object} backend.HTTPError
// @Router /tournaments/{id}/overview/ [get]
func (s *Server) OverviewHandler(c *gin.Context) {
	tm, err := s.getTournament(c)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	tlog := s.log.With(
		zap.String("path", c.Request.URL.Path),
		zap.String("tournament", tm.Slug),
	)

	ps, err := s.DB.GetOverview(tm)
	if err != nil {
		tlog.Info("Could not get summaries", zap.Error(err))
		c.JSON(http.StatusInternalServerError, gin.H{"message": "could not get summaries"})
		return
	}

	c.JSON(http.StatusOK, SummariesResponse{ps})
}

// ScoresHandler godoc
// @Summary Returns the player summaries for all the players in a tournament
// @Tags tournament
// @Accept json
// @Produce json
// @Success 200 {object} backend.ScoreResponse
// @Failure 400 {object} backend.HTTPError
// @Failure 404 {object} backend.HTTPError
// @Failure 500 {object} backend.HTTPError
// @Router /tournaments/{id}/scores/ [get]
func (s *Server) ScoresHandler(c *gin.Context) {
	tm, err := s.getTournament(c)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	tlog := s.log.With(
		zap.String("path", c.Request.URL.Path),
		zap.String("tournament", tm.Slug),
	)

	ps, err := s.DB.GetScores(tm)
	if err != nil {
		tlog.Info("Could not get scores", zap.Error(err))
		c.JSON(http.StatusInternalServerError, gin.H{"message": "could not get scores"})
		return
	}

	c.JSON(http.StatusOK, ScoreResponse{ps})
}

// TournamentLeaderboardHandler godoc
// @Summary Returns a special JSON view of the current leaderboard
// @Tags tournament
// @Accept json
// @Produce json
// @Success 200 {object} backend.TournamentLeaderboardResponse
// @Failure 400 {object} backend.HTTPError
// @Failure 404 {object} backend.HTTPError
// @Failure 500 {object} backend.HTTPError
// @Router /tournaments/{id}/players/ [get]
func (s *Server) TournamentLeaderboardHandler(c *gin.Context) {
	tm, err := s.getTournament(c)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	tlog := s.log.With(
		zap.String("path", c.Request.URL.Path),
		zap.String("tournament", tm.Slug),
	)

	ps, err := s.DB.GetTournamentLeaderboard(tm)
	if err != nil {
		tlog.Info("Could not get tournament leaderboard", zap.Error(err))
		c.JSON(http.StatusInternalServerError, gin.H{"message": "could not get tournament leaderboard"})
		return
	}

	c.JSON(http.StatusOK, TournamentLeaderboardResponse{ps})
}

// QueueHandler godoc
// @Summary Returns the current queue
// @Description TODO(thiderman): Don't return the leaderboard.
// @Tags tournament
// @Accept json
// @Produce json
// @Success 200 {object} backend.PeopleResponse
// @Failure 400 {object} backend.HTTPError
// @Failure 404 {object} backend.HTTPError
// @Failure 500 {object} backend.HTTPError
// @Router /tournaments/{id}/queue/ [get]
func (s *Server) QueueHandler(c *gin.Context) {
	tm, err := s.getTournament(c)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	tlog := s.log.With(
		zap.String("path", c.Request.URL.Path),
		zap.String("tournament", tm.Slug),
	)

	ps, err := s.DB.GetQueuedPlayers(tm)
	if err != nil {
		tlog.Info("Could not get tournament leaderboard", zap.Error(err))
		c.JSON(http.StatusInternalServerError, gin.H{"message": "could not get tournament leaderboard"})
		return
	}

	c.JSON(http.StatusOK, PeopleResponse{
		People: ps,
	})
}

// RunnerupsHandler godoc
// @Summary Returns the current runnerups for a tournament, in playing order
// @Tags tournament
// @Accept json
// @Produce json
// @Success 200 {object} backend.PlayerSummariesResponse
// @Failure 400 {object} backend.HTTPError
// @Failure 404 {object} backend.HTTPError
// @Failure 500 {object} backend.HTTPError
// @Router /tournaments/{id}/runnerups/ [get]
func (s *Server) RunnerupsHandler(c *gin.Context) {
	tm, err := s.getTournament(c)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	tlog := s.log.With(
		zap.String("path", c.Request.URL.Path),
		zap.String("tournament", tm.Slug),
	)

	ps, err := s.DB.GetAllRunnerups(tm)
	if err != nil {
		tlog.Info("Could not get summaries", zap.Error(err))
		c.JSON(http.StatusInternalServerError, gin.H{"message": "could not get summaries"})
		return
	}

	c.JSON(http.StatusOK, PlayerSummariesResponse{ps})
}

// PlayoffRunnerupsHandler godoc
// @Summary Returns the current playoff runnerups for a tournament, in playing order
// @Description The scores are only for the playoffs and not for the full tournament.
// @Tags tournament
// @Accept json
// @Produce json
// @Success 200 {object} backend.PlayerSummariesResponse
// @Failure 400 {object} backend.HTTPError
// @Failure 404 {object} backend.HTTPError
// @Failure 500 {object} backend.HTTPError
// @Router /tournaments/{id}/runnerups/playoffs/ [get]
func (s *Server) PlayoffRunnerupsHandler(c *gin.Context) {
	tm, err := s.getTournament(c)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	tlog := s.log.With(
		zap.String("path", c.Request.URL.Path),
		zap.String("tournament", tm.Slug),
	)

	ps, err := s.DB.GetPlayoffRunnerups(tm)
	if err != nil {
		tlog.Info("Could not get runnerups", zap.Error(err))
		c.JSON(http.StatusInternalServerError, gin.H{"message": "could not get runnerups"})
		return
	}

	c.JSON(http.StatusOK, PlayerSummariesResponse{ps})
}

// StartPlayHandler godoc
// @Summary Sends a message to the game that it is time to  start doing whatever is next.
// @Tags tournament
// @Accept json
// @Produce json
// @Success 200 {object} backend.StartPlayResponse
// @Failure 400 {object} backend.HTTPError
// @Failure 404 {object} backend.HTTPError
// @Failure 500 {object} backend.StartPlayResponse
// @Router /tournaments/{id}/play/ [post]
func (s *Server) StartPlayHandler(c *gin.Context) {
	s.log.Info("sending start_play")
	err := s.publisher.Publish(messages.GameStartPlay, messages.StartPlayMessage{})
	if err != nil {
		s.log.Info("Publishing failed", zap.Error(err))
		c.JSON(http.StatusInternalServerError, StartPlayResponse{false})
		return
	}
	c.JSON(http.StatusOK, StartPlayResponse{true})
}

// MatchesHandler godoc
// @Summary Gets the matches for a tournament
// @Tags tournament
// @Accept json
// @Produce json
// @Success 200 {object} backend.MatchesResponse
// @Failure 400 {object} backend.HTTPError
// @Failure 404 {object} backend.HTTPError
// @Failure 500 {object} backend.HTTPError
// @Router /tournaments/{id}/matches/ [get]
func (s *Server) MatchesHandler(c *gin.Context) {
	tm, err := s.getTournament(c)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	tlog := s.log.With(
		zap.String("path", c.Request.URL.Path),
		zap.String("tournament", tm.Slug),
	)
	tlog.Info("Getting matches...")

	ms, err := s.DB.GetMatches(tm, "all")

	if err != nil {
		tlog.Error("Couldn't get matches", zap.Error(err))
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": "Couldn't get matches",
			"error":   err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, MatchesResponse{ms})
}

// ToggleHandler godoc
// @Summary Adds or removes a player from a tournament
// @Tags tournament
// @Accept json
// @Produce json
// @Success 200 {object} backend.HTTPResponse
// @Failure 400 {object} backend.HTTPError
// @Failure 404 {object} backend.HTTPError
// @Failure 500 {object} backend.HTTPError
// @Router /tournaments/{id}/toggle/{person} [get]
func (s *Server) ToggleHandler(c *gin.Context) {
	t, err := s.getTournament(c)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	tlog := s.log.With(
		zap.String("path", c.Request.URL.Path),
		zap.String("tournament", t.Slug),
	)

	id, found := c.Params.Get("person")
	if !found {
		tlog.Info("Couldn't get person ID")
		c.JSON(http.StatusBadRequest, gin.H{"message": "Couldn't get person ID"})
		return
	}
	pslog := tlog.With(zap.String("player_id", id))

	pid, err := strconv.Atoi(id)
	if err != nil {
		pslog.Info("Couldn't convert person ID")
		c.JSON(http.StatusBadRequest, gin.H{"message": "Couldn't convert person ID"})
		return
	}

	err = s.DB.TogglePlayer(t, uint(pid))
	if err != nil {
		pslog.Error("Could not toggle player", zap.Error(err))
		c.JSON(http.StatusBadRequest, gin.H{
			"message": "Couldn't toggle player",
			"error":   err.Error(),
		})
		return
	}

	summaries, err := s.DB.GetPlayerSummaries(t)
	if err != nil {
		pslog.Info("Couldn't get player summaries")
		c.JSON(http.StatusBadRequest, gin.H{"message": "Couldn't convert person ID"})
		return
	}

	err = s.wb.PlayerSummariesUpdate(t, summaries)
	if err != nil {
		pslog.Error("Could send player summary update", zap.Error(err))
	}

	pslog.Info("Player toggled")
	c.JSON(http.StatusOK, HTTPResponse{})
}

// SetTimeHandler godoc
// @Summary Sets the pause time for the next match
// @Tags tournament
// @Accept json
// @Produce json
// @Success 200 {object} backend.HTTPResponse
// @Failure 400 {object} backend.HTTPError
// @Failure 404 {object} backend.HTTPError
// @Failure 500 {object} backend.HTTPError
// @Router /tournaments/{id}/time/{minutes} [get]
func (s *Server) SetTimeHandler(c *gin.Context) {
	t, err := s.getTournament(c)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	tlog := s.log.With(
		zap.String("path", c.Request.URL.Path),
		zap.String("tournament", t.Slug),
	)

	st, found := c.Params.Get("time")
	if !found {
		tlog.Error("Couldn't get time")
		c.JSON(http.StatusBadRequest, gin.H{"message": "Couldn't get time"})
		return
	}

	x, err := strconv.Atoi(st)
	if err != nil {
		tlog.Error("Couldn't parse time")
		c.JSON(http.StatusBadRequest, gin.H{"message": "Couldn't parse time"})
		return
	}

	m, err := s.DB.NextMatch(t)
	if err != nil {
		tlog.Error("Couldn't get next match")
		c.JSON(http.StatusBadRequest, gin.H{"message": "Couldn't get next match"})
		return
	}

	mlog := tlog.With(zap.Int("match", m.Index))

	// If the match is already started, we need to bail
	if m.IsStarted() {
		mlog.Warn("Match already started")
		c.JSON(http.StatusForbidden, gin.H{"message": "Match already started"})
		return
	}

	m.SetTime(x)
	err = s.DB.SaveMatch(m)
	if err != nil {
		mlog.Info("Could not save match", zap.Error(err))
		c.JSON(http.StatusInternalServerError, gin.H{"message": err.Error()})
		return
	}

	ms, err := s.DB.GetMatches(t, "all")
	if err != nil {
		mlog.Error("Couldn't get matches", zap.Error(err))
		c.JSON(http.StatusInternalServerError, gin.H{"message": err.Error()})
		return
	}

	err = s.wb.MatchesUpdate(t, ms)
	if err != nil {
		mlog.Error("Couldn't send time websocket update", zap.Error(err))
		c.JSON(http.StatusInternalServerError, gin.H{"message": err.Error()})
		return
	}

	mlog.Info("Time set", zap.Int("minutes", x))
	c.JSON(http.StatusOK, HTTPResponse{})
}

// EndQualifyingHandler godoc
// @Summary Sets the cutoff time for the qualifying rounds
// @Description The cutoff time works so that the tournament will continue to schedule qualifying matches until the timestamp has passed.
// @Description This is a necessity due to the nature of always scheduling two // matches ahead.
// @Description Note that the 'time.Time' parameter called 'time' cannot be
// @Description documented at this time due to a swaggo error.
// @Tags tournament
// @Accept json
// @Produce json
// @Success 200 {object} backend.HTTPResponse
// @Failure 400 {object} backend.HTTPError
// @Failure 404 {object} backend.HTTPError
// @Failure 500 {object} backend.HTTPError
// @Router /tournaments/{id}/endqualifying/ [post]
func (s *Server) EndQualifyingHandler(c *gin.Context) {
	var req EndQualifyingRequest

	plog := s.log.With(zap.String("path", c.Request.URL.Path))

	tm, err := s.getTournament(c)
	if err != nil {
		plog.Error("Couldn't get tournament", zap.Error(err))
	}

	err = c.BindJSON(&req)
	if err != nil {
		plog.Error("Bind failed", zap.Error(err))
		c.JSON(http.StatusInternalServerError, gin.H{"message": "Cannot JSON"})
	}

	tm.EndQualifyingRounds(req.Time)
	err = s.DB.SaveTournament(tm)
	if err != nil {
		plog.Info("Could not save tournament", zap.Error(err))
		c.JSON(http.StatusInternalServerError, gin.H{"message": err.Error()})
		return
	}

	plog.Info("Qualifying rounds set to end", zap.Time("end", req.Time))
	c.JSON(http.StatusOK, HTTPResponse{})
}
