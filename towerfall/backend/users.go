package backend

import (
	"errors"
	"net/http"
	"strconv"

	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
	"gitlab.com/one-eye/drunkenfall/towerfall/models"
	"go.uber.org/zap"
)

// UserHandler godoc
// @Summary Returns data about the current user
// @Tags user
// @Accept json
// @Produce json
// @Success 200 {object} models.Person
// @Failure 400 {object} backend.AuthResponse
// @Failure 404 {object} backend.HTTPError
// @Failure 500 {object} backend.HTTPError
// @Router /user/ [get]
func (s *Server) UserHandler(c *gin.Context) {
	plog := s.log.With(zap.String("path", c.Request.URL.Path))
	if !HasPermission(c, models.PermissionPlayer) {
		plog.Info("Not signed in")
		c.JSON(http.StatusOK, AuthResponse{false})
		return
	}

	p, err := s.PersonFromSession(c)
	if err != nil {
		plog.Info("No person found for session")
		c.JSON(http.StatusOK, AuthResponse{false})
		return
	}

	c.JSON(http.StatusOK, p)
}

func (s *Server) DBLoginHandler(c *gin.Context) {
	plog := s.log.With(zap.String("path", c.Request.URL.Path))
	id, found := c.Params.Get("fb")

	p, err := s.DB.GetPersonFromFacebook(id)
	if err != nil {
		plog.Info(
			"No person found for session",
			zap.String("id", id),
			zap.Bool("found", found), zap.Error(err),
		)
		c.JSON(http.StatusOK, gin.H{"authenticated": false})
		return
	}

	err = p.StoreCookies(c)
	if err != nil {
		plog.Error(
			"Couldn't store cookies",
			zap.String("id", id),
			zap.Bool("found", found),
			zap.Error(err),
		)
		c.JSON(http.StatusOK, gin.H{"authenticated": false})
		return
	}
	c.Redirect(http.StatusMovedPermanently, "/")
}

// DisableHandler godoc
// @Summary Disables or enables players
// @Tags user
// @Accept json
// @Produce json
// @Success 200 {object} backend.HTTPResponse
// @Failure 400 {object} backend.HTTPError
// @Failure 404 {object} backend.HTTPError
// @Failure 500 {object} backend.HTTPError
// @Router /user/disable/{person} [get]
func (s *Server) DisableHandler(c *gin.Context) {
	plog := s.log.With(zap.String("path", c.Request.URL.Path))

	id, found := c.Params.Get("person")
	if !found {
		plog.Error("Couldn't get person ID")
		c.JSON(http.StatusBadRequest, gin.H{"message": "Couldn't get person ID"})
		return
	}

	pid, err := strconv.Atoi(id)
	if err != nil {
		plog.Info("Couldn't convert person ID")
		c.JSON(http.StatusBadRequest, gin.H{"message": "Couldn't convert person ID"})
		return
	}

	err = s.DB.DisablePerson(uint(pid))
	if err != nil {
		plog.Error("couldn't disable person", zap.Error(err))
		c.JSON(http.StatusInternalServerError, gin.H{"message": "disable failed"})
		return
	}

	plog.Info("Person disabled", zap.String("person", id))
	c.JSON(http.StatusOK, HTTPResponse{})
}

// LogoutHandler godoc
// @Summary Logs out the user
// @Tags user
// @Accept json
// @Produce json
// @Success 200 {object} backend.HTTPResponse
// @Failure 400 {object} backend.HTTPError
// @Failure 404 {object} backend.HTTPError
// @Failure 500 {object} backend.HTTPError
// @Router /user/logout/ [get]
func (s *Server) LogoutHandler(c *gin.Context) {
	p, err := s.PersonFromSession(c)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"message": "Logout failed (no session)"})
		return
	}
	plog := s.log.With(
		zap.String("path", c.Request.URL.Path),
		zap.Uint("user", p.ID),
	)

	err = p.RemoveCookies(c)
	if err != nil {
		plog.Error("Removing cookies failed", zap.Error(err))
		c.JSON(http.StatusInternalServerError, gin.H{"message": "Logout failed (cookie failure)"})
		return
	}

	plog.Info("User logged out")
	c.JSON(http.StatusOK, gin.H{"message": "Done"})
}

// SettingsHandler godoc
// @Summary Updates settings
// @Tags user
// @Accept json
// @Produce json
// @Success 200 {object} backend.HTTPResponse
// @Failure 400 {object} backend.HTTPError
// @Failure 404 {object} backend.HTTPError
// @Failure 500 {object} backend.HTTPError
// @Router /user/settings/ [post]
func (s *Server) SettingsHandler(c *gin.Context) {
	plog := s.log.With(zap.String("path", c.Request.URL.Path))
	var req SettingsPostRequest

	err := c.BindJSON(&req)
	if err != nil {
		plog.Info("Couldn't JSON")
		c.JSON(http.StatusInternalServerError, gin.H{"message": "Cannot JSON"})
		return
	}

	p, err := s.PersonFromSession(c)
	if err != nil {
		plog.Info("No person found")
		c.JSON(http.StatusInternalServerError, gin.H{"message": "No person found"})
		return
	}

	p.Name = req.Name
	p.Nick = req.Nick
	p.ArcherType = req.ArcherType
	p.PreferredColor = req.Color
	err = s.DB.SavePerson(p)
	if err != nil {
		plog.Error("Saving failed", zap.Error(err))
		c.JSON(http.StatusInternalServerError, gin.H{"message": "Database failed"})
		return
	}

	err = p.StoreCookies(c)
	if err != nil {
		plog.Error("Setting cookies failed", zap.Error(err))
		c.JSON(http.StatusInternalServerError, gin.H{"message": "Cookies failed"})
		return
	}

	plog.Info("Person saved", zap.Uint("person", p.ID))
	c.JSON(http.StatusOK, gin.H{"person": p})
}

// RegisterHandler godoc
// @Summary Adds a new user.
// @Description If a tournament is currently running, immediately add the user to there.
// @Tags user
// @Accept json
// @Produce json
// @Success 200 {object} models.Person
// @Failure 400 {object} backend.HTTPError
// @Failure 404 {object} backend.HTTPError
// @Failure 500 {object} backend.HTTPError
// @Router /user/register/ [post]
func (s *Server) RegisterHandler(c *gin.Context) {
	plog := s.log.With(zap.String("path", c.Request.URL.Path))
	var req SettingsPostRequest

	err := c.BindJSON(&req)
	if err != nil {
		plog.Info("Couldn't JSON")
		c.JSON(http.StatusInternalServerError, gin.H{"message": "Cannot JSON"})
		return
	}

	p := &models.Person{}
	p.Name = req.Name
	p.Nick = req.Nick
	p.ArcherType = req.ArcherType
	p.PreferredColor = req.Color

	err = s.DB.AddPerson(p)
	if err != nil {
		plog.Error("Saving failed", zap.Error(err))
		c.JSON(http.StatusInternalServerError, gin.H{"message": "Database failed"})
		return
	}

	t, err := s.DB.GetCurrentTournament()
	if err != nil {
		plog.Info("Getting latest tournament failed", zap.Error(err))
		c.JSON(http.StatusOK, gin.H{"message": "Player registered, but not added to tournament"})
		return
	}

	tlog := plog.With(zap.Uint("tournament", t.ID))

	err = s.DB.AddPlayer(t, models.NewPlayerSummary(p))
	if err != nil {
		tlog.Error("Getting latest tournament failed", zap.Error(err))
		c.JSON(http.StatusInternalServerError, gin.H{"message": "Getting tournament failed"})
		return
	}

	tlog.Info("Person saved and added to tournament", zap.Uint("person", p.ID))
	c.JSON(http.StatusOK, p)
}

// PersonFromSession returns the Person{} object attached to the session
func (s *Server) PersonFromSession(c *gin.Context) (*models.Person, error) {
	if c == nil {
		return nil, errors.New("no context provided")
	}

	session := sessions.Default(c)
	uid := session.Get("user")
	if uid == nil {
		return nil, errors.New("could not get user id")
	}

	id := uid.(uint)
	p, err := s.DB.GetPerson(id)
	if err != nil {
		s.log.Warn("Nonexisting session",
			zap.Uint("id", id),
			zap.Error(err),
		)
		return nil, errors.New("no session found")
	}

	return p, nil
}
