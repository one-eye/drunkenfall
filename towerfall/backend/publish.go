package backend

import (
	"gitlab.com/one-eye/drunkenfall/towerfall/messages"
	"gitlab.com/one-eye/drunkenfall/towerfall/models"
	"go.uber.org/zap"
)

// PublishNext sends information about the next match to the game
//
// It only does this if the match already has four players. If it does
// not, it's a semi that needs backfilling, and then the backfilling
// will make the publish. This should always be called before the
// match is started, so t.NextMatch() can always safely be used.
func (s *Server) PublishNext(t *models.Tournament) error {
	if s.publisher == nil {
		return nil
	}

	next, err := s.DB.NextMatch(t)
	if err != nil {
		t.Log.Error("Getting next match failed", zap.Error(err))
		return err
	}

	// If it is a special match, allow less than four players
	if len(next.Players) != 4 && next.Kind != models.SPECIAL {
		return models.ErrPublishIncompleteMatch
	}

	msg := messages.GameMatchMessage{
		Tournament: t.Slug,
		Level:      next.RealLevel(),
		Length:     next.Length,
		Ruleset:    next.Ruleset,
		Kind:       next.Kind,
	}

	for _, p := range next.Players {
		gp := messages.GamePlayer{
			TopName:    p.DisplayNames[0],
			BottomName: p.DisplayNames[1],
			Color:      p.NumericColor(),
			ArcherType: p.ArcherType,
		}
		msg.Players = append(msg.Players, gp)
	}

	t.Log.Info("Sending publish", zap.Any("match", msg))
	return s.publisher.Publish(messages.GameMatch, msg)
}
