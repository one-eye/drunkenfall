package backend

import (
	"time"

	"gitlab.com/one-eye/drunkenfall/towerfall/models"
)

// NewRequest is the request to make a new tournament
type NewRequest struct {
	Name      string    `json:"name" binding:"required"`
	ID        string    `json:"id" binding:"required"`
	Scheduled time.Time `json:"scheduled" binding:"required"`
	Cover     string    `json:"cover"`
	Fake      bool      `json:"fake"`
	Kind      string    `json:"kind" form:",default=headhunters"`
}

// SettingsPostRequest is a settings update, or a new user register
type SettingsPostRequest struct {
	Name       string `json:"name"`
	Nick       string `json:"nick"`
	Color      string `json:"color"`
	ArcherType int    `json:"archer_type"`
}

// PerishRequest is the request for the server to go die
type PerishRequest struct {
	Key string `json:"key"`
}

type FakeNameResponse struct {
	Name    string `json:"name"`
	Numeral string `json:"numeral"`
}

type EndQualifyingRequest struct {
	Time time.Time `json:"time"`
}

// In lack of a better place to put them, set response types here
type HTTPError struct {
	Message string `json:"message"`
	Error   string `json:"error"`
}

// HTTPResponse is a generic empty response
type HTTPResponse struct {
}

// TournamentResponse represents a single tournament, with matches, summaries,
// runnerups and states attached.
type TournamentResponse struct {
	Tournament      *models.Tournament      `json:"tournament"`
	Matches         []*models.Match         `json:"matches"`
	PlayerSummaries []*models.PlayerSummary `json:"player_summaries"`
	Runnerups       []*models.PlayerSummary `json:"runnerups"`
	PlayerStates    []*models.PlayerState   `json:"player_states"`
}

// TournamentListResponse is a list of Tournament objects
type TournamentListResponse struct {
	Tournaments []*models.Tournament `json:"tournaments"`
}

// LeagueResponse is a single League
type LeagueResponse struct {
	League *models.League `json:"league"`
}

// LeagueListResponse is a list of League objects
type LeagueListResponse struct {
	Leagues []*models.League `json:"leagues"`
}

// PlayerSummariesResponse is a list PlayerSummary objects
type PlayerSummariesResponse struct {
	PlayerSummaries []*models.PlayerSummary `json:"player_summaries"`
}

// SummariesResponse is a list of Summary objects
type SummariesResponse struct {
	Summaries []*models.Summary `json:"summaries"`
}

// ScoreResponse is a list of Score objects
type ScoreResponse struct {
	Scores []*models.Score `json:"scores"`
}

// TournamentLeaderboardResponse is a list TournamentLeaderboardPlayer objects
type TournamentLeaderboardResponse struct {
	Players []*models.TournamentLeaderboardPlayer `json:"players"`
}

// StartPlayResponse tells whether a start_play messages were sent or not
type StartPlayResponse struct {
	Sent bool `json:"sent"`
}

// AuthResponse returns boolean if we are authenticated or not
type AuthResponse struct {
	Authenticated bool `json:"authenticated"`
}

// MatchesResponse is a list of matches
type MatchesResponse struct {
	Matches []*models.Match `json:"matches"`
}

// PeopleResponse is a list of everyone registered in the app, with a leaderboard
type PeopleResponse struct {
	People      []*models.Person            `json:"people"`
	Leaderboard []*models.LeaderboardPlayer `json:"leaderboard"`
}

// PesonResponse is like PeopleResponse, but for a single person
type PersonResponse struct {
	Person      *models.Person            `json:"person"`
	Leaderboard *models.LeaderboardPlayer `json:"leaderboard"`
}
