package backend

import (
	"net/http"
	"syscall"
	"time"

	"github.com/gin-gonic/gin"
	"go.uber.org/zap"
)

// LeagueHandler godoc
// @Summary Exit the application
// @Description This is used to trigger a restart and upgrade
// @Tags league
// @Accept json
// @Produce json
// @Success 200 {object} backend.HTTPResponse
// @Failure 400 {object} backend.HTTPError
// @Failure 403 {object} backend.HTTPError
// @Failure 500 {object} backend.HTTPError
// @Router /perish/ [post]
func (s *Server) PerishHandler(c *gin.Context) {
	var req PerishRequest

	log := s.log.With(
		zap.String("path", c.Request.URL.Path),
	)

	if s.config.PerishKey == "" {
		log.Info("No key set, cannot perish")
		c.JSON(http.StatusInternalServerError, gin.H{"message": ":ohno:"})
		return
	}

	err := c.BindJSON(&req)
	if err != nil {
		log.Info("Couldn't JSON")
		c.JSON(http.StatusInternalServerError, gin.H{"message": "Cannot JSON"})
		return
	}

	if req.Key != s.config.PerishKey {
		log.Info("Wrong perish key provided", zap.String("key", req.Key))
		c.JSON(http.StatusForbidden, gin.H{"message": "wrong"})
		return
	}

	log.Info("Perish request received. Scheduling termination.")
	go func() {
		time.Sleep(1 * time.Second)
		log.Info("Perishing...")

		err := syscall.Kill(syscall.Getpid(), syscall.SIGINT)
		if err != nil {
			log.Error("Sending kill SIGINT failed")
		}
	}()

	c.JSON(http.StatusOK, HTTPResponse{})
}
