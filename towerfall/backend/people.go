package backend

import (
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"go.uber.org/zap"
)

// PeopleHandler godoc
// @Summary Returns a list of all the players registered in the app, including the leaderboard
// @Tags people
// @Accept json
// @Produce json
// @Success 200 {object} backend.PeopleResponse
// @Failure 400 {object} backend.HTTPError
// @Failure 404 {object} backend.HTTPError
// @Failure 500 {object} backend.HTTPError
// @Router /people/ [get]
func (s *Server) PeopleHandler(c *gin.Context) {
	plog := s.log.With(zap.String("path", c.Request.URL.Path))

	all := false
	if c.Request.URL.Query().Get("all") != "" {
		all = true
	}

	ps, err := s.DB.GetPeople(all)
	if err != nil {
		plog.Error("couldn't get people", zap.Error(err))
		c.JSON(http.StatusInternalServerError, gin.H{"message": "no people"})
		return
	}

	lb, err := s.DB.GetLeaderboard()
	if err != nil {
		plog.Error("couldn't get leaderboard", zap.Error(err))
		c.JSON(http.StatusInternalServerError, gin.H{"message": "no leaderboard"})
		return
	}

	c.JSON(http.StatusOK, PeopleResponse{
		People:      ps,
		Leaderboard: lb,
	})
}

// PersonHandler godoc
// @Summary Returns a single player, including their leaderboard data
// @Tags people
// @Accept json
// @Produce json
// @Success 200 {object} backend.PersonResponse
// @Failure 400 {object} backend.HTTPError
// @Failure 404 {object} backend.HTTPError
// @Failure 500 {object} backend.HTTPError
// @Router /people/{person} [get]
func (s *Server) PersonHandler(c *gin.Context) {
	plog := s.log.With(zap.String("path", c.Request.URL.Path))

	id, found := c.Params.Get("person")
	if !found {
		plog.Info("Couldn't get person ID")
		c.JSON(http.StatusBadRequest, gin.H{"message": "Couldn't get person ID"})
		return
	}
	pslog := plog.With(zap.String("player_id", id))

	pid, err := strconv.Atoi(id)
	if err != nil {
		pslog.Info("Couldn't convert person ID")
		c.JSON(http.StatusBadRequest, gin.H{"message": "Couldn't convert person ID"})
		return
	}

	ps, err := s.DB.GetPerson(uint(pid))
	if err != nil {
		plog.Error("couldn't get people", zap.Error(err))
		c.JSON(http.StatusInternalServerError, gin.H{"message": "no people"})
		return
	}

	lb, err := s.DB.GetLeaderboardPlayer(pid)
	if err != nil {
		plog.Error("couldn't get leaderboard", zap.Error(err))
		c.JSON(http.StatusInternalServerError, gin.H{"message": "no leaderboard"})
		return
	}

	c.JSON(http.StatusOK, PersonResponse{
		Person:      ps,
		Leaderboard: lb,
	})
}
