package backend

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/one-eye/drunkenfall/towerfall/messages"
	"go.uber.org/zap"
)

// UsurpTournamentHandler godoc
// @Summary Adds random players to a tournament
// @Description Default is to add 8.
// @Tags testing
// @Accept json
// @Produce json
// @Success 200 {object} backend.HTTPResponse
// @Failure 400 {object} backend.HTTPError
// @Failure 404 {object} backend.HTTPError
// @Failure 500 {object} backend.HTTPError
// @Router /tournaments/{id}/usurp/ [get]
func (s *Server) UsurpTournamentHandler(c *gin.Context) {
	tm, err := s.getTournament(c)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	tlog := s.log.With(
		zap.String("path", c.Request.URL.Path),
		zap.String("tournament", tm.Slug),
	)

	err = s.DB.UsurpTournament(tm, 8)
	if err != nil {
		tlog.Info("Could not usurp tournament", zap.Error(err))
		c.JSON(http.StatusInternalServerError, gin.H{"message": err.Error()})
		return
	}

	ps, err := s.DB.GetPlayerSummaries(tm)
	if err != nil {
		tlog.Info("Could not usurp tournament", zap.Error(err))
		c.JSON(http.StatusInternalServerError, gin.H{"message": err.Error()})
		return
	}

	err = s.wb.PlayerSummariesUpdate(tm, ps)
	if err != nil {
		tlog.Error("Sending websocket update failed")
		c.JSON(http.StatusInternalServerError, gin.H{"message": err.Error()})
	}

	tlog.Info("Tournament usurped")
	c.JSON(http.StatusOK, gin.H{})
}

// AutoplayMatchHandler godoc
// @Summary Replays a real match on a testing match, completing it in full
// @Tags testing
// @Accept json
// @Produce json
// @Success 200 {object} backend.HTTPResponse
// @Failure 400 {object} backend.HTTPError
// @Failure 404 {object} backend.HTTPError
// @Failure 500 {object} backend.HTTPError
// @Router /tournaments/{id}/autoplay/ [get]
func (s *Server) AutoplayMatchHandler(c *gin.Context) {
	tm, err := s.getTournament(c)

	if err != nil {
		s.log.Error("Could not get tournament for autoplay")
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	tlog := s.log.With(
		zap.String("path", c.Request.URL.Path),
		zap.String("tournament", tm.Slug),
	)

	m, err := s.DB.NextMatch(tm)
	if err != nil {
		tlog.Info("Could not get next match")
		c.JSON(http.StatusInternalServerError, gin.H{})
		return
	}

	mlog := tlog.With(zap.Int("match", m.Index))

	err = s.DB.AutoplayMatch(m)
	if err != nil {
		mlog.Error("Could not autoplay", zap.Error(err))
		c.JSON(http.StatusInternalServerError, gin.H{})
		return
	}

	mlog.Info("Autoplay successful")
	c.JSON(http.StatusOK, HTTPResponse{})
}

// ClearTournamentHandler godoc
// @Summary Removes all test tournaments
// @Description Any tournament that does not start with "DrunkenFall" is removed.
// @Description Returns a list of the remaining tournaments.
// @Tags testing
// @Accept json
// @Produce json
// @Success 200 {object} backend.TournamentListResponse
// @Failure 400 {object} backend.HTTPError
// @Failure 404 {object} backend.HTTPError
// @Failure 500 {object} backend.HTTPError
// @Router /tournaments/ [delete]
func (s *Server) ClearTournamentHandler(c *gin.Context) {
	plog := s.log.With(zap.String("path", c.Request.URL.Path))

	err := s.DB.ClearTestTournaments()
	if err != nil {
		plog.Info("Couldn't clear test tournaments")
		c.JSON(http.StatusBadRequest, HTTPError{
			Message: "Couldn't clear test tournaments",
			Error:   err.Error(),
		})
		return
	}

	plog.Info("Test tournaments cleared")
	s.TournamentListHandler(c)
}

// AddMessageToQueue godoc
// @Summary Adds a message into the current match
// @Description This is a tool to simulate input from the game.
// @Description It literally just accepts the JSON body of the POST
// @Tags testing
// @Accept json
// @Produce json
// @Success 200 {object} backend.HTTPResponse
// @Failure 400 {object} backend.HTTPError
// @Failure 404 {object} backend.HTTPError
// @Failure 500 {object} backend.HTTPError
// @Router /tournaments/{id}/message [post]
func (s *Server) AddMessageToQueue(c *gin.Context) {
	plog := s.log.With(zap.String("path", c.Request.URL.Path))
	var req messages.IncomingGameMessage

	err := c.BindJSON(&req)
	if err != nil {
		plog.Info("Couldn't JSON")
		c.JSON(http.StatusInternalServerError, gin.H{"message": "Cannot JSON"})
		return
	}

	err = s.DB.QueueMessage(&req)
	if err != nil {
		plog.Info("Couldn't add the message to the queue")
		c.JSON(http.StatusInternalServerError, gin.H{"message": "Cannot add to the queue"})
		return
	}

	plog.Info("Message added to queue", zap.Any("message", req))
	c.JSON(http.StatusOK, HTTPResponse{})
}

// ReplayRound godoc
// @Summary Replays a single round in real time
// @Description This is a tool to simulate input from the game, real time style.
// @Tags testing
// @Accept json
// @Produce json
// @Success 200 {object} backend.HTTPResponse
// @Failure 400 {object} backend.HTTPError
// @Failure 404 {object} backend.HTTPError
// @Failure 500 {object} backend.HTTPError
// @Router /tournaments/{id}/round_replay [get]
func (s *Server) ReplayRound(c *gin.Context) {
	log := s.log.With(zap.String("path", c.Request.URL.Path))

	go func() {
		err := s.DB.ReplayRound()
		if err != nil {
			log.Error(":ohno: it fuck up", zap.Error(err))
		}
	}()

	log.Info("Replay round started")
	c.JSON(http.StatusOK, HTTPResponse{})
}
