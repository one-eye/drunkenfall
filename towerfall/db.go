package towerfall

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"time"

	"github.com/go-pg/pg"
	"github.com/go-pg/pg/orm"
	"github.com/pkg/errors"
	"gitlab.com/one-eye/drunkenfall/towerfall/messages"
	"gitlab.com/one-eye/drunkenfall/towerfall/models"
	"go.uber.org/zap"
)

var DB *Database

var ErrNoTourmamentRunning = errors.New("no tournament is running")

const listenKey = "drunkenfall"

type Database struct {
	db       *pg.DB
	log      *zap.Logger
	wb       *messages.WebsocketBroker
	messages chan *messages.IncomingGameMessage
	listener *pg.Listener
}

type LogHook struct {
	log *zap.Logger
}

func (l LogHook) BeforeQuery(event *pg.QueryEvent) {}

func (l LogHook) AfterQuery(event *pg.QueryEvent) {
	query, err := event.FormattedQuery()
	if err != nil {
		panic(err)
	}

	if len(query) > 4000 {
		query = query[0:4000] + "</truncated>"
	}

	if os.Getenv("GIN_MODE") != "release" {
		// During tests, print it outside of zap and with a semicolon at the end for
		// quick and easy copypasta. Also, color it yellow since it's the most
		// neutral (not red/green) color that can be seen easily on terminal
		// backgrounds.

		fmt.Println(fmt.Sprintf("\033[1;33m%s;\033[0m", query))
	} else {
		// Otherwise, use zap
		l.log.Debug("Postgres query", zap.String("query", query))
	}
}

// NewDatabase sets up the database reader and writer
func NewDatabase(c *Config, wb *messages.WebsocketBroker) (*Database, error) {
	pgdb := pg.Connect(&pg.Options{
		Addr:     c.Database.Host,
		Database: c.Database.Name,
		User:     c.Database.User,
		Password: c.Database.Password,
	})

	db := &Database{
		db:       pgdb,
		log:      c.Log,
		wb:       wb,
		messages: make(chan *messages.IncomingGameMessage),
		listener: pgdb.Listen(listenKey),
	}

	if c.Database.Verbose {
		db.log.Info("DRUNKENFALL_DBVERBOSE is set; enabling logger")
		pgdb.AddQueryHook(LogHook{
			log: db.log,
		})
	}

	DB = db

	go db.consumeMessages()
	go db.listen()

	return db, nil
}

// SaveTournament stores the current state of the tournaments into the db
func (d *Database) SaveTournament(t *models.Tournament) error {
	return d.db.Update(t)
}

// SaveTournament stores the current state of the tournaments into the db
func (d *Database) NewTournament(t *models.Tournament) error {
	err := d.db.Insert(t)
	if err != nil {
		return err
	}
	t.Log = d.log.With(zap.Uint("tid", t.ID))
	return err
}

// AddPlayer adds a player object to the tournament
func (d *Database) AddPlayer(t *models.Tournament, ps *models.PlayerSummary) error {
	ps.TournamentID = t.ID
	return d.db.Insert(ps)
}

// RemovePlayer removes a player from a tourmament
func (d *Database) RemovePlayer(ps *models.PlayerSummary) error {
	return d.db.Delete(ps)
}

// TogglePlayer toggles a player in a tournament
func (d *Database) TogglePlayer(t *models.Tournament, id uint) error {
	p, err := d.GetPerson(id)
	if err != nil {
		return err
	}

	// If the player is already in the tournament, it is time to remove them
	present, err := d.IsInTournament(t, p)
	if err != nil {
		return err
	}

	if present {
		ps, err := DB.GetPlayerSummary(t, id)
		if err != nil {
			return err
		}
		return d.RemovePlayer(ps)
	}

	ps := models.NewPlayerSummary(p)
	return d.AddPlayer(t, ps)
}

// IsInTournament returns if the player is in the tournament or not
func (d *Database) IsInTournament(t *models.Tournament, p *models.Person) (bool, error) {
	q := d.db.Model(&models.PlayerSummary{}).Where("tournament_id = ?", t.ID)
	count, err := q.Where("person_id = ?", p.ID).Count()
	if err != nil {
		return false, err
	}

	return count == 1, nil
}

// SaveMatch saves a match
func (d *Database) SaveMatch(m *models.Match) error {
	err := d.db.Update(m)
	if err != nil {
		return errors.WithStack(err)
	}
	return nil
}

// ResetMatch resets a match
func (d *Database) ResetMatch(m *models.Match) error {
	_, err := d.db.Exec(`SELECT reset_match(?);`, m.ID)
	if err != nil {
		return errors.WithStack(err)
	}
	return nil
}

// StoreMessage stores a message for a match
func (d *Database) StoreMessage(m *models.Match, msg *messages.IncomingGameMessage) error {
	msg.MatchID = m.ID

	out, err := json.Marshal(msg.Data)
	if err != nil {
		log.Printf("Error when marshalling JSON: %+v", err)
		return errors.WithStack(err)
	}

	msg.JSON = string(out)

	return d.db.RunInTransaction(func(tx *pg.Tx) error {
		err = tx.Insert(msg)
		if err != nil {
			d.log.Error("Error when saving message in transaction", zap.Error(err))
			return err
		}
		return nil
	})
}

// QueueMessage
func (d *Database) QueueMessage(msg *messages.IncomingGameMessage) error {
	d.messages <- msg
	return nil
}

// consumeMessages reads messages from the incoming queue
//
// This had to be added since because if we don't do it the cosumption of
// messages would cause deadlocks in the databaz. Sad, but here we are.
func (d *Database) consumeMessages() {
	d.log.Info("Starting loop listening for game messages")

	for {
		msg := <-d.messages
		d.log.Debug("Incoming message on the queue", zap.Any("msg", msg))

		m, err := d.runningMatch()
		if err != nil {
			d.log.Info(
				"Couldn't get currently running match, ignoring message",
				zap.Any("msg", msg),
				zap.Error(err),
			)
			continue
		}

		err = d.handleMessage(m, *msg)
		if err != nil {
			d.log.Info(
				"Couldn't store message",
				zap.Any("msg", msg),
				zap.Error(err),
			)
		}
	}
}

// listen gets messages from the async NOTIFY interface from the databaz
func (d *Database) listen() {
	d.log.Info("Starting loop listening for notifications", zap.String("key", listenKey))
	ch := d.listener.Channel()

	for {
		msg := <-ch
		if msg == nil {
			d.log.Info("nil message received - exiting")
			return
		}

		d.log.Info("Incoming notification", zap.String("msg", msg.Payload))

		switch msg.Payload {
		case "match_end":
			m, err := d.lastMatch()
			if err != nil {
				d.log.Info(
					"Couldn't get the latest match, ignoring notification",
					zap.String("msg", msg.Payload),
					zap.Error(err),
				)
				continue
			}
			err = d.onMatchEnd(m)
			if err != nil {
				d.log.Info("Couldn't handle match_end", zap.Error(err))
			}

		case "player_states":
			m, err := d.runningMatch()
			if err != nil {
				d.log.Info(
					"Couldn't get currently running match, ignoring notification",
					zap.String("msg", msg.Payload),
					zap.Error(err),
				)
				continue
			}

			err = d.onStates(m)
			if err != nil {
				d.log.Info("Couldn't handle player_states", zap.Error(err))
			}
		}
	}
}

// handleMessage decides what to do with an incoming message
func (d *Database) handleMessage(m *models.Match, msg messages.IncomingGameMessage) error {
	if msg.Timestamp.IsZero() {
		msg.Timestamp = time.Now()
	}

	// Store the message. Do this before figuring out the type and even
	// if it would not be parsed.
	m.Log.Info("Handling message", zap.Any("msg", msg))
	err := d.StoreMessage(m, &msg)
	if err != nil {
		m.Log.Error("Storing message failed", zap.Error(err))
		return nil
	}

	switch msg.Type {
	case messages.InMatchEnd:
		m.Ended = time.Now()
		err := d.SaveMatch(m)
		if err != nil {
			m.Log.Error("Saving match failed", zap.Error(err))
			return err
		}

		return nil
		// return d.onMatchEnd(m)

	case messages.InRoundEnd:
		if m.Kind == models.QUALIFYING {
			m.Log.Info("Scheduling automatic start_play")

			go func() {
				time.Sleep(time.Second * 3)
				m.Log.Info("Sending automatic start_play for qualifying match")
				err := globalPublisher.Publish(messages.GameStartPlay, messages.StartPlayMessage{})
				if err != nil {
					m.Log.Error("Automatic start_play failed", zap.Error(err))
				}
			}()
		}
	}

	return nil
	// return d.onStates(m)
}

func (d *Database) matchEndPublish(t *models.Tournament, m *models.Match) error {
	if globalPublisher == nil {
		return nil
	}

	// Publish the next match and then send updates to the interface
	next, err := d.NextMatch(t)
	if err != nil {
		m.Log.Error("Getting next match failed", zap.Error(err))
		return err
	}

	// If it is a special match, allow less than four players
	if len(next.Players) != 4 && next.Kind != models.SPECIAL {
		return models.ErrPublishIncompleteMatch
	}

	msg := messages.GameMatchMessage{
		Tournament: t.Slug,
		Level:      next.RealLevel(),
		Length:     next.Length,
		Ruleset:    next.Ruleset,
		Kind:       next.Kind,
	}

	for _, p := range next.Players {
		gp := messages.GamePlayer{
			TopName:    p.DisplayNames[0],
			BottomName: p.DisplayNames[1],
			Color:      p.NumericColor(),
			ArcherType: p.ArcherType,
		}
		msg.Players = append(msg.Players, gp)
	}

	t.Log.Info("Sending publish", zap.Any("match", msg))
	return globalPublisher.Publish(messages.GameMatch, msg)
}

// onMatchEnd is the update sent whenever a match has ended
func (d *Database) onMatchEnd(m *models.Match) error {
	t, err := d.GetTournament(m.TournamentID)
	if err != nil {
		m.Log.Error("Getting tournament failed", zap.Error(err))
		return err
	}

	// Publish the next match to the game
	if m.Kind != models.FINAL {
		err = d.matchEndPublish(t, m)
		if err != nil {
			m.Log.Error("Publishing next match failed", zap.Error(err))
			return err
		}
	}

	ms, err := d.GetMatches(t, "all")
	if err != nil {
		m.Log.Error("Couldn't get matches", zap.Error(err))
		return err
	}

	runnerups, err := d.GetAllRunnerups(t)
	if err != nil {
		m.Log.Error("Couldn't get runnerups", zap.Error(err))
		return err
	}

	summaries, err := d.GetPlayerSummaries(t)
	if err != nil {
		m.Log.Error("Couldn't get summaries", zap.Error(err))
		return err
	}

	// Send the match end update
	err = d.wb.MatchEndUpdate(t, ms, runnerups, summaries)
	if err != nil {
		m.Log.Error("Couldn't send match_end update", zap.Error(err))
		return err
	}

	if m.Kind == models.FINAL {
		err = globalPublisher.SendTournamentComplete(t)
		if err != nil {
			m.Log.Error("Couldn't send tournament_complete", zap.Error(err))
			return err
		}
	}

	return nil
}

// onStates is the update sent on a normal message; an update about the current
// player states
func (d *Database) onStates(m *models.Match) error {
	sts, err := d.GetPlayerStates(m)
	if err != nil {
		d.log.Error("Getting player states failed", zap.Error(err))
		return errors.WithStack(err)
	}

	return d.wb.MatchUpdate(m, sts)
}

// runningMatch gets the currently running match
func (d *Database) runningMatch() (*models.Match, error) {
	t, err := d.GetCurrentTournament()
	if err != nil {
		return nil, err
	}

	m, err := d.CurrentMatch(t)
	if err != nil {
		return nil, err
	}

	return m, nil
}

// SavePerson stores a person into the DB
func (d *Database) SavePerson(p *models.Person) error {
	return d.db.Update(p)
}

// AddPerson adds a person into the DB
func (d *Database) AddPerson(p *models.Person) error {
	return d.db.Insert(p)
}

// GetPerson gets a Person{} from the DB
func (d *Database) GetPerson(id uint) (*models.Person, error) {
	p := models.Person{}
	err := d.db.Model(&p).Column("person.*").Relation("HighestPosition").Where("id = ?", id).Select()
	return &p, err
}

// GetPersonFromFacebook gets a Person{} from a Facebook auth request
func (d *Database) GetPersonFromFacebook(oauth string) (*models.Person, error) {
	p := models.Person{}

	log.Printf("%+v", oauth)
	err := d.db.Model(&p).Where("person_id = ?", oauth).Select(&p)
	return &p, err
}

// DisablePerson disables or re-enables a person
func (d *Database) DisablePerson(id uint) error {
	query := `UPDATE people SET disabled = NOT disabled WHERE id = ?;`
	_, err := d.db.Exec(query, id)
	return err
}

// GetPeople loads the people from the database
func (d *Database) GetPeople(all bool) ([]*models.Person, error) {
	ret := make([]*models.Person, 0)
	q := d.db.Model(&ret).Column("person.*").Relation("HighestPosition")

	if !all {
		q = q.Where("NOT disabled")
	}

	err := q.Select()
	return ret, err
}

// GetLeaderboard gets the leaderboard
func (d *Database) GetLeaderboard() ([]*models.LeaderboardPlayer, error) {
	ret := make([]*models.LeaderboardPlayer, 0)

	_, err := d.db.Query(&ret, "SELECT * FROM leaderboard()")
	return ret, err
}

// GetTournamentLeaderboard gets the leaderboard
func (d *Database) GetTournamentLeaderboard(tm *models.Tournament) ([]*models.TournamentLeaderboardPlayer, error) {
	ret := make([]*models.TournamentLeaderboardPlayer, 0)

	_, err := d.db.Query(&ret, "SELECT nick, shots, sweeps, kills, self, matches, total_score, skill_score FROM overview(?);", tm.ID)
	return ret, err
}

// GetLeaderboardPlayer gets a single person from the leaderboard
func (d *Database) GetLeaderboardPlayer(id int) (*models.LeaderboardPlayer, error) {
	ret := models.LeaderboardPlayer{}

	_, err := d.db.Query(&ret, "SELECT * FROM person_leaderboard(?)", id)
	return &ret, err
}

// GetTournament gets a tournament by id, or returns the cached one if there is one
func (d *Database) GetTournament(id uint) (*models.Tournament, error) {
	t := models.Tournament{
		Log: d.log.With(zap.Uint("tid", id)),
	}

	err := d.db.Model(&t).Where("id = ?", id).First()
	if err != nil {
		return nil, err
	}

	return &t, nil
}

// GetOverview gets the overview
func (d *Database) GetOverview(t *models.Tournament) ([]*models.Summary, error) {
	ret := make([]*models.Summary, 0)

	_, err := d.db.Query(&ret, "SELECT * FROM overview(?)", t.ID)
	return ret, err
}

// GetScores gets the scores
func (d *Database) GetScores(t *models.Tournament) ([]*models.Score, error) {
	ret := make([]*models.Score, 0)

	_, err := d.db.Query(&ret, "SELECT * FROM scores(?)", t.ID)
	return ret, err
}

func (d *Database) GetTournaments() ([]*models.Tournament, error) {
	ret := make([]*models.Tournament, 0)
	q := d.db.Model(&ret)
	err := q.Order("opened DESC").Select()
	return ret, err
}

// GetCurrentTournament gets the currently running tournament
func (d *Database) GetCurrentTournament() (*models.Tournament, error) {
	t := models.Tournament{}

	err := d.db.Model(&t).Order("id DESC").First()
	if err != nil {
		return nil, err
	}
	t.Log = d.log.With(zap.Uint("tid", t.ID))
	return &t, nil
}

// GetLeague gets a single League
func (d *Database) GetLeague(id uint) (*models.League, error) {
	l := models.League{}

	err := d.db.Model(&l).Column("league.*").Relation("Tournaments", func(q *orm.Query) (*orm.Query, error) {
		return q.Where("league_id = ?", id).Order("id DESC"), nil
	}).Where("id = ?", id).First()
	if err != nil {
		return nil, err
	}

	return &l, nil
}

// GetLeagues gets all the leagues
func (d *Database) GetLeagues() ([]*models.League, error) {
	ret := make([]*models.League, 0)

	err := d.db.Model(&ret).Column("league.*").Relation("Tournaments").Order("id DESC").Select()

	if err != nil {
		return nil, err
	}

	return ret, nil
}

// GetMatch gets a match
func (d *Database) GetMatch(id uint) (*models.Match, error) {
	m := models.Match{}
	q := d.db.Model(&m).Where("match.id = ?", id)
	err := q.Select()
	if err != nil {
		return nil, err
	}

	ps := []*models.Player{}
	q = d.db.Model(&ps).Where("match_id = ?", id).Order("index")
	err = q.Select()
	m.Players = ps

	m.Log = d.log.With(
		zap.Uint("tid", m.TournamentID),
		zap.Uint("mid", m.ID),
	)

	return &m, err
}

// GetMatches gets a slice of matches based on a kind
func (d *Database) GetMatches(t *models.Tournament, kind string) ([]*models.Match, error) {
	ret := []*models.Match{}

	q := d.db.Model(&ret)

	if kind != "all" {
		q = q.Where("kind = ?", kind)
	}

	q = q.Where("tournament_id = ?", t.ID).Order("id ASC")
	err := q.Select(&ret)
	if err != nil {
		return ret, err
	}

	// XXX(thiderman): This is terrible, but without it the ordering of the players is wrong again
	for x, m := range ret {
		ps := []*models.Player{}
		q = d.db.Model(&ps).Where("match_id = ?", m.ID).Order("index")
		err = q.Select()
		if err != nil {
			return ret, err
		}
		ret[x].Players = ps

		m.Log = d.log.With(
			zap.Uint("tid", m.TournamentID),
			zap.Uint("mid", m.ID),
		)
	}

	return ret, err
}

// NextMatch the next playable match of a tournament
func (d *Database) NextMatch(t *models.Tournament) (*models.Match, error) {
	m := models.Match{}

	q := d.db.Model(&m).Where("tournament_id = ? AND started IS NULL", t.ID)
	q = q.Order("id").Limit(1)

	err := q.Select()
	if err != nil {
		return nil, err
	}

	ps := []*models.Player{}
	q = d.db.Model(&ps).Where("match_id = ?", m.ID).Order("index")
	err = q.Select()
	m.Players = ps

	m.Log = d.log.With(
		zap.Uint("tid", m.TournamentID),
		zap.Uint("mid", m.ID),
	)

	return &m, err
}

// CurrentMatch returns the currently running match
func (d *Database) CurrentMatch(t *models.Tournament) (*models.Match, error) {
	m := models.Match{}

	q := d.db.Model(&m).Where("tournament_id = ? AND ended IS NULL", t.ID)
	q = q.Order("id").Limit(1)

	err := q.Select()
	if err != nil {
		return nil, err
	}

	ps := []*models.Player{}
	q = d.db.Model(&ps).Where("match_id = ?", m.ID).Order("index ASC")
	err = q.Select()
	m.Players = ps

	m.Log = d.log.With(
		zap.Uint("tid", m.TournamentID),
		zap.Uint("mid", m.ID),
	)

	return &m, err
}

// lastMatch returns the match that most recently finished
//
// This is important when getting match_end updates from the database
func (d *Database) lastMatch() (*models.Match, error) {
	m := models.Match{}
	t, err := d.GetCurrentTournament()
	if err != nil {
		return nil, err
	}

	q := d.db.Model(&m).Where("tournament_id = ? AND ended IS NOT NULL", t.ID)
	q = q.Order("id DESC").Limit(1)

	err = q.Select()
	if err != nil {
		return nil, err
	}

	ps := []*models.Player{}
	q = d.db.Model(&ps).Where("match_id = ?", m.ID).Order("index ASC")
	err = q.Select()
	m.Players = ps

	m.Log = d.log.With(
		zap.Uint("tid", m.TournamentID),
		zap.Uint("mid", m.ID),
	)

	return &m, err
}

// GetAllRunnerups gets all the runnerups that aren't already booked
// into matches
func (d *Database) GetAllRunnerups(t *models.Tournament) ([]*models.PlayerSummary, error) {
	ps := []*models.PlayerSummary{}
	q := `SELECT * FROM runnerups(?)`
	_, err := d.db.Query(&ps, q, t.ID)
	return ps, err
}

// GetQueuedPlayers returns a list of all the players that are queued for the tournament
func (d *Database) GetQueuedPlayers(t *models.Tournament) ([]*models.Person, error) {
	ps := []*models.Person{}
	q := `SELECT p.* FROM people P INNER JOIN player_queue q ON q.person_id = p.ID ORDER BY q.ID ASC;`
	_, err := d.db.Query(&ps, q, t.ID)
	return ps, err
}

// GetPlayoffRunnerups gets the runnerups in the playoffs
func (d *Database) GetPlayoffRunnerups(t *models.Tournament) ([]*models.PlayerSummary, error) {
	ps := []*models.PlayerSummary{}
	q := `SELECT * FROM playoff_runnerups_as_summaries(?);`
	_, err := d.db.Query(&ps, q, t.ID)
	return ps, err
}

// GetPlayerSummary gets a single player summary for a tourmanent
func (d *Database) GetPlayerSummary(t *models.Tournament, pid uint) (*models.PlayerSummary, error) {
	ret := models.PlayerSummary{}
	q := d.db.Model(&ret).Where("player_summary.person_id = ?", pid).Where("tournament_id = ?", t.ID)
	err := q.Select(&ret)
	return &ret, err
}

// GetPlayerSummaries gets all player summaries for a tourmanent
func (d *Database) GetPlayerSummaries(t *models.Tournament) ([]*models.PlayerSummary, error) {
	ret := []*models.PlayerSummary{}
	q := d.db.Model(&ret)
	err := q.Where("tournament_id = ?", t.ID).Order("id ASC").Select(&ret)
	if err != nil {
		return nil, errors.WithStack(err)
	}

	return ret, err
}

// GetPlayerState gets the player state for a player
func (d *Database) GetPlayerState(m *models.Match, idx int) (*models.PlayerState, error) {
	sts, err := d.GetPlayerStates(m)
	if err != nil {
		return nil, err
	}
	return sts[idx], err
}

// GetPlayerStates gets the players for all players in a match
func (d *Database) GetPlayerStates(m *models.Match) ([]*models.PlayerState, error) {
	r := models.Round{}
	st := []*models.PlayerState{}

	// First, get the most current round
	err := d.db.Model(&r).Where("match_id = ?", m.ID).Order("id DESC").First()
	if err != nil {
		// d.log.Error("No round has started yet", zap.Error(err))
		// No round exists yet, so we pretend there are four empty ones
		st = []*models.PlayerState{
			models.NewPlayerState(),
			models.NewPlayerState(),
			models.NewPlayerState(),
			models.NewPlayerState(),
		}
		// Need to set the index as well, otherwise only P1 will get all of the updates.
		for x := range st {
			st[x].Index = x
		}
		return st, nil
	}

	q := d.db.Model(&st).Where("round_id = ?", r.ID)
	// Always get from the latest round, and pick them in index order.
	err = q.Order("round_id DESC", "index ASC").Limit(4).Select(&st)
	return st, err
}

// AutoplayMatch replays an old match on top of an unplayed one
func (d *Database) AutoplayMatch(m *models.Match) error {
	// TODO(thiderman): Use the provided match
	_, err := d.db.Exec(`SELECT replay_match();`)
	return err
}

// UsurpTournament adds testing players
func (d *Database) UsurpTournament(t *models.Tournament, x int) error {
	_, err := d.db.Exec(`SELECT usurp_tournament(?, ?)`, t.ID, x)
	if err != nil {
		log.Printf("Usurping failed: %+v", err)
	}
	return err
}

// ClearTestTournaments deletes any tournament that doesn't begin with "DrunkenFall"
func (d *Database) ClearTestTournaments() error {
	_, err := d.db.Model(&models.Tournament{}).Where("name NOT LIKE 'DrunkenFall%'").Delete()
	return err
}

// GetOrCreatePerson gets or creates a new person from a Auth user body
func (d *Database) GetOrCreatePerson(pa *models.PersonAuth) (*models.Person, error, bool) {
	// First, see if we can get an existing person_auth (i.e. the user has
	// authenticated before)
	existing := models.PersonAuth{}
	q := d.db.Model(&existing).Where(
		"person_auth.remote_id = ? AND person_auth.provider = ?",
		pa.RemoteID,
		pa.Provider,
	)

	err := q.Select()
	if err != nil {
		if err != pg.ErrNoRows {
			d.log.Info("Selecting existing person_auth failed", zap.Any("user", pa), zap.Error(err))
			return nil, err, false
		}

		person := &models.Person{
			Name:      pa.Name,
			Nick:      pa.Nick,
			AvatarURL: pa.AvatarURL,
		}

		// No user exists or the query broke - make a new user
		err = d.AddPerson(person)
		if err != nil {
			d.log.Info("Could not make a new person", zap.Any("user", pa), zap.Error(err))
			return nil, err, false
		}

		pa.PersonID = person.ID
		err = d.db.Insert(pa)
		d.log.Info("Adding a new user", zap.Any("user", pa), zap.Any("person", person))
		return person, err, true
	}

	// If there is already a user, get it
	p, err := d.GetPerson(existing.PersonID)
	return p, err, false
}

// ReplayRound replays a single round.
//
// Intended to be run inside of a goroutine - it will replay in real time.
func (d *Database) ReplayRound() error {
	max := 2 * time.Second
	msgs := []*messages.IncomingGameMessage{}

	q := `SELECT * FROM random_messages_for_replay();`
	_, err := d.db.Query(&msgs, q)
	if err != nil {
		d.log.Info("Couldn't get the random messages for replay", zap.Error(err))
		return err
	}

	m, err := d.runningMatch()
	if err != nil {
		d.log.Info("Couldn't get currently running match, aborting", zap.Error(err))
		return err
	}

	d.log.Info("Replaying round", zap.Uint("mid", m.ID), zap.Uint("rid", msgs[0].ID))

	previous := msgs[0].Timestamp
	for _, msg := range msgs {

		msg.ID = 0
		msg.MatchID = m.ID
		_ = json.Unmarshal([]byte(msg.JSON), &msg.Data)

		err = d.StoreMessage(m, msg)
		if err != nil {
			d.log.Info("Couldn't store message", zap.Error(err), zap.Any("msg", msg))
			return err
		}

		slep := msg.Timestamp.Sub(previous)
		// Sleep for a capped maximum amount
		if slep > max {
			slep = max
		}

		d.log.Info(
			"Stored message",
			zap.Duration("sleep", slep),
			zap.Any("msg", msg),
		)
		time.Sleep(slep)

		previous = msg.Timestamp
	}

	return err
}

// Close closes the database
func (d *Database) Close() error {
	d.log.Info("Closing database...")

	err := d.db.Close()
	if err != nil {
		d.log.Error("Could not close db", zap.Error(err))
		return err
	}

	// err = d.listener.Close()
	// if err != nil {
	// 	d.log.Error("Could not close listener", zap.Error(err))
	// 	return err
	// }

	d.log.Info("Database closed", zap.Error(err))
	return err
}
