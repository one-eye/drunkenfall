package messages

import "time"

// An IncomingGameMessage is a message from the game put into the DB consumer queue
type IncomingGameMessage struct {
	ID        uint        `json:"id"`
	MatchID   uint        `json:"match_id"`
	Type      string      `json:"type"`
	Data      interface{} `json:"data" sql:"-"`
	JSON      string      `json:"-"`
	Timestamp time.Time   `json:"timestamp"`
	RoundID   uint        `json:"round_id"`
	tableName struct{}    `sql:"messages"` // nolint
}

// Message types as used by IncomingGameMessage
// nolint
const (
	InKill       = "kill"
	InRoundStart = "round_start"
	InRoundEnd   = "round_end"
	InMatchStart = "match_start"
	InMatchEnd   = "match_end"
	InPickup     = "arrows_collected"
	InShot       = "arrow_shot"
	InShield     = "shield_state"
	InWings      = "wings_state"
	InOrbLava    = "lava_orb_state"
	// TODO(thiderman): Non-player orbs are not implemented
	InOrbSlow   = "slow_orb_state"
	InOrbDark   = "dark_orb_state"
	InOrbScroll = "scroll_orb_state"
)
