package messages

import (
	"encoding/json"
	"net/http"

	"github.com/olahol/melody"
	"gitlab.com/one-eye/drunkenfall/towerfall/models"
	"go.uber.org/zap"
)

const (
	wTournament      = "tournament"
	wPlayerSummaries = "player_summaries"
	// wRunnerups       = "runnerups"
	// wPlayer          = "player"
	wMatch    = "match"
	wMatches  = "matches"
	wMatchEnd = "match_end"
)

var broadcasting = true

// Message is the data to send back
type websocketMessage struct {
	Type string      `json:"type"`
	Data interface{} `json:"data"`
}

type wsTournament struct {
	TournamentID uint               `json:"tournament_id"`
	Tournament   *models.Tournament `json:"tournament"`
}

type wsPlayerSummaries struct {
	TournamentID    uint                    `json:"tournament_id"`
	PlayerSummaries []*models.PlayerSummary `json:"player_summaries"`
}

// type wsRunnerups struct {
// 	TournamentID uint                    `json:"tournament_id"`
// 	Runnerups    []*models.PlayerSummary `json:"runnerups"`
// }

type wsMatches struct {
	TournamentID uint            `json:"tournament_id"`
	Matches      []*models.Match `json:"matches"`
}

type wsMatchEnd struct {
	TournamentID    uint                    `json:"tournament_id"`
	Tournament      *models.Tournament      `json:"tournament"`
	Runnerups       []*models.PlayerSummary `json:"runnerups"`
	PlayerSummaries []*models.PlayerSummary `json:"player_summaries"`
	PlayerStates    []*models.PlayerState   `json:"player_states"`
	Matches         []*models.Match         `json:"matches"`
}

type WebsocketBroker struct {
	log    *zap.Logger
	melody *melody.Melody
}

func NewWebsocketBroker(log *zap.Logger) *WebsocketBroker {
	w := WebsocketBroker{
		log:    log,
		melody: melody.New(),
	}

	w.melody.HandleMessage(func(ms *melody.Session, msg []byte) {
		w.log.Info("Websocket message", zap.String("message", string(msg)))
		err := w.melody.Broadcast(msg)
		if err != nil {
			w.log.Error("Handling websocket message failed", zap.Error(err))
		}
	})

	return &w
}

// DisableWebsocketUpdates... disables websocket updates.
func (w *WebsocketBroker) DisableWebsocketUpdates() {
	// log.Printf("%+v", s)
	// w.log.Info("Disabling websocket broadcast")
	broadcasting = false
}

// EnableWebsocketUpdates... enables websocket updates.
func (w *WebsocketBroker) EnableWebsocketUpdates() {
	// w.log.Info("Enabling websocket broadcast")
	broadcasting = true
}

// HandleRequest passes the handling on to Melody
func (w *WebsocketBroker) HandleRequest(rw http.ResponseWriter, r *http.Request) error {
	return w.melody.HandleRequest(rw, r)
}

// WebsocketUpdate sends an update to all listening sockets
func (w *WebsocketBroker) WebsocketUpdate(kind string, data interface{}) error {
	if !broadcasting {
		w.log.Info("Not broadcasting websocket updates")
		return nil
	}

	go func(kind string, data interface{}) {
		msg := websocketMessage{
			Type: kind,
			Data: data,
		}

		out, err := json.Marshal(msg)
		if err != nil {
			w.log.Warn("cannot marshal", zap.Error(err))
			return
		}

		err = w.melody.Broadcast(out)
		if err != nil {
			w.log.Error("Broadcast failed", zap.Error(err))
			return
		}

		w.log.Info(
			"Websocket update",
			zap.String("type", kind),
			// zap.Any("data", data),
		)
	}(kind, data)

	return nil
}

// TournamentUpdate sends an update about the tournament
func (w *WebsocketBroker) TournamentUpdate(t *models.Tournament) error {
	return w.WebsocketUpdate(wTournament, wsTournament{
		TournamentID: t.ID,
		Tournament:   t,
	})
}

// PlayerSummariesUpdate sends an update to the player summaries
func (w *WebsocketBroker) PlayerSummariesUpdate(t *models.Tournament, summaries []*models.PlayerSummary) error {
	return w.WebsocketUpdate(wPlayerSummaries, wsPlayerSummaries{
		TournamentID:    t.ID,
		PlayerSummaries: summaries,
	})
}

// // RunnerupsUpdate sends an update to the runnerups
// func (w *WebsocketBroker) RunnerupsUpdate(t *models.Tournament) error {
// 	runnerups, err := w.DB.GetAllRunnerups(t)
// 	if err != nil {
// 		return err
// 	}

// 	return w.WebsocketUpdate(wRunnerups, wsRunnerups{
// 		TournamentID: t.ID,
// 		Runnerups:    runnerups,
// 	})
// }

// // PlayerUpdate sends a status update for a single player
// func (w *WebsocketBroker) PlayerUpdate(m *models.Match, states *models.PlayerState) error {
// 	return w.WebsocketUpdate(
// 		wPlayer,
// 		PlayerStateUpdateMessage{m.TournamentID, m.Index, states},
// 	)
// }

// MatchUpdate sends a status update for the entire match
func (w *WebsocketBroker) MatchUpdate(m *models.Match, sts []*models.PlayerState) error {
	return w.WebsocketUpdate(
		wMatch,
		MatchUpdateMessage{
			m.TournamentID,
			m,
			sts,
		},
	)
}

// MatchesUpdate sends an update about the matches
func (w *WebsocketBroker) MatchesUpdate(t *models.Tournament, ms []*models.Match) error {
	return w.WebsocketUpdate(wMatches, wsMatches{
		TournamentID: t.ID,
		Matches:      ms,
	})
}

// MatchEndUpdate sends the updates needed when a match is ended
func (w *WebsocketBroker) MatchEndUpdate(t *models.Tournament, ms []*models.Match, runnerups []*models.PlayerSummary, summaries []*models.PlayerSummary) error {
	// TODO(thiderman): This is also used as a "send a full tournament status
	// update" when starting a new tournament. Perhaps the function should be
	// renamed?
	w.log.Info("Sending match end update", zap.Uint("tid", t.ID))

	states := []*models.PlayerState{
		models.NewPlayerState(),
		models.NewPlayerState(),
		models.NewPlayerState(),
		models.NewPlayerState(),
	}
	// Need to set the index as well, otherwise only P1 will get all of the updates.
	for x := range states {
		states[x].Index = x
	}

	return w.WebsocketUpdate(wMatchEnd, wsMatchEnd{
		TournamentID:    t.ID,
		Tournament:      t,
		Matches:         ms,
		Runnerups:       runnerups,
		PlayerSummaries: summaries,
		PlayerStates:    states,
	})
}
