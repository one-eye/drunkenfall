package messages

import "gitlab.com/one-eye/drunkenfall/towerfall/models"

// Constant strings for use as kinds when communicating with the game
const (
	GameMatch      = "match"
	GameConnect    = "game_connected"
	GameDisconnect = "game_disconnected"
	GameStartPlay  = "start_play"
	GameComplete   = "tournament_complete"
)

type PlayerStateUpdateMessage struct {
	Tournament uint                `json:"tournament"`
	Match      int                 `json:"match"`
	State      *models.PlayerState `json:"state"`
}

type MatchUpdateMessage struct {
	Tournament uint                  `json:"tournament"`
	Match      *models.Match         `json:"match"`
	Players    []*models.PlayerState `json:"player_states"`
}

// StartPlayMessage is sent to the game whenever one of the shot girls
// have indicated that the next round can start
type StartPlayMessage struct{}

// TournamentCompleteMessage is sent when the tournament is done
type TournamentCompleteMessage struct{}

// StartMatchMessage is sent when a match is starting
type StartMatchMessage struct{}

// GameMatchMessage is the message sent to the game about the
// configuration of the next match
type GameMatchMessage struct {
	Players    []GamePlayer `json:"players"`
	Tournament string       `json:"tournament"`
	Level      string       `json:"level"`
	Length     int          `json:"length"`
	Ruleset    string       `json:"ruleset"`
	Kind       string       `json:"kind"`
}

// GamePlayer is a player object to be consumed by the game
type GamePlayer struct {
	TopName    string `json:"top_name"`
	BottomName string `json:"bottom_name"`
	Color      int    `json:"color"`
	ArcherType int    `json:"archer_type"`
}
