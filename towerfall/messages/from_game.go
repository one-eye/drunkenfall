package messages

import "gitlab.com/one-eye/drunkenfall/towerfall/models"

type KillMessage struct {
	Player int `json:"player"`
	Killer int `json:"killer"`
	Cause  int `json:"cause"`
}

type ArrowMessage struct {
	Player int           `json:"player"`
	Arrows models.Arrows `json:"arrows"`
}

type ShieldMessage struct {
	Player int  `json:"player"`
	State  bool `json:"state"`
}

type WingsMessage struct {
	Player int  `json:"player"`
	State  bool `json:"state"`
}

type SlowOrbMessage struct {
	State bool `json:"state"`
}

type DarkOrbMessage struct {
	State bool `json:"state"`
}

type ScrollOrbMessage struct {
	State bool `json:"state"`
}

type LavaOrbMessage struct {
	Player int  `json:"player"`
	State  bool `json:"state"`
}

type StartRoundMessage struct {
	Arrows []models.Arrows `json:"arrows"`
}
