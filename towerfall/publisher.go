package towerfall

import (
	"encoding/json"
	"time"

	"github.com/pkg/errors"
	"github.com/streadway/amqp"
	"gitlab.com/one-eye/drunkenfall/towerfall/messages"
	"gitlab.com/one-eye/drunkenfall/towerfall/models"
)

var globalPublisher *Publisher
var ErrPublisherDisabled = errors.New("publisher is disabled")

type Publisher struct {
	conn     *amqp.Connection
	outgoing amqp.Queue
	ch       *amqp.Channel
}

// NewPublisher sets up a new listener
func NewPublisher(conf *Config) (*Publisher, error) {
	var err error
	p := Publisher{}

	p.conn, err = amqp.Dial(conf.Rabbit.URL)
	if err != nil {
		return nil, err
	}

	p.ch, err = p.conn.Channel()
	if err != nil {
		return &p, errors.New("Failed to open publishing channel")
	}

	p.outgoing, err = p.ch.QueueDeclare(
		conf.Rabbit.Outgoing, // name
		false,                // durable
		false,                // delete when unused
		false,                // exclusive
		false,                // no-wait
		nil,                  // arguments
	)
	if err != nil {
		return &p, errors.New("Failed to declare the outgoing queue")
	}

	globalPublisher = &p

	return &p, err
}

// Publish sends a message on the default exchange to the currently
// configured queue
func (p *Publisher) Publish(kind string, data interface{}) error {
	if p == nil {
		return ErrPublisherDisabled
	}

	msg := messages.IncomingGameMessage{
		Type:      kind,
		Data:      data,
		Timestamp: time.Now(),
	}

	body, err := json.Marshal(msg)
	if err != nil {
		return err
	}

	err = p.ch.Publish(
		"",              // exchange (default)
		p.outgoing.Name, // routing key
		false,           // mandatory
		false,           // immediate
		amqp.Publishing{
			ContentType: "application/json",
			Body:        []byte(body),
		})
	return err
}

// SendTournamentComplete sends the tournament ended message
func (p *Publisher) SendTournamentComplete(t *models.Tournament) error {
	return p.Publish(messages.GameComplete, messages.TournamentCompleteMessage{})
}
