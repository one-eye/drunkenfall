package models

type LeaderboardPlayer struct {
	PersonID    int      `json:"person_id"`
	Shots       int      `json:"shots" sql:",notnull"`
	Sweeps      int      `json:"sweeps" sql:",notnull"`
	Kills       int      `json:"kills" sql:",notnull"`
	Self        int      `json:"self" sql:",notnull"`
	Matches     int      `json:"matches" sql:",notnull"`
	Tournaments int      `json:"tournaments" sql:",notnull"`
	TotalScore  int      `json:"total_score" sql:",notnull"`
	SkillScore  int      `json:"skill_score" sql:",notnull"`
	tableName   struct{} `pg:",discard_unknown_columns"` // nolint
}

type TournamentLeaderboardPlayer struct {
	Nick       string `json:"nick"`
	Shots      int    `json:"shots" sql:",notnull"`
	Sweeps     int    `json:"sweeps" sql:",notnull"`
	Kills      int    `json:"kills" sql:",notnull"`
	Self       int    `json:"self" sql:",notnull"`
	Matches    int    `json:"matches" sql:",notnull"`
	TotalScore int    `json:"total_score" sql:",notnull"`
	SkillScore int    `json:"skill_score" sql:",notnull"`
}
