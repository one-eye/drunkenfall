package models

import (
	"encoding/json"
	"errors"
	"log"
	"net/http"
	"strconv"
	"time"

	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
	// dfhttp "gitlab.com/one-eye/drunkenfall/towerfall/http"
)

// A Person is someone having a role in the tournament
type Person struct {
	ID              uint      `json:"id" sql:",pk"`
	Name            string    `json:"name"`
	Nick            string    `json:"nick"`
	PreferredColor  string    `json:"preferred_color"`
	ArcherType      int       `json:"archer_type"`
	AvatarURL       string    `json:"avatar_url"`
	Userlevel       int       `json:"userlevel"`
	Disabled        bool      `json:"disabled" sql:",notnull"`
	DisplayNames    []string  `sql:",array" json:"display_names"`
	HighestPosition *Position `json:"highest_position"`
}

type Position struct {
	Position  uint     `json:"position"`
	Times     uint     `json:"times"`
	PersonID  uint     `json:"person_id"`
	tableName struct{} `sql:"highest_positions"` // nolint
}

// Credits represents the data structure needed to display the credits
type Credits struct {
	Executive     *Person   `json:"executive"`
	Producers     []*Person `json:"producers"`
	Players       []*Person `json:"players"`
	ArchersHarmed int       `json:"archers_harmed"`
}

// Userlevels. Designed so that we can insert new ones in between them.
const (
	PermissionProducer    = 100
	PermissionCommentator = 50
	PermissionJudge       = 30
	PermissionPlayer      = 10
)

// Archer types. These decide which version of the character you are
// playing as. Secret is selectable but never used.
// nolint
const (
	atNormal = iota
	atAlternate
	atSecret
)

var ErrFacebookAlreadyExists = errors.New("facebook user already exists")
var ErrPlayerDisabled = errors.New("player is disabled")

type score map[int]string

// ScoreSummary is a collection of scores for a Person
type ScoreSummary struct {
	Totals      score
	Tournaments map[string]score
}

// JSON returns the person as a JSON representation
func (p *Person) JSON() (out []byte, err error) {
	out, err = json.Marshal(p)
	return
}

// Score gets the score of the Person
//
// Returned as a map of the total score and an array of maps - one per
// tournament participated in.
func (p *Person) Score() *ScoreSummary {
	return nil
}

// StoreCookies stores the cookies of the
func (p *Person) StoreCookies(c *gin.Context) error {
	cookie := &http.Cookie{
		Name:    "userlevel",
		Value:   strconv.Itoa(p.Userlevel),
		Path:    "/",
		Expires: time.Now().Add(30 * 24 * time.Hour), // Set to the same as CookieStore
	}
	http.SetCookie(c.Writer, cookie)

	session := sessions.Default(c)
	session.Set("user", p.ID)
	session.Set("userlevel", p.Userlevel)
	err := session.Save()
	if err != nil {
		return err
	}

	return nil
}

// RemoveCookies ...
func (p *Person) RemoveCookies(c *gin.Context) error {
	cookie := &http.Cookie{
		Name:    "userlevel",
		Value:   "0",
		Path:    "/",
		Expires: time.Now(),
	}
	http.SetCookie(c.Writer, cookie)

	session := sessions.Default(c)
	session.Delete("user")
	session.Delete("userlevel")
	return session.Save()
}

// LoadPerson loads a person from the database
func LoadPerson(data []byte) (*Person, error) {
	p := &Person{}
	err := json.Unmarshal(data, p)

	if err != nil {
		log.Print(err)
		return p, err
	}

	if p.Disabled {
		return p, ErrPlayerDisabled
	}

	return p, nil
}
