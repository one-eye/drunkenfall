package models

import "time"

// League represents a league
type League struct {
	ID          uint          `json:"id"`
	Title       string        `json:"title"`
	Description string        `json:"description"`
	Started     time.Time     `json:"started"`
	Ended       time.Time     `json:"ended"`
	Tournaments []*Tournament `json:"tournaments"`
}
