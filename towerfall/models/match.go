package models

import (
	"time"

	"github.com/pkg/errors"
	"go.uber.org/zap"
)

const (
	QUALIFYING = "qualifying"
	PLAYOFF    = "playoff"
	FINAL      = "final"
	SPECIAL    = "special"
)

var ErrPublishIncompleteMatch = errors.New("cannot publish match without four players")

// Match represents a game being played
type Match struct {
	ID           uint          `json:"id"`
	TournamentID uint          `json:"tournament_id"`
	Players      []*Player     `json:"players"`
	Kind         string        `json:"kind"`
	Index        int           `json:"index" sql:",notnull"`
	Length       int           `json:"length"`
	Title        string        `json:"name"` // TODO(thiderman): Should be "title"
	Round        uint          `json:"round"`
	Pause        time.Duration `json:"pause"`
	Scheduled    time.Time     `json:"scheduled"`
	Started      time.Time     `json:"started"`
	Ended        time.Time     `json:"ended"`
	Level        string        `json:"level"`
	Ruleset      string        `json:"ruleset"`
	Log          *zap.Logger   `sql:"-" json:"-"`
}

// Round is a state commit for a round of a match
type Round struct {
	TableName struct{} `sql:"rounds"`
	ID        uint
	MatchID   uint
	Committed time.Time
}

// realLevel returns the level name string that the game expects
func (m *Match) RealLevel() string {
	return realNames[m.Level]
}

// SetTime sets the scheduled time based on the Pause attribute
func (m *Match) SetTime(minutes int) {
	m.Scheduled = time.Now().Add(time.Minute * time.Duration(minutes))
}

// IsStarted returns boolean whether the match has started or not
func (m *Match) IsStarted() bool {
	return !m.Started.IsZero()
}

// IsEnded returns boolean whether the match has ended or not
func (m *Match) IsEnded() bool {
	return !m.Ended.IsZero()
}
