package models

type PersonAuth struct {
	TableName   struct{} `sql:"people_auth"`
	ID          uint     `json:"id" sql:",pk"`
	PersonID    uint     `json:"person_id"`
	Provider    string   `json:"provider"`
	RemoteID    string   `json:"remote_id"`
	AvatarURL   string   `json:"avatar_url"`
	Nick        string   `json:"nick"`
	Name        string   `json:"name"`
	AccessToken string   `json:"access_token"`
}
