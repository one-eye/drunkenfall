package models

import (
	"log"
)

// AllColors is a list of the available player colors
var AllColors = []interface{}{
	"green",
	"blue",
	"pink",
	"orange",
	"white",
	"yellow",
	"cyan",
	"purple",
	"red",
}

// Player is a representation of one player in a match
type Player struct {
	ID             uint     `json:"id"`
	MatchID        uint     `sql:",pk" json:"match_id"`
	PersonID       uint     `json:"person_id"`
	Index          int      `json:"index" sql:",notnull"`
	Nick           string   `json:"nick"`
	Color          string   `json:"color"`
	PreferredColor string   `json:"preferred_color"`
	ArcherType     int      `json:"archer_type" sql:",notnull"`
	Shots          int      `json:"shots" sql:",notnull"`
	Sweeps         int      `json:"sweeps" sql:",notnull"`
	Kills          int      `json:"kills" sql:",notnull"`
	Self           int      `json:"self" sql:",notnull"`
	MatchScore     int      `json:"match_score" sql:",notnull"`
	TotalScore     int      `json:"total_score" sql:",notnull"`
	DisplayNames   []string `sql:",array" json:"display_names"`
}

// A PlayerSummary is a tournament-wide summary of the scores a player has
type PlayerSummary struct {
	ID                 uint     `json:"id"`
	TournamentID       uint     `json:"-"`
	PersonID           uint     `json:"person_id"`
	Shots              int      `json:"shots" sql:",notnull"`
	Sweeps             int      `json:"sweeps" sql:",notnull"`
	Kills              int      `json:"kills" sql:",notnull"`
	Self               int      `json:"self" sql:",notnull"`
	Matches            int      `json:"matches" sql:",notnull"`
	TotalScore         int      `json:"score" sql:",notnull"` // TODO(thiderman): Should be "total_score"
	SkillScore         int      `json:"skill_score" sql:",notnull"`
	QualifyingPosition int      `json:"qualifying_position"`
	tableName          struct{} `pg:",discard_unknown_columns"` // nolint
}

// A Summary is a representation of the SQL type of the same name
type Summary struct {
	Position     uint   `json:"position"`
	SummaryID    uint   `json:"summary_id"`
	PersonID     uint   `json:"person_id"`
	TournamentID uint   `json:"tournament_id"`
	Nick         string `json:"nick"`
	Kills        int    `json:"kills"`
	Sweeps       int    `json:"sweeps"`
	Self         int    `json:"self"`
	Shots        int    `json:"shots"`
	Matches      int    `json:"matches"`
	TotalScore   int    `json:"total_score"`
	SkillScore   int    `json:"skill_score"`
}

// A Score is a representation of the SQL type of the same name
type Score struct {
	TournamentID uint   `json:"tournament_id"`
	MatchID      uint   `json:"match_id"`
	Title        string `json:"title"`
	PlayerID     uint   `json:"player_id"`
	Nick         string `json:"nick"`
	Color        string `json:"color"`
	Kills        int    `json:"kills"`
	Sweeps       int    `json:"sweeps"`
	Self         int    `json:"self"`
	Shots        int    `json:"shots"`
	Matches      int    `json:"matches"`
	TotalScore   int    `json:"total_score"`
	MatchScore   int    `json:"match_score"`
}

type PlayerState struct {
	ID        uint   `json:"id"`
	PlayerID  uint   `json:"player_id"`
	RoundID   uint   `json:"round_id"`
	Index     int    `json:"index" sql:",notnull"`
	Arrows    Arrows `json:"arrows" sql:",array"`
	Shield    bool   `json:"shield" sql:",notnull"`
	Wings     bool   `json:"wings" sql:",notnull"`
	Hat       bool   `json:"hat" sql:",notnull"`
	Invisible bool   `json:"invisible" sql:",notnull"`
	Speed     bool   `json:"speed" sql:",notnull"`
	Alive     bool   `json:"alive" sql:",notnull"`
	Lava      bool   `json:"lava" sql:",notnull"`
	Killer    int    `json:"killer" sql:",notnull"`
	Up        int    `json:"up" sql:",notnull"`
	Down      int    `json:"down" sql:",notnull"`
}

// List of integers where one item is an arrow type as described in
// the arrow types above.
type Arrows []int

// NewPlayer returns a new instance of a player
func NewPlayer(ps *Person) *Player {
	p := &Player{
		PersonID:       ps.ID,
		Nick:           ps.Nick,
		ArcherType:     ps.ArcherType,
		PreferredColor: ps.PreferredColor,
		DisplayNames:   ps.DisplayNames,
	}

	return p
}

func NewPlayerState() *PlayerState {
	ps := &PlayerState{
		Arrows: make(Arrows, 0),
		Alive:  true,
		Hat:    true,
		Killer: -2,
	}
	return ps
}

// NewPlayerSummary returns a new instance of a tournament player
func NewPlayerSummary(ps *Person) *PlayerSummary {
	p := &PlayerSummary{
		PersonID: ps.ID,
	}
	return p
}

// NumericColor is the numeric representation of the color the player has
func (p *Player) NumericColor() int {
	for x, c := range AllColors {
		if p.Color == c {
			return x
		}
	}

	// No color was found - this is a bug. Return default.
	log.Printf("Player '%s' did not match a color for '%s'", p.Nick, p.Color)
	return 0
}

// Summary resturns a Summary{} object for the player
func (p *Player) Summary() PlayerSummary {
	return PlayerSummary{
		PersonID: p.PersonID,
		Shots:    p.Shots,
		Sweeps:   p.Sweeps,
		Kills:    p.Kills,
		Self:     p.Self,
	}
}

// // Player returns a new Player{} object from the summary
// func (p *PlayerSummary) Player() Player {
// 	person, err := DB.GetPerson(p.PersonID)
// 	if err != nil {
// 		log.Fatal(err)
// 	}

// 	return *NewPlayer(person)
// }

// DividePlayoffPlayers divides the playoff players into four buckets based
// on their score
//
// The input is expected to be sorted with score descending
func DividePlayoffPlayers(ps []*PlayerSummary) ([][]*PlayerSummary, error) {
	ret := [][]*PlayerSummary{
		[]*PlayerSummary{},
		[]*PlayerSummary{},
	}

	if len(ps) == 16 {
		ret = append(ret, []*PlayerSummary{})
		ret = append(ret, []*PlayerSummary{})
	}

	mod := len(ret)
	for x, p := range ps {
		ret[x%mod] = append(ret[x%mod], p)
	}

	return ret, nil
}
