package models

import (
	"time"

	"github.com/pkg/errors"
	"go.uber.org/zap"
)

var (
	ErrPublishDisconnected = errors.New("not connected; will not publish")
)

// Tournament represents a tournament
type Tournament struct {
	ID            uint        `json:"id"`
	Name          string      `json:"name"`
	Slug          string      `json:"slug"`
	Opened        time.Time   `json:"opened"`
	Scheduled     time.Time   `json:"scheduled"`
	Started       time.Time   `json:"started"`
	QualifyingEnd time.Time   `json:"qualifying_end"`
	Ended         time.Time   `json:"ended"`
	Color         string      `json:"color"`
	Cover         string      `json:"cover"`
	Length        int         `json:"length"`
	FinalLength   int         `json:"final_length"`
	LeagueID      int         `json:"league_id"`
	Kind          string      `json:"kind"`
	Log           *zap.Logger `sql:"-" json:"-"`
}

const matchLength = 10
const finalLength = 20

// NewTournament returns a completely new Tournament
func NewTournament(name, id, cover, kind string, scheduledStart time.Time) (*Tournament, error) {
	t := Tournament{
		Name:        name,
		Slug:        id,
		Opened:      time.Now(),
		Scheduled:   scheduledStart,
		Cover:       cover,
		Kind:        kind,
		Length:      matchLength,
		FinalLength: finalLength,
	}

	return &t, nil
}

// StartTournament will generate the tournament.
func (t *Tournament) StartTournament() error {
	t.Started = time.Now()
	return nil
}

// EndQualifyingRounds marks when the qualifying rounds end
func (t *Tournament) EndQualifyingRounds(ts time.Time) {
	t.QualifyingEnd = ts
}

// IsRunning returns boolean true if the tournament is running or not
func (t *Tournament) IsRunning() bool {
	return !t.Started.IsZero() && t.Ended.IsZero()
}

// GetCredits returns the credits object needed to display the credits roll.
func (t *Tournament) GetCredits() (*Credits, error) {
	return &Credits{}, nil
}
