package towerfall

import (
	"io/ioutil"
	"os"

	"go.uber.org/zap"
	"gopkg.in/yaml.v2"
)

type Config struct {
	Production bool   `json:"production"`
	PerishKey  string `json:"perish_key"`

	Server   *ServerConfig   `yaml:"server" json:"server"`
	Database *DatabaseConfig `yaml:"database" json:"database"`
	Discord  *DiscordConfig  `yaml:"discord" json:"discord"`
	Rabbit   *RabbitConfig   `yaml:"rabbit" json:"rabbit"`

	Log      *zap.Logger `json:"-"`
	filename string
}

// ServerConfig contains the http server configuration
type ServerConfig struct {
	Port int    `yaml:"port" json:"port"`
	Mode string `yaml:"mode" json:"mode"`
}

// DatabaseConfig unsurprisingly contains the database configuration
type DatabaseConfig struct {
	Host     string `yaml:"host" json:"host"`
	Name     string `yaml:"name" json:"name"`
	User     string `yaml:"user" json:"user"`
	Password string `yaml:"password" json:"-"` // JSON disabled to not log the password
	Verbose  bool   `yaml:"verbose" json:"verbose"`
}

// DiscordConfig unsurprisingly contains the Discord configuration
type DiscordConfig struct {
	Key      string `yaml:"key" json:"key"`
	Secret   string `yaml:"secret" json:"secret"`
	Callback string `yaml:"callback" json:"callback"`
}

// RabbitConfig unsurprisingly contains the RabbitMQ configuration
type RabbitConfig struct {
	URL      string `yaml:"url" json:"url"`
	Incoming string `yaml:"incoming" json:"incoming"`
	Outgoing string `yaml:"outgoing" json:"outgoing"`
}

func ParseConfig() *Config {
	fn := "./drunkenfall.yaml"
	env := os.Getenv("DRUNKENFALL_CONFIG")
	if env != "" {
		fn = env
	}

	logger, _ := zap.NewProduction()
	c := Config{
		Log:      logger,
		filename: fn,
	}

	body, err := ioutil.ReadFile(fn)
	if err != nil {
		c.Log.Fatal(
			"Could not read configuration",
			zap.String("filename", c.filename),
		)
	}

	err = yaml.Unmarshal(body, &c)
	if err != nil {
		c.Log.Fatal(
			"Could not unmarshal configuration file",
			zap.String("filename", c.filename),
		)
	}

	if !c.Production {
		c.Log, _ = zap.NewDevelopment()
	}

	return &c
}

// Print prints a visualization of what's going on
func (c *Config) Print() {
	c.Log.Info(
		"Configuration loaded",
		zap.String("filename", c.filename),
		zap.Any("config", c),
	)
}

// HasRabbit returns if we can talk to the Rabbit server or not
func (c *Config) HasRabbit() bool {
	return c.Rabbit.URL != ""
}
