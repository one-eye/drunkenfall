package main

import "gitlab.com/one-eye/drunkenfall/cmd"

// @title DrunkenFall
// @version 5.0
// @description The backend that serves requests for DrunkenFall tournaments
// @termsOfService https://drunkenfall.com/tos/

// @license.name MIT
// @license.url https://gitlab.com/one-eye/drunkenfall/blob/master/LICENSE

// @host dev.drunkenfall.com
// @BasePath /api
func main() {
	cmd.Execute()
}
