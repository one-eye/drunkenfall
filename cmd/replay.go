package cmd

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"

	"github.com/spf13/cobra"
)

func init() {
	rootCmd.AddCommand(replayCmd)
}

var replayCmd = &cobra.Command{
	Use:   "replay",
	Args:  cobra.ExactArgs(0),
	Short: "Replay a single round, in real time",
	Long:  ``,
	Run: func(cmd *cobra.Command, args []string) {
		client := &http.Client{}

		out, err := ioutil.ReadFile(".drunkenfall-auth-cookie")
		if err != nil {
			log.Println("Could not load auth cookie. Please run `drunkenfall authenticate`.")
			log.Printf("Error: %s\n", err)
			return
		}
		cookie := string(out)

		req, err := http.NewRequest("GET", "http://localhost:42001/api/tournaments/:current/replay", nil)
		if err != nil {
			return
		}

		req.Header.Set("Cookie", fmt.Sprintf("user-session=%s", cookie))

		resp, err := client.Do(req)
		if err != nil {
			return
		}

		defer resp.Body.Close()
		if resp.StatusCode == 401 {
			body, _ := ioutil.ReadAll(resp.Body)
			log.Printf("Error: %s\n", body)
			log.Println("Authentication failed. Please run `drunkenfall authenticate`.")
		} else if resp.StatusCode != 200 {
			log.Printf("Non-200 status: %d", resp.StatusCode)
		}
	},
}
