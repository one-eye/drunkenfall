package cmd

import (
	"log"

	"github.com/spf13/cobra"
	"gitlab.com/one-eye/drunkenfall/towerfall"
	"gitlab.com/one-eye/drunkenfall/towerfall/backend"
	"gitlab.com/one-eye/drunkenfall/towerfall/messages"
	"go.uber.org/zap"
)

func init() {
	rootCmd.AddCommand(serveCmd)
}

var serveCmd = &cobra.Command{
	Use:   "serve",
	Short: "Start and serve the backend",
	Long:  ``,
	Run: func(cmd *cobra.Command, args []string) {
		// Load the configuration
		config := towerfall.ParseConfig()
		config.Log.Info(
			"Starting DrunkenFall",
			zap.String("version", Version),
			zap.String("buildtime", BuildTime),
		)
		config.Print()

		wb := messages.NewWebsocketBroker(config.Log)

		// Instantiate the database
		db, err := towerfall.NewDatabase(config, wb)
		if err != nil {
			log.Fatal(err)
		}

		// Start the live updater
		if config.HasRabbit() {
			config.Log.Info("RabbitMQ URL set - starting listener")

			listener, err := towerfall.NewListener(config, db)
			if err != nil {
				log.Fatal(err)
			}
			go listener.Serve()
		} else {
			config.Log.Info("No RabbitMQ URL set - not starting listener")
		}

		// Create the server instance
		s := backend.NewServer(config, db, wb)

		// Actually start serving
		if err := s.Serve(); err != nil {
			log.Fatal(err)
		}

		config.Log.Info("Exiting")
	},
}
