package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
)

// These are set as buildtime arguments from the magefile, in compile()
var Version, BuildTime string

func init() {
	rootCmd.AddCommand(versionCmd)
}

var versionCmd = &cobra.Command{
	Use:   "version",
	Short: "Print the version",
	Long:  `All software has versions. This one is mine.`,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Printf("DrunkenFall %s (%s)\n", Version, BuildTime)
	},
}
