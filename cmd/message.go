package cmd

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"math/rand"
	"net/http"
	"strconv"

	"github.com/spf13/cobra"
	msg "gitlab.com/one-eye/drunkenfall/towerfall/messages"
	"gitlab.com/one-eye/drunkenfall/towerfall/models"
)

var messageURL = "http://localhost:42001/api/tournaments/:current/message"

func init() {
	messageCmd.AddCommand(startMatchCmd)
	messageCmd.AddCommand(startRoundCmd)
	messageCmd.AddCommand(endMatchCmd)
	messageCmd.AddCommand(endRoundCmd)
	messageCmd.AddCommand(killCmd)
	rootCmd.AddCommand(messageCmd)
}

var messageCmd = &cobra.Command{
	Use:   "message",
	Short: "Send control messages to the current match, as if the game sent them",
	Long:  ``,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("Subcommand needed. See --help.")
	},
}

var startMatchCmd = &cobra.Command{
	Use:   "startMatch",
	Short: "Start a match. Will start the first round as well.",
	Long:  ``,
	Run: func(cmd *cobra.Command, args []string) {
		messages := []msg.IncomingGameMessage{
			{Type: msg.InMatchStart, Data: msg.StartMatchMessage{}},
		}
		err := send(messages...)
		if err != nil {
			log.Printf("Oh noes it broek: %s", err)
			return
		}
		fmt.Println("Match started.")
		startRound(cmd, args)
	},
}

func startRound(cmd *cobra.Command, args []string) {
	messages := []msg.IncomingGameMessage{
		{Type: msg.InShield, Data: msg.ShieldMessage{Player: 0, State: false}},
		{Type: msg.InWings, Data: msg.WingsMessage{Player: 0, State: false}},
		{Type: msg.InShield, Data: msg.ShieldMessage{Player: 1, State: false}},
		{Type: msg.InWings, Data: msg.WingsMessage{Player: 1, State: false}},
		{Type: msg.InShield, Data: msg.ShieldMessage{Player: 2, State: false}},
		{Type: msg.InWings, Data: msg.WingsMessage{Player: 2, State: false}},
		{Type: msg.InShield, Data: msg.ShieldMessage{Player: 3, State: false}},
		{Type: msg.InWings, Data: msg.WingsMessage{Player: 3, State: false}},
		{Type: msg.InRoundStart, Data: msg.StartRoundMessage{
			Arrows: []models.Arrows{
				{0, 0, 0, 0, 0, 0},
				{0, 0, 0, 0, 0, 0},
				{0, 0, 0, 0, 0, 0},
				{0, 0, 0, 0, 0, 0},
			},
		}},
	}
	err := send(messages...)
	if err != nil {
		log.Printf("Oh noes it broek: %s", err)
		return
	}
	fmt.Println("Round started.")
}

var startRoundCmd = &cobra.Command{
	Use:   "startRound",
	Short: "Start a round.",
	Long:  ``,
	Run:   startRound,
}

var endMatchCmd = &cobra.Command{
	Use:   "endMatch",
	Short: "End the current match. Will end the last round as well.",
	Long:  ``,
	Run: func(cmd *cobra.Command, args []string) {
		messages := []msg.IncomingGameMessage{
			{Type: msg.InRoundEnd, Data: struct{}{}},
			{Type: msg.InMatchEnd, Data: struct{}{}},
		}
		err := send(messages...)
		if err != nil {
			log.Printf("Oh noes it broek: %s", err)
			return
		}
		fmt.Println("Match ended.")
	},
}

var endRoundCmd = &cobra.Command{
	Use:   "endRound",
	Short: "End the current round.",
	Long:  ``,
	Run: func(cmd *cobra.Command, args []string) {
		messages := []msg.IncomingGameMessage{
			{Type: msg.InRoundEnd, Data: struct{}{}},
		}
		err := send(messages...)
		if err != nil {
			log.Printf("Oh noes it broek: %s", err)
			return
		}
		fmt.Println("Round ended.")
	},
}

var killCmd = &cobra.Command{
	Use:   "kill <player> [killer [cause]]",
	Short: "Kill a player.",
	Long: `If no killer is selected, one is chosen on random, and can include the
current player (i.e. a self-kill). A cause can also be provided, and if not
provided it will be randomized.

Causes are as follows:

	0  Arrow
	1  Explosion
	2  Brambles
	3  JumpedOn
	4  Lava
	5  Shock
	6  SpikeBall
	7  FallingObject
	8  Squish
	9  Curse
	10 Miasma (timeout shade)
	11 Enemy (adventure mode only)
	12 Chalice (unused in the game)
`,
	Args: cobra.RangeArgs(1, 3),
	Run: func(cmd *cobra.Command, args []string) {
		var err error
		var player, killer, cause int

		player, err = strconv.Atoi(args[0])
		if err != nil {
			return
		}

		if len(args) > 1 {
			killer, err = strconv.Atoi(args[1])
			if err != nil {
				return
			}
		} else {
			killer = rand.Intn(4)
		}

		if len(args) > 2 {
			cause, err = strconv.Atoi(args[2])
			if err != nil {
				return
			}
		} else {
			cause = rand.Intn(12)
		}

		message := msg.IncomingGameMessage{
			Type: msg.InKill,
			Data: msg.KillMessage{
				Player: player,
				Killer: killer,
				Cause:  cause,
			},
		}
		err = send(message)
		if err != nil {
			log.Printf("Oh noes it broek: %s", err)
			return
		}
		fmt.Printf("Player %d killed by %d via %d.\n", player, killer, cause)
	},
}

func send(args ...msg.IncomingGameMessage) error {
	client := &http.Client{}
	for _, msg := range args {
		out, err := json.Marshal(msg)
		if err != nil {
			return err
		}

		req, err := http.NewRequest("POST", messageURL, bytes.NewBuffer(out))
		if err != nil {
			return err
		}

		resp, err := client.Do(req)
		if err != nil {
			return err
		}

		defer resp.Body.Close()
		if resp.StatusCode != 200 {
			log.Printf("Non-200 status: %d", resp.StatusCode)
			break
		}
	}

	return nil
}
