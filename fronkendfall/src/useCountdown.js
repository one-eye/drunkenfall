import { useEffect, useState } from 'react';

const useCountdown = (endTime) => {
  const [ms, setMs] = useState(new Date(endTime) - new Date());
  useEffect(() => {
    const id = setInterval(() => {
      setMs(new Date(endTime) - new Date());
    }, 100);

    return () => {
      clearInterval(id);
    }
  }, [endTime, setMs]);

  return Math.max(ms, 0);
};

export default useCountdown;