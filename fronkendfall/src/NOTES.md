# Why the fuck is the font lighter?

tl; dr: to make everything look non-horrible on non-retina displays

We need to add font smoothing rules in the stylesheet to enable antialiasing (for some reason it's not by default. i couldn't be arsed to google it). This fixes the horrible pixelated look of the text on displays with "normal" DPI; but makes text appear thinner.

Normally, to fix that, one would bump the font-weight by 100 or so. But the main font we use, Lato, offers only 3 different weights: 400 (normal), 700 (bold), 900 (black). Since there's no 500, the next step is 700, which would make **everything bold all the time, like this, annoying isn't it**?


# Why don't we have two different styles of buttons on the home page anymore?

It makes epsilon% sense to differentiate between those classes of action from a UX perspective. Also the dim button looked too dim. We can revisit this decision and redesign a new button if needed. (spoiler alert: it won't be needed)

