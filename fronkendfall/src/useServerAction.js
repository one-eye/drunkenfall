import { useState } from "react";

const useServerAction = (actor) => {
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(false);
  const [success, setSuccess] = useState(false);
  const handleClick = (e) => {
    e.preventDefault();
    setLoading(true);
    setError(false);
    actor()
      .then(response => {
        console.log(response);
        setSuccess(true);
        setTimeout(() => setSuccess(false), 3000);
        return response;
      })
      .catch((e) => {
        console.error(e);
        setError(true);
        setTimeout(() => setError(false), 3000);
      })
      .finally(() => setLoading(false));
  };
  return [handleClick, loading, error, success];
};

export default useServerAction;
