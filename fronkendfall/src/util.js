const ordinals = ['th', 'st', 'nd', 'rd'];
const getOrdinalNumber = (number) => {
  const ordinal = ordinals[number % 10] || 'th';
  return `${number}${ordinal}`;
}
const MonthAndDay = new Intl.DateTimeFormat('en-US', { month: 'long', day: 'numeric' })
const MonthDayAndYear = new Intl.DateTimeFormat('en-US', { month: 'long', day: 'numeric', year: 'numeric' })
const toMonthAndDay = (dateString) => {
  return MonthAndDay.formatToParts(new Date(dateString))
    .map(part => part.type === 'day' ? getOrdinalNumber(part.value) : part.value)
    .join('');
}
const toMonthDayAndYear = (dateString) => {
  return MonthDayAndYear.formatToParts(new Date(dateString))
    .map(part => part.type === 'day' ? getOrdinalNumber(part.value) : part.value)
    .join('');
}

export {
  toMonthAndDay,
  toMonthDayAndYear
}
