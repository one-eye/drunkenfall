// const baseUrl = 'http://localhost';
// const baseApiUrl = `${baseUrl}:3000/api`;
import { sanitizeTournamentDetailObject } from "../lib/TournamentUtils";
import { sanitizeMatchObject } from "../lib/MatchUtils";
import {Color, LeaderboardEntry, Person, PlayerSummary, Tournament, TournamentResponse} from "../lib/Types";
import {Maybe} from "../lib/Maybe";

const baseApiUrl = `/api`;

const rejectIfNotOk = (path: string) => {
  return (r: Response): Response | Promise<any> => {
    return r.ok ?
      r :
      new Promise((_, reject) => reject(new Error(`Call to ${path} failed. ${r.status} (${r.statusText})`)));
  };
};

const get = (path: string, options = {}): Promise<any> => fetch(`${baseApiUrl}${path}`, {
  ...options,
  credentials: 'include'
})
  .then(rejectIfNotOk(path))
  .then(r => r.json());

const postRaw = (path: string, body: any, options = {}) => fetch(`${baseApiUrl}${path}`, {
    ...options,
    method: 'POST',
    credentials: 'include',
    headers: {
      "Content-Type": "application/json;charset=UTF-8",
    },
    body: JSON.stringify(body)
  })
  .then(rejectIfNotOk(path));

const post = (path: string, body?: any, options?: any) => postRaw(path, body, options)
  .then(r => r.json());

const del = (path: string, options = {}) => fetch(`${baseApiUrl}${path}`, {
  ...options,
  method: 'DELETE',
  credentials: 'include',
})
  .then(rejectIfNotOk(path))
  .then(r => r.json());

export const getAllTournaments = (): Promise<Tournament[]> => {
  return get('/tournaments/')
    .then(r => r.tournaments.map(sanitizeTournamentDetailObject));
};
/**
 * Returns tournaments in the current league.
 * A tournament is in the current league if it has a cover image.
 * TODO: Make the model aware of the leagues.
 */
export const getCurrentLeagueTournaments = (): Promise<Tournament[]> => {
  return getAllTournaments()
    .then(tournaments => tournaments.filter(t => t.cover));
};

export const getLatestLeagueTournament = (): Promise<Maybe<Tournament>> => {
  return getCurrentLeagueTournaments()
    .then(tournaments => new Maybe(tournaments[0]));
};

export const getTournament = (id: number): Promise<TournamentResponse> => {
  return get(`/tournaments/${id}/`)
    .then(t => ({
      ...t,
      matches: t.matches.map(sanitizeMatchObject),
      tournament: sanitizeTournamentDetailObject(t.tournament)
    }));
};

export const getLatestTournament = (): Promise<TournamentResponse> => {
  return get('/tournaments/:latest')
    .then((t => ({
      ...t,
      matches: t.matches.map(sanitizeMatchObject),
      tournament: sanitizeTournamentDetailObject(t.tournament)
    })));
};

export const registerPlayer = (nick: string, name: string, color: Color, isAlternate: boolean): Promise<any> => {
  // eslint-disable-next-line @typescript-eslint/camelcase
  const data = { nick, name, color, archer_type: isAlternate ? 1 : 0 };
  return post('/user/register/', data);
};


export const updateSettings = (nick: string, name: string, color: Color, isAlternate: boolean): Promise<any> => {
  // eslint-disable-next-line @typescript-eslint/camelcase
  const data = { nick, name, color, archer_type: isAlternate ? 1 : 0 };
  return post('/user/settings/', data)
    .then(r => r.person);
};

const sorted = <T> (fn: ((a: T, b: T) => number)): ((list: T[]) => T[]) => {
  return (list: T[]): T[] => {
    list.sort(fn);
    return list;
  };
};

const sortedByName = sorted((p1: Person, p2: Person) => p1.name.localeCompare(p2.name));

export const getPeople = (): Promise<Person[]> => {
  return get('/people/')
    .then(r => r.people)
    .then(p => sortedByName(p));
};

export const getAllPeople = () => {
  return get('/people/?all=true')
    .then(r => r.people)
    .then(p => sortedByName(p));
};

export const getLeaderboard = (): Promise<LeaderboardEntry[]> => {
  return get('/people/')
    .then(r => r.leaderboard);
};

export const getAllPeopleAndLeaderboard = () => {
  return get('/people/?all=true')
};


export const getFakeTournamentName = (): Promise<string> => get('/fake/name');

type TournamentInput = {
  name: string;
  color: Color;
  id: number;
  scheduled: string; //FIXME
}

type DrunkenfallInput = TournamentInput & { cover: string };

export const createDrunkenfall = ({ name, color, cover, id, scheduled }: DrunkenfallInput): Promise<{id: number}> => {
  const data = { name, color, cover, id, scheduled };
  return post('/tournaments/', data);
};

export const createGroupTournament = ({ name, color, id, scheduled }: TournamentInput): Promise<{id: number}> => {
  const data = { name, color, id, scheduled };
  return post('/tournaments/', data);
};

interface LoggedOutResponse {
  authenticated: boolean;
}

const getUser = (): Promise<Person | LoggedOutResponse> => {
  return get('/user/');
};

function isLoggedOutResponse(arg: any): arg is LoggedOutResponse {
  return arg.authenticated !== undefined;
}

export const getLoggedInUser = (): Promise<Person | undefined> => {
  return getUser()
    .then(r => {
      if (isLoggedOutResponse(r)) {
        return undefined;
      } else {
        return ({...r, authenticated: true})
      }
    });
};

export const deleteTestTournaments = (): Promise<any> => {
  return del(`/tournaments/`)
    .then(r => r.tournaments);
};

export const startTournament = (id: number): Promise<any> => {
  return get(`/tournaments/${id}/start/`);
};

export const toggleUser = (id: number): Promise<any> => {
  return get(`/user/disable/${id}`);
};

export const toggleParticipant = (tournamentId: number, playerId: number): Promise<any> => {
  return get(`/tournaments/${tournamentId}/toggle/${playerId}`);
};

export const getParticipants = (tournamentId: number): Promise<PlayerSummary[]> => {
  return get(`/tournaments/${tournamentId}/players/`)
    .then(response => response.player_summaries);
};

export const startMatch = (tournamentId: number, matchIndex: number): Promise<any> => {
  return post(`/tournaments/${tournamentId}/play/?index=${matchIndex}`);
};

export const addTestingPlayers = (tournamentId: number): Promise<any> => {
  return get(`/tournaments/${tournamentId}/usurp/`);
};

export const pauseUntil = (tournamentId: number, minutes: number): Promise<any> => {
  return get(`/tournaments/${tournamentId}/time/${minutes}`);
};

export const logOut = (): Promise<any> => {
  return get('/user/logout/');
};

export const endQualifying = (tournamentId: number, time: string): Promise<void> => {
  return post(`/tournaments/${tournamentId}/endqualifying/`, {time});
};

