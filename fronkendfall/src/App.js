import React, { useEffect } from 'react';
import { connect, Provider } from 'react-redux';
import { Route, Switch } from "react-router-dom";
import './App.css';
import routes from "./routes";
import ScrollToTop from './ScrollToTop';
import NavigationBar from './components/NavigationBar';
import protectedPage from './components/ProtectedPage';
import store, {history} from './store';
import ErrorBoundary from "./components/ErrorBoundary";
import withErrorBoundary from "./components/withErrorBoundary";
import { logIn, wsConnect, wsDisconnect } from "./actions";
import { ConnectedRouter } from 'connected-react-router'

const getWsUrl = () => {
  const proto = window.location.protocol === "https:" ? 'wss://' : "ws://";
  return `${proto}${window.location.hostname}/api/auto-updater`;
};

const mapStateToProps = () => ({});
const mapDispatchToProps = (dispatch) => ({
  connectWs: () => dispatch(wsConnect(getWsUrl())),
  disconnectWs: () => dispatch(wsDisconnect()),
  logIn: () => dispatch(logIn()),
});

const App = connect(mapStateToProps, mapDispatchToProps)(({connectWs, disconnectWs, logIn}) => {
  useEffect(() => {
    connectWs();
    logIn();
    return () => disconnectWs();
  },[]);
  return (
    <ConnectedRouter history={history}>
      <>
          <NavigationBar />
          <ScrollToTop>
            <div className="content">
              <ErrorBoundary>
                <Switch>
                  {
                    routes.map((route, i) => {
                      const Component =
                        withErrorBoundary(
                          route.requireLogin ?
                            protectedPage(route.minUserLevel, route.component)
                            :
                            route.component);
                      return (
                          <Route key={i} {...route} component={Component} />
                      )
                    })
                  }
                </Switch>
              </ErrorBoundary>
            </div>
          </ScrollToTop>
      </>
    </ConnectedRouter>
  );
});

export default () => (<Provider store={store}><App /></Provider>);
