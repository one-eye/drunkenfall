import { combineReducers, createStore, applyMiddleware} from "redux";
import thunk from 'redux-thunk';
import * as reducers from './reducers'
import webSocketMiddleware from "./webSocketMiddleware";
import {composeWithDevTools} from "redux-devtools-extension";
import {createBrowserHistory, History, LocationState} from "history";
import {connectRouter, routerMiddleware} from 'connected-react-router';

export const history: History<LocationState> = createBrowserHistory();

const middlewares = [routerMiddleware(history), thunk, webSocketMiddleware];

const rootReducerCreator = (history: History<LocationState>) => combineReducers({
    router: connectRouter(history),
    ...reducers
  }
);
const rootReducer = rootReducerCreator(history);

const store = createStore(
  rootReducer,
  composeWithDevTools(
      applyMiddleware(...middlewares)
  )
);

export type AppState = ReturnType<typeof rootReducer>;
export default store;
