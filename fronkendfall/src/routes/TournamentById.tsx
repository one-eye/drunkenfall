import React from "react";
import TournamentByIdContainer from "../containers/TournamentByIdContainer";
import { RouteComponentProps } from "react-router";
import Tournament from "./Tournament";

type MatchParams = {
  id: string;
}

const TournamentById: React.FunctionComponent<RouteComponentProps<MatchParams>> = ({ match }) => {
  const tournamentId = parseInt(match.params.id);
  return (
    <TournamentByIdContainer id={tournamentId}>
      {
        tournament => <Tournament tournament={tournament}/>
      }
    </TournamentByIdContainer>
  );
};

export default TournamentById;