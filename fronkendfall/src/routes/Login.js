import React from 'react';
import './Login.css';
import { Redirect } from "react-router-dom";
import Button from '../components/Button';
import { faArrowRight } from '@fortawesome/free-solid-svg-icons';
import { connect } from "react-redux";

const Login = ({user, location: { state } }) => {

  const { from } = state || { from: '/' };

  return (
    <div id="login-page">
      {
        user !== undefined ? <Redirect to={from} /> :
          <form>
            <Button onClick="/api/auth/discord/" type="external" icon={faArrowRight}>Login with Discord</Button>
          </form>
      }
    </div>
  )
};

const mapStateToProps = (state) => ({
  user: state.session.data.user,
});
export default connect(mapStateToProps)(Login);
