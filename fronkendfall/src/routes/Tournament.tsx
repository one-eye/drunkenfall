import React from 'react';
import PastTournament from './PastTournament';
import RunningTournament from "./RunningTournament";
import ScheduledTournament from "./ScheduledTournament";
import { isEnded, isStarted } from "../lib/DateUtils";
import {Person, TournamentResponse} from "../lib/Types";
import {Dict} from "../reducers";
import PeopleContainer from "../containers/PeopleContainer";

type TournamentProps = {
  tournament: TournamentResponse;
}

const getElementToRender = (tournament: TournamentResponse): React.FunctionComponent<{tournament: TournamentResponse; people: Dict<Person>}> => {
  if (isEnded(tournament.tournament)) {
    return PastTournament;
  }
  if (isStarted(tournament.tournament)) {
    return RunningTournament;
  }
  return ScheduledTournament;
};

const Tournament: React.FunctionComponent<TournamentProps> = ({ tournament }) => {
  const ElementToRender = getElementToRender(tournament);
  return (
    <div id="tournament-page">
      {
        <PeopleContainer>
          {
            people => (<ElementToRender tournament={tournament} people={people}/>)
          }
        </PeopleContainer>
      }
    </div>
  )
};

export default Tournament;