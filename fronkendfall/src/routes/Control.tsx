import React from 'react';

import './Control.css';
import { RouteComponentProps } from 'react-router';
import TournamentByIdContainer from "../containers/TournamentByIdContainer";
import PeopleContainer from "../containers/PeopleContainer";
import Control from "../components/Control";

type MatchParams = {
  id: string;
}

const ControlTournament: React.FunctionComponent<RouteComponentProps<MatchParams>> = ({ match }) => {
  const tournamentId = parseInt(match.params.id);
  return (
    <TournamentByIdContainer id={tournamentId}>
      {
        tournament => (
          <PeopleContainer>
            {
              people => <Control people={people} tournament={tournament} />
            }
          </PeopleContainer>
        )
      }
    </TournamentByIdContainer>
  );
};

export default ControlTournament;