import React from "react";

import './Participants.css';
import {RouteComponentProps} from "react-router";
import TournamentByIdContainer from "../containers/TournamentByIdContainer";
import PeopleContainer from "../containers/PeopleContainer";
import ParticipantsContainer from "../containers/ParticipantsContainer";
import ParticipantSelector from "../components/ParticipantSelector";

type MatchParams = {
  id: string;
}

const TournamentParticipants: React.FunctionComponent<RouteComponentProps<MatchParams>> = ({match}) => (
  <TournamentByIdContainer id={parseInt(match.params.id)}>
    {
      tournament => (
        <PeopleContainer>
          {
            people => (
              <ParticipantsContainer tournamentId={tournament.tournament.id}>
                {
                  participants => (
                    <ParticipantSelector people={people} participants={participants} tournament={tournament} />
                  )
                }
              </ParticipantsContainer>
            )
          }
        </PeopleContainer>
      )
    }
  </TournamentByIdContainer>
);

export default TournamentParticipants;