import React from "react";
import {AppState} from "../store";
import {Redirect, RouteComponentProps} from "react-router";
import {connect} from "react-redux";
import useInputState from "../useInputState";
import {nowTime, todayAtTime} from "../lib/DateUtils";
import SubmitButton from "../components/SubmitButton";
import {endQualifying} from "../api/api";
import useSubmitAndRedirect from "../useSubmitAndRedirect";
import {faSave} from "@fortawesome/free-solid-svg-icons";

import "./EndQualifying.css";

interface StateProps {
  tournamentId: number;
}

type EndQualifyingProps = StateProps;
const EndQualifying: React.FunctionComponent<EndQualifyingProps> = ({tournamentId}) => {
  const [time, handleTimeChange] = useInputState(nowTime());
  const [handleSubmit, loading, error, success, redirect] = useSubmitAndRedirect(
    () => endQualifying(tournamentId, todayAtTime(time)),
    () => `/tournaments/${tournamentId}`
  );
  const isValid = time.trim().length > 0;
  return (
    <div id="end-qualifying-page">
      {
        redirect && <Redirect to={redirect} />
      }
      <h1>End Qualifying</h1>
      <form onSubmit={handleSubmit}>
        <input type="time" onChange={handleTimeChange} value={time}/>
        <SubmitButton icon={faSave} disabled={!isValid} {...{loading, error,success}}>
          Go go
        </SubmitButton>
      </form>
    </div>
  );
};

type MatchParams = {
  id: string;
}

const mapStateToProps = (state: AppState, ownProps: RouteComponentProps<MatchParams>) => {

  const tournamentId = parseInt(ownProps.match.params.id);

  return ({
    tournamentId,
  });
};

export default connect(
  mapStateToProps,
)(EndQualifying)