import {
  matchEnded,
  matchesUpdated,
  matchUpdated,
  playerSummariesUpdated,
  tournamentUpdated,
  WS_CONNECT,
  WS_DISCONNECT,
  wsConnected,
  wsDisconnected
} from "./actions";
import { sanitizeTournamentDetailObject } from "./lib/TournamentUtils";
import { sanitizeMatchObject } from "./lib/MatchUtils";
import ReconnectingWebSocket from 'reconnecting-websocket';


const webSocketMiddleware = () => {
  let socket = null;

  const onOpen = store => event => {
    console.log("websocket onOpen");
    store.dispatch(wsConnected(event.target.url))
  };

  const onClose = store => event => {
    console.log("websocket onClose");
    store.dispatch(wsDisconnected());
  };

  const onError = store => event => {
    console.log("websocket onerror");
  };

  const onMessage = store => event => {
    const payload = JSON.parse(event.data);

    switch (payload.type) {
      case "player_summaries":
        store.dispatch(
          playerSummariesUpdated(
            payload.data.tournament_id,
            payload.data.player_summaries));
        break;
      case "tournament":
        store.dispatch(
          tournamentUpdated(sanitizeTournamentDetailObject(payload.data)));
        break;
      case "match":
        store.dispatch(
          matchUpdated(
            payload.data.tournament,
            sanitizeMatchObject(payload.data.match),
            payload.data.player_states));
        break;
      case "matches":
        store.dispatch(
          matchesUpdated(
            payload.data.tournament_id,
            payload.data.matches.map(match => sanitizeMatchObject(match))));
        break;
      case "match_end":
        store.dispatch(
          matchEnded(
            payload.data.tournament_id,
            {
              tournament: sanitizeTournamentDetailObject(payload.data.tournament),
              matches: payload.data.matches.map(match => sanitizeMatchObject(match)),
              // eslint-disable-next-line @typescript-eslint/camelcase
              player_states: payload.data.player_states,
              // eslint-disable-next-line @typescript-eslint/camelcase
              player_summaries: payload.data.player_summaries,
              runnerups: payload.data.runnerups,
            }
          )
        );
        break;
      default:
        console.log("WebSocket message", payload)
    }
  };

  return store => next => action => {
    switch (action.type) {
      case WS_CONNECT:
        if(socket !== null) {
          socket.close();
        }

        socket = new ReconnectingWebSocket(action.host);

        socket.onmessage = onMessage(store);
        socket.onclose = onClose(store);
        socket.onopen = onOpen(store);
        socket.onerror = onError(store);

        return next(action);
      case WS_DISCONNECT:
        if(socket !== null) {
          socket.close();
        }
        socket = null;
        return next(action);
      default:
        return next(action);
    }
  }
};

export default webSocketMiddleware();