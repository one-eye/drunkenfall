import {Person} from "../lib/Types";
import React from "react";
import useInputState from "../useInputState";
import {PersonAvatar} from "./Avatar";

import "./FilterableUserList.css";

interface FilterableUserListProps {
  people: Person[];
  onUserSelect: (p: Person) => void;
}

export const FilterableUserList: React.FunctionComponent<FilterableUserListProps> = ({people, onUserSelect}) => {
  const [filter, handleFilterChange, setFilter] = useInputState('');
  const trimmed = filter.trim().toLowerCase();
  const filtered = trimmed.length > 0 ?
    people.filter(p => p.nick.toLowerCase().includes(trimmed) || p.name.toLowerCase().includes(trimmed)) : people;
  return (
    <div className="filterable-user-list">
      <div className="input">
        <input type="text" value={filter} placeholder="Filter.." onChange={handleFilterChange}/>
        <button onClick={() => setFilter('')}><span role="img" aria-label="cross">❌</span></button>
      </div>
      {
        filtered.map(person => (
          <div key={person.id} className="user" onClick={() => onUserSelect(person)}>
            <div className="avatar">
              <PersonAvatar person={person}/>
            </div>
            <div className="identifier">
              <div className="nick">{person.nick}</div>
              <div className="name">{person.name}</div>
            </div>
          </div>
        ))
      }
    </div>
  );
};