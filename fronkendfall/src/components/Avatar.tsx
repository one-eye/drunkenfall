import React from "react";
import Ribbon from "../components/Ribbon";
import styles from './Avatar.module.css';
import {Person} from "../lib/Types";
import ImageWithFallback from "./ImageWithFallback";
import placeholder from "../img/greenSlime.png";
import {ClassNameBuilder} from "../lib/ClassNameUtils";

interface AvatarWithRibbonProps {
  person: Person;
  className?: string;
}

export const AvatarWithRibbon: React.FunctionComponent<AvatarWithRibbonProps> = ({person, className}) => {
  const classes = new ClassNameBuilder("avatar-with-ribbon").add(className);
  return (
    <div className={classes.build()}>
      <div className={styles.avatarWithRibbon}>
        <div className="image">
          <ImageWithFallback
            src={person.avatar_url}
            fallback={placeholder}
            width="96"
            height="96"
            title={person.nick}
            alt={person.nick}/>
        </div>
        <Ribbon color={person.preferred_color}>
          {person.nick}
        </Ribbon>
      </div>
    </div>
  );
};

interface PersonAvatarProps {
  person: Person;
}

export const PersonAvatar: React.FunctionComponent<PersonAvatarProps> = ({person}) => {
  return (
    <ImageWithFallback
      className="person"
      src={person.avatar_url}
      fallback={placeholder}
      width="96"
      height="96"
      title={person.nick}
      alt={person.nick}/>
  );
};

interface AvatarProps {
  url: string;
  title: string;
}

export const Avatar: React.FunctionComponent<AvatarProps> = ({url, title}) => {
  return (
    <ImageWithFallback
      className={styles.avatarImage}
      src={url}
      fallback={placeholder}
      width="96"
      height="96"
      title={title}
      alt={title}/>
  );
};
