import React, { useState } from "react";

import './ArcherSelector.css';
import { archers } from '../lib/ArcherImages';
import { ClassNameBuilder } from "../lib/ClassNameUtils";

const Archer = ({ image, name, alternate, selected, onSelect }) => {
  const classNameList = new ClassNameBuilder('archer');
  if (selected) {
    classNameList.add('selected');
  }
  return (
    <div className={classNameList.build()} onClick={() => onSelect(name, alternate)}>
      <img src={image} alt={`${name} archer${alternate ? ' (alternate)' : ''}`} />
    </div>
  );
};

const ArcherSelector = ({ initialArcher = '', initialAlternate = false, onChange }) => {
  const [selected, setSelected] = useState({ name: initialArcher, alternate: initialAlternate });
  const onSelect = (name, alternate) => {
    setSelected({ name, alternate });
    onChange && onChange({ name, alternate });
  };

  return (
    <div className="archer-selector">
      <label>Preferred archer</label>
      {
        archers.map(archer => (
          <div key={`${archer.name}`}>
            <Archer
              image={archer.image}
              name={archer.name}
              alternate={false}
              onSelect={onSelect}
              selected={selected.name === archer.name && selected.alternate === false}
            />
            <Archer
              image={archer.alternate}
              name={archer.name}
              alternate={true}
              onSelect={onSelect}
              selected={selected.name === archer.name && selected.alternate === true}
            />
          </div>
        ))
      }
    </div>
  )
};

export default ArcherSelector;
