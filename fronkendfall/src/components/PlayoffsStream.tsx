import React from "react";
import {Person, TournamentResponse} from "../lib/Types";
import {Dict} from "../reducers";
import logo from "../img/oem.svg";

import {getCurrentMatch, getTitleFromFullName} from "../lib/TournamentUtils";
import {getLevelTitle, getMatchLength} from "../lib/MatchUtils";
import StatusBar from "../components/StatusBar";

type PlayoffsStreamProps = {
  tournament: TournamentResponse;
  people: Dict<Person>;
}

const PlayoffsStream: React.FunctionComponent<PlayoffsStreamProps> = ({tournament, people }) => {
  const tournamentTitle = getTitleFromFullName(tournament.tournament.name);
  const currentMatch = getCurrentMatch(tournament).getOrThrow(() => new Error(`Could not get the current match`));
  const matchLength = getMatchLength(currentMatch);
  return (
    <div id="game">
      <div className="branding">
        <div className="logo">
          <img src={logo} alt="Drunkenfall Logo"/>
        </div>
        <div className="tournament">
          <h1>
            Drunkenfall
          </h1>
          <h2>
            {tournamentTitle}
          </h2>
        </div>
      </div>
      <div className="caster-info">
        <div className="caster">Loud Person</div>
        <div className="caster">Crazy Person</div>
      </div>
      <div className="match-info">
        <div className="title">
          {currentMatch.name} - Round {currentMatch.round}
        </div>
        <div className="level-and-length">
          {getLevelTitle(currentMatch)} - Match to {matchLength} points
        </div>
      </div>
      <StatusBar currentMatch={currentMatch} tournament={tournament} people={people}/>
    </div>
  )
};

export default PlayoffsStream;