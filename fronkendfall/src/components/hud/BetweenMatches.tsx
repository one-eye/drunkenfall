import React from "react";
import useCurrentTime from "../../useCurrentTime";
import {getNextMatch, getTitleFromFullName} from "../../lib/TournamentUtils";
import {getLevelName} from "../../lib/LevelUtils";
import Countdown from "../Countdown";
import {AvatarWithRibbon, PersonAvatar} from "../Avatar";

import './BetweenMatches.css';
import logo from '../../img/oem.svg';
import {dateToTimeStringWithTimezone} from "../../lib/DateUtils";
import {LiveTournamentProps} from "../../containers/LiveTournamentContainer";


export const BetweenMatches: React.FunctionComponent<LiveTournamentProps> = ({tournament, people}) => {
  const now = useCurrentTime();
  const nextMatch = getNextMatch(tournament.matches).getOrThrow(() => new Error("Could not get the next match."));
  return (
    <div id="between-matches">
      <div className="header">
        <div className="branding">
          <div className="logo">
            <img src={logo} alt="DrunkenFall" />
          </div>
          <div className="tournament">
            <h1>DrunkenFall</h1>
            <h2>{getTitleFromFullName(tournament.tournament.name)}</h2>
          </div>
        </div>
        <div className="current-time">
          {
            dateToTimeStringWithTimezone(now)
          }
        </div>
      </div>
      <div className="match-info">
        <h1>{nextMatch.name}</h1>
        <h2>{getLevelName(nextMatch.level)}</h2>
        <h3><Countdown to={nextMatch.scheduled}/></h3>
      </div>
      <div className="players">
        {
          nextMatch.players.map(player => (
            <AvatarWithRibbon person={people[player.person_id]} />
          ))
        }
      </div>
    </div>
  );
};