import React from 'react';
import './UserPicker.css';
import {Person} from "../lib/Types";
import {PersonAvatar} from "./Avatar";
import {ClassNameBuilder} from "../lib/ClassNameUtils";
import {padArray} from "../lib/CollectionUtils";

type UserListProps = {
  users: Person[];
  className?: string;
}

const Placeholder: React.FC = () => (
  <div className="placeholder">
    &nbsp;
  </div>
);

export const UserCloud = ({ users, className }: UserListProps) => {
  const classNameList = new ClassNameBuilder(className).add('user-cloud');
  const slots = padArray(users, 32, null);
  return (
    <div className={classNameList.build()}>
      {
        slots.map((p,i) => (
          p === null ?
            <Placeholder key={`pl${i}`} />
            :
            <PersonAvatar key={p.id} person={p} />
          ))
      }
    </div>
  )
};
