import React, { useEffect } from 'react';
import Error from './Error';

const Loading = ({ isLoading, error, children }) => {
  useEffect(() => {
    error && console.error(error);
  }, [error]);
  return (
    <>
      {
        isLoading ?
          <div className="loading">Loading...</div>
          :
          error ? <Error error={error}>Something went terribly wrong.. :(</Error> :
            <>
              {children}
            </>
      }
    </>
)};

export default Loading;
