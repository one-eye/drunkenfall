import React from 'react';
import { Redirect } from "react-router-dom";
import { connect } from "react-redux";


const mapStateToProps = (state) => ({
  user: state.session.data.user
});

const protectedPage = (minUserLevel, WrappedComponent) => connect(mapStateToProps)((props) => {
  const user = props.user;

  return (
    user === undefined || user.userlevel < minUserLevel ? <Redirect to={{ pathname: "/login", state: { from: props.location } }} /> :
      <WrappedComponent {...props} loggedInUser={user} />
  )
});

export default protectedPage;
