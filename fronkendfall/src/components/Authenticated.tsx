import React from 'react';
import './Authenticated.css';
import {AppState} from "../store";
import {logIn} from "../actions";
import {ThunkDispatch} from "redux-thunk";
import {connect} from "react-redux";
import {Person} from "../lib/Types";

interface OwnProps {
  userLevel?: number;
  ifNot?: React.ReactElement;
  children: any;
}
interface StateProps {
  user: Person|undefined;
}
interface DispatchProps {
  logIn: () => void;
}

type AuthenticatedProps = OwnProps & StateProps & DispatchProps;

const Authenticated: React.FunctionComponent<AuthenticatedProps> = (
  { userLevel = 0, children, ifNot, user }) => {

  return (
    <>
      {
        user === undefined ?
          ifNot
          :
          user.userlevel < userLevel ?
            (ifNot && ifNot)
            :
            children
      }
    </>
  )
};

const mapStateToProps = (state: AppState): StateProps => ({
  user: state.session.data.user
});

const mapDispatchToProps = (dispatch: ThunkDispatch<{}, {}, any>): DispatchProps => ({
  logIn: (): void => {dispatch(logIn())}
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Authenticated);
