import React from "react";
import {Color} from "../lib/Types";
import {ClassNameBuilder} from "../lib/ClassNameUtils";
import {Avatar} from "./Avatar";

import styles from "./PersonCard.module.css";
import colors from "./BackgroundColors.module.css";

interface PersonCardProps {
  nick: string;
  color: Color;
  avatarUrl: string;
  className?: string;
  highlight?: HighlightStatus;
}

export type HighlightStatus = "highlight" | "normal" | "dim";

const PersonCard: React.FC<PersonCardProps> = ({nick, avatarUrl, color, highlight, className, children}) => {
  const classes = new ClassNameBuilder(styles.personCard).add(className);
  const infoClasses = new ClassNameBuilder(styles.info);

  if(highlight === "highlight") {
    infoClasses.add(colors[color]);
  }

  if(highlight === "dim") {
    classes.add(styles.dim);
  }

  return (
    <div className={classes.build()}>
      <div className={`${styles.avatarContainer} ${colors[color]}`}>
        <Avatar url={avatarUrl} title={nick} />
      </div>
      <div className={infoClasses.build()}>
        <div className={styles.nick}>
          {nick}
        </div>
        {
          children !== undefined && children
        }
      </div>
    </div>
  )
};

export default PersonCard;