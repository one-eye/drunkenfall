import React from "react";
import useCountdown from "../useCountdown";
import {toTimeString} from "../lib/DateUtils";

interface CountdownProps {
  to: string | null ;
}

const Countdown: React.FunctionComponent<CountdownProps> = ({to}) => {
  const timeLeft = useCountdown(to);
  const eta = timeLeft === 0 ? "Soon™" : toTimeString(timeLeft);
  return (
    <span>{eta}</span>
  )
};

export default Countdown;