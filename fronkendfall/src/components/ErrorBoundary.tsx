import React from 'react';

type ErrorBoundaryState = {
  error: Error | null;
}

class ErrorBoundary extends React.Component<{}, ErrorBoundaryState> {
  constructor(props: {}) {
    super(props);
    this.state = {error: null};
  }

  static getDerivedStateFromError(error: Error) {
    return { error };
  }

  componentDidCatch(error: Error, errorInfo: React.ErrorInfo): void {
    console.error("Caught error:", error, errorInfo);
  }

  render(): React.ReactElement | string | number | {} | React.ReactNodeArray | React.ReactPortal | boolean | null | undefined {
    if (this.state.error === null) {
      return this.props.children;
    }
    return (
      <>
        <h1>Åh nej!</h1>
        <p>Our software pooped itself because we're bad at computers.</p>
        <p>Here's some more info in case it's helpful to someone:</p>
        <pre>
          {
            this.state.error.toString()
          }
        </pre>

      </>
    )
  }
}

export default ErrorBoundary;