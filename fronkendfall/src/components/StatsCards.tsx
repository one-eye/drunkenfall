import React from "react";
import {ClassNameBuilder} from "../lib/ClassNameUtils";
import styles from './StatsCards.module.css';
import colors from './BorderColors.module.css';
import {Color} from "../lib/Types";

export const Stat: React.FC<{name: string; className?: string}> = ({name, className, children}) => {
  const classes = new ClassNameBuilder(styles.stat).add(className).build();
  return (
    <div className={classes}>
      <div className={styles.name}>{name}</div>
      {
        children
      }
    </div>
  );
};

interface StatsCardProps {
  title: string;
  className: string;
  color?: Color;
}

export const StatsCard: React.FC<StatsCardProps> = ({title, color, className, children}) => {
  const classes = new ClassNameBuilder(styles.statsCard).add(className);
  if (color) {
    classes.add(colors[color]);
  }
  return (
    <div className={classes.build()}>
      <div className={styles.title}>{title}</div>
      {
        children
      }
    </div>
  );
};

export const LargeStatsCard: React.FC<StatsCardProps> = ({title, color, className, children}) => {
  const classes = new ClassNameBuilder(styles.largeStatsCard).add(className);
  if (color) {
    classes.add(colors[color]);
  }
  return (
    <div className={classes.build()}>
      <div className={styles.title}>{title}</div>
      {
        children
      }
    </div>
  );
};