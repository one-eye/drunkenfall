import React, {ImgHTMLAttributes, useState} from "react";
import {ClassNameBuilder} from "../lib/ClassNameUtils";

interface OwnProps {
  fallback: string;
}

type ImageProps = OwnProps & ImgHTMLAttributes<HTMLImageElement>

const ImageWithFallback: React.FunctionComponent<ImageProps> = ({fallback, src, className, onError, ...rest}) => {
  const [error, setError] = useState(false);
  const url = error ? fallback : src;

  const classNames = new ClassNameBuilder(className);
  if (error) {
    classNames.add('error')
  }

  return (
    <img onError={() => setError(true)} className={classNames.build()} src={url} {...rest}/>
  )
};

export default ImageWithFallback;