import { useState } from 'react';
import useTimer from "./useTimer";

const useCurrentTime = () => {
  const [now, setNow] = useState(new Date());
  useTimer(() => setNow(new Date()), 1000);
  return now;
};

export default useCurrentTime;