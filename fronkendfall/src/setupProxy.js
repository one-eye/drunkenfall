const proxy = require('http-proxy-middleware');

module.exports = function(app) {
  app.use(proxy('/api', { target: 'http://localhost/'}));
  app.use(proxy('/img', { target: 'http://localhost/'}));
};