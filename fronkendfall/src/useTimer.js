import { useEffect } from 'react';

const useTimer = (handler, timeout) => {
  useEffect(() => {
    const id = setInterval(() => {
      handler();
    }, timeout);

    return () => {
      clearInterval(id);
    }
  }, [timeout]);
};

export default useTimer;